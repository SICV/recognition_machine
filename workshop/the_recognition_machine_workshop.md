

Translations

* Computer Vision: Vision par ordinateur
* Machine Learning: Apprentissage automatique
* Deep Learning: Apprentissage profond 

* Yann LeCun: En quoi l'apprentissage profond est-il une révolution ? - Intelligence artificielle
<https://www.youtube.com/watch?v=Edslp56WaFw>
 ... longer lecture <https://www.youtube.com/watch?v=2WiPx6thH2E>


## Exercises

### Write a Data Diary

Listen: Anne-James Chaton Evenements no. 11 (listen)
* Use a QR code reader (or barcode reader) to decode QR codes in your environment (on receipts) (todo: URL website QR code reader / barcode ??)

Variant: Download your data from google or facebook (todo: URLs of such resources)
Create a script based on this log

View: Scene from alphaville with the machine voice (occupe / libre)

## Listenings
* <https://www.franceculture.fr/emissions/creation-air/tentative-de-description-de-choses-vues-au-carrefour-mabillon-le-19-mai-1978>
* Anne-James Chaton Evenements no 11
* Data Diary
* I Love Alaska


## Readings
* Machine Learners, Adrian MacKenzie
* Deep Learning with Python, Francois Chollet

* Federica Muzzarelli, Formato Tessera (Photomaton) <https://www.unibo.it/sitoweb/federica.muzzarelli/en>
* Arnulf Rainer <http://www.artnet.com/artists/arnulf-rainer/?type=photographs>
* Wanted! Storia, tecnica ed estetica della fotografia criminale, segnaletica e giudiziaria
di Ando Gilardi <https://www.libreriauniversitaria.it/wanted-storia-tecnica-estetica-fotografia/libro/9788842490180>

## Slides / Examples to make
* <https://www.jcdecaux.com/partners/monetising-audiences/>



## Equipment
* RM camera
* barcode reader


# source of data

google

Example of location tracking, and estimation of mode of transportation.
https://www.google.com/maps/timeline?pb=!1m2!1m1!1s2016-08-15

* <https://support.google.com/accounts/answer/3024190?hl=fr>
* <https://takeout.google.com/settings/takeout>

facebook

* <https://www.facebook.com/help/1701730696756992?helpref=hc_global_nav>
* <https://www.wired.com/story/download-facebook-data-how-to-read/>

Ads tab

* Find a specific thing of interest in the inteface google/facebook provides to you.
(Example: Bicycle route on 16 Aug 2016)
* Request to download data you think is relevant, and once you have it, try to locate relevant data to your starting point.

=================================

Examples
Kodak film + racism
Facial Recognition: Fag Face





Final Documentation
=======================

Group 3: login: RecognitionMachine  - pwd: whoami


Discoveries
The Printer works very well to create a situation --- excitement to use, the pacing of the automatic portrait / classify / print loop is strong ... no one is looking 


Adjusting your face
Video inside...
Watching the search
Lichtbak / guidelines on the outside (dataset)
