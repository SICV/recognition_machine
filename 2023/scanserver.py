import asyncio
from aiohttp import web
import jinja2, aiohttp_jinja2
import json
# from sketch_annotate import Predictor
from PIL import Image
from post import post as post_to_server

@aiohttp_jinja2.template('index.html')
async def hello(request):
    # return web.Response(text="Hello, world")
    return {'message':"python"}


@aiohttp_jinja2.template('blank.html')
async def blank_view(request):
    # return web.Response(text="Hello, world")
    return {}


@aiohttp_jinja2.template('scanner.html')
async def scanner_view(request):
    # return web.Response(text="Hello, world")
    return {}

active_web_sockets = []

# https://aiohttp.readthedocs.io/en/stable/web_quickstart.html#aiohttp-web-websockets
async def scanner_view_ws(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    active_web_sockets.append(ws)
    print ("{0} websockets open".format(len(active_web_sockets)))
    # print ("websocket readloop")
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                await ws.send_str(msg.data + '/answer')
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())

    print('websocket connection closed')
    active_web_sockets.remove(ws)
    print ("{0} websockets open".format(len(active_web_sockets)))
    return ws

# https://aiohttp.readthedocs.io/en/stable/web_advanced.html#background-tasks

async def send_scan_start (magic, width, height, bpp):
    msg = {"msg": "scanstart", "magic": magic, "width": width, "height": height, "bpp": bpp}
    for ws in active_web_sockets:
        print("sending scan_start")
        await ws.send_json(msg)

async def send_scan_line (y, data):
    # if y == 0:
    # print ("scan_line, first bytes")
    # for i, byte in enumerate(data[:12]):
    #      print ("{0}: {1}".format(i, byte))
    # data = base64.encodebytes(data)
    # data = data.decode("ascii")
    # msg = {"msg": "scanline", "y": y, "data": data}
    for ws in active_web_sockets:
        # await ws.send_json(msg)
        await ws.send_bytes(data)
# consider divide/reduce the data already here!

# async def send_prediction (data):
#     msg = {"msg": "prediction", "prediction": data}
#     for ws in active_web_sockets:
#         print("sending prediction")
#         await ws.send_json(msg)

async def send_display_text (text):
    msg = {"msg": "displaytext", "text": text}
    for ws in active_web_sockets:
        print("sending displaytext")
        await ws.send_json(msg)

async def send_show_url (url):
    msg = {"msg": "show_url", "url": url}
    for ws in active_web_sockets:
        print(f"sending show_url {url}")
        await ws.send_json(msg)

async def run_scan_loop (app):
    process = None
    try:
        # print ("Initing predictor")
        # model = Predictor()
        print ()
        print ("Running scan loop")
        while True:
            if args.button:
                await wait_for_button()
            else:
                print ("Auto scan in 5 secs")
                await asyncio.sleep(5)

            # I. SCAN IMAGE
            process = await asyncio.create_subprocess_exec(*SCAN, stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
            magic, width, height, bpp = None, None, None, None
            with open("scan.ppm", "wb") as scan_out:
                while True:
                    line = await process.stdout.readline()
                    scan_out.write(line)
                    line = line.decode("utf-8").rstrip()
                    # print ("readline", line)
                    if not line.startswith("#"):
                        if magic == None:
                            magic = line
                        elif width == None:
                            width, height = [int(x) for x in line.split()]
                        else:
                            bpp = int(line)
                            break
                print ("Read image header: {0} {1}x{2} {3}".format(magic, width, height, bpp))
                # send scan_start message to websocket clients
                await send_scan_start(magic, width, height, bpp)
                linebytes = width*3
                # print ("linebytes", linebytes)
                y = 0
                while True:
                    data = await process.stdout.readexactly(linebytes)
                    if not data: break # using readexacly means we don't' actually trigger this
                    scan_out.write(data)
                    # print ("scan: got {0} {1} {2}".format(len(data), type(data), y, height))
                    await send_scan_line(y, data)
                    y += 1
                    if y == height:
                        break
            # print ("end {0} {1}".format(y, height))
            # Stop the subprocess
            #process.terminate()
            code = await process.wait()
            print('Scan terminated with code {}'.format(code))

            # convert ppm to jpg
            # Feb 2023 for IVAM, adding noramlize and contrast, ada test scanner is quite gray
            convert = "convert scan.ppm -crop 2438x3433+55+55 -normalize -contrast scan.jpg".split()
            process = await asyncio.create_subprocess_exec(*convert)
            stdout, stderr = await process.communicate()
            print ("convert returned {0}".format(process.returncode))

            qr = None
            # bar code detection and placement
            # ZBAR = "./zbarscan.py scan.jpg".split()
            # process = await asyncio.create_subprocess_exec(*ZBAR, stdout=asyncio.subprocess.PIPE)
            # stdout, stderr = await process.communicate()
            # if (process.returncode == 0):
            #     qr = json.loads(stdout)
            #     print ("qr", qr)
            #     if len(qr):
            #         qr = qr[0]
            #         ###
            #         # ROTATE
            #         x, y = qr['location'][0]
            #         left = x < 2488/2
            #         top = y < 3500/2
            #         rot = 0
            #         if left:
            #             if top:
            #                 rot = 270
            #             else: # bottom
            #                 rot = 0
            #         else: # right
            #             if top:
            #                 rot = 180
            #             else: # bottom
            #                 rot = 90 # aka -90
            #         if rot:
            #             convert = "mogrify -rotate {0} scan.jpg".format(rot).split()
            #             process = await asyncio.create_subprocess_exec(*convert)
            #             stdout, stderr = await process.communicate()
            #             print ("mogrify returned {0}".format(process.returncode))
            #     else:
            #         qr = None

            # if qr:
            #     await send_display_text("Archive link found!\nVandalizing the archive...")
            #     # transmit to server
            #     print ("sending data to server")

            # else:
            #     await send_display_text("No archive link detected.\nAdding scan to rejects collection.")

            await send_display_text("Guardando el archivo...")

        
            post = {}
            im = Image.open("scan.jpg")
            post['width'], post['height'] = im.size
            im.close()
            if qr:
                post['qr_data'] = qr['data']
                post['qr_location'] = json.dumps(qr['location'])

            # post['sketch_annotation'] = json.dumps(sketch)
            
            print (json.dumps(post, indent=2))
            if not args.nopost:
                print ("posting to server")
                try:
                    resp = post_to_server("scan.jpg", post)
                    # upload("scan.jpg", qr.data)
                    print ("response from server", resp)
                    if ("next_url" in resp):
                        await send_show_url(resp['next_url'])
                except Exception as e:
                    print ("Exception sending to server", e)
                    await send_display_text("Error, inténtelo de nuevo más tarde")
                    await asyncio.sleep(5)
                    
            # sketch recognition
            # sketch = model.predict_sketch_category("scan.jpg")
            # print("sketch", json.dumps(sketch))
            # await send_prediction(sketch)

            if not args.button:
                print ("waiting 10 secs")
                await asyncio.sleep(10)

    except asyncio.CancelledError:
        pass
        # if process:
        #     print ("Waiting for process to scanner to finish")
        #     code = await process.wait()
        #     print('Scan terminated with code {}'.format(code))
    finally:
        print ("Cleaning up scan loop")


###################
# RUN!

async def start_background_tasks(app):
    app['scanloop'] = asyncio.create_task(run_scan_loop(app))

async def cleanup_background_tasks(app):
    app['scanloop'].cancel()
    await app['scanloop']

BUTTON_PIN = 26
BUTTON_LED_PIN = 19

async def wait_for_button():
    import RPi.GPIO as GPIO
    GPIO.output(BUTTON_LED_PIN, True)
    while True:
        button_state = GPIO.input(BUTTON_PIN)
        if button_state == False:
            GPIO.output(BUTTON_LED_PIN, False)
            return True
        await asyncio.sleep(0.1)

async def init_gpio(app):
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(BUTTON_LED_PIN, GPIO.OUT) # BUTTON LED
    GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # BUTTON

async def cleanup_gpio(app):
    import RPi.GPIO as GPIO
    GPIO.output(BUTTON_LED_PIN, False)

import argparse

ap = argparse.ArgumentParser("")
ap.add_argument('--button', default=False, action="store_true")
ap.add_argument('--scanplaceholder')
ap.add_argument('--nopost', default=False, action="store_true")
args = ap.parse_args()


if args.scanplaceholder:
    SCAN = ["cat", args.scanplaceholder]
else:
    SCAN = "scanimage --mode Color --resolution 300".split()

SCAN_BUFFER_SIZE = 1024*100

app = web.Application()
aiohttp_jinja2.setup(app,
    loader=jinja2.FileSystemLoader('templates'))
# app.add_routes([web.get('/', hello)])
app.add_routes([web.get('/', scanner_view)])
app.add_routes([web.get('/blank', blank_view)])
app.add_routes([web.get('/ws', scanner_view_ws)])

if args.button:
    app.on_startup.append(init_gpio)
    app.on_cleanup.append(cleanup_gpio)
app.on_startup.append(start_background_tasks)
app.on_cleanup.append(cleanup_background_tasks)

web.run_app(app)
