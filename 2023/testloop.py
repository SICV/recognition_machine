import argparse
from time import sleep
from picamera2 import Picamera2
from libcamera import controls

BUTTON = 26
BUTTON_LED = 19
RELAY_1 = 27
RELAY_2 = 22

ap = argparse.ArgumentParser("")
ap.add_argument('--gpio', default=True, action="store_true")
ap.add_argument('--audio', default=False, action="store_true")
args = ap.parse_args()

def wait_for_button(pin):
    while True:
        button_state = GPIO.input(pin)
        if button_state == False:
            return True
        sleep(0.1)

def tone ():
    os.system("aplay beep.wav 2> /dev/null")

if args.gpio:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(BUTTON_LED, GPIO.OUT)
    GPIO.setup(RELAY_1, GPIO.OUT)
    GPIO.setup(RELAY_2, GPIO.OUT)
    GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)

try:
    picam2 = Picamera2()
    count = 1
    # camera_config = picam2.create_still_configuration(main={"size": (1920, 1080)}, lores={"size": (640, 480)}, display="lores")
    camera_config = picam2.create_still_configuration(main={"size": (640, 480)}, lores={"size": (640, 480)}, display="lores")
    picam2.configure(camera_config)
    while True:
        if args.gpio:
            GPIO.output(BUTTON_LED, True)
            wait_for_button(BUTTON)
            GPIO.output(BUTTON_LED, False)
        else:
            input("Press THE BUTTON to take a photo")

        if args.audio:
            os.system("omxplayer -o both audio/01.mp4")

        # LIGHT
        if args.gpio:
            GPIO.output(RELAY_1, GPIO.HIGH)

        # if args.audio:
        #     # os.system("omxplayer -o both audio/02.mp3")
        #     os.system("omxplayer -o both audio/02.mp4 &")
        #     sleep(2)
        # else:
        #     tone()


        sleep(1)
        # current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
        # current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)

 
        picam2.start(show_preview=False)
        picam2.set_controls({"AfMode": controls.AfModeEnum.Continuous, "AfSpeed": controls.AfSpeedEnum.Fast})
        # picam2.start_and_capture_files("fastfocus-test{:d}.jpg", num_files=3, delay=0.5)
        sleep(3)
        picam2.capture_file(f"snapshot{count:04d}.jpg")
        picam2.stop_preview()
        count += 1

        if args.gpio:
            GPIO.output(RELAY_1, GPIO.LOW)
        if args.audio:
            os.system("killall omxplayer.bin")
finally:
    if args.gpio:
        GPIO.output(BUTTON_LED, False)
        GPIO.output(RELAY_1, GPIO.LOW)
        GPIO.output(RELAY_2, GPIO.LOW)
    picam2.stop()
