Sep 2023
=============

Useful is [photobooth/NOTES.md](../photobooth/NOTES.md)

![](grove_relay/Grove%20-%202-Channel%20SPDT%20Relay%20Seeed%20Studio%20Wiki_files/pin_map.jpg)

![](../photobooth/notes/raspberry_pins.jpeg)




https://www.bol.com/be/nl/p/luminexpro-adapter/9300000156020675/?bltgh=jOQLOwmOpK4XeHInFMxAuQ.2_70.74.ProductImage
https://www.bol.com/be/nl/p/lurk-ringlamp-set-pro-18-inch-verstelbaar-statief-afstandsbieding-led-geschikt-voor-smartphone-en-camera/9300000131536297/?bltgh=keQm6R2-q6WYNqzzAWdM5w.pEN2FyfArfMNPhBGnPhOtA_0_15.16.ProductImage

TL-RING (g13 connection?)
https://www.bol.com/be/nl/p/osram-4058075135482-led-lamp-energielabel-a-ring-20-w-neutraalwit-30-mm-x-298-mm-1-stuk/9200000101441673/?bltgh=po6ZjwR6qjceKFGW3-BGOQ.2_6.9.ProductTitle


OK... so

Flash on Pi
--------------
GPIO -Purple -- Pin7: GPIO4
GND   Gray --- Pin 6 : Ground
GND   White? -- not connected
5V -- Black -- Pin 2: 5V

Button
-----------


## button

35 Yellow GPIO19 (button lamp)
37 Green GPIO26 (button)
39 Blue GND

yellow,green,blue
pi: purple,gray,white <--> 35,37,39(gnd)

(match "blue" (on extension) to white (cable to box))

## relay <--> GPIO

* LAMP / Grove
Reuse Flash mappings?
BLACK: GND: pin 9, 15
RED: 5V/VCC: Phyical pin 2
WHITE: SIG2
YELLOW: SIG1

pi: black,brown,red,orange <--> 14(gnd), 2(vcc), 13, 15

from the relay:
black,red,white,yellow

so match black!


## PIR sensor <-> GPIO
    recommended:
    Left pin ("GND") 	Physical pin 6 (GND pin)
    Middle pin ("OUT") 	Physical pin 11 (GPIO 17)
    Right pin ("VCC") 	Physical pin 4 (5V)

* blue-purple-white (vcc,signal,gnd) <-> 4,11,6


Camera Module 3
========================

Using [this guide](https://www.tomshardware.com/how-to/raspberry-pi-camera-module-3-python-picamera-2)

libstill

Nov 2023, Installing the website locally
=========================================

Checking install on ovh

pip freeze shows:
```
absl-py==1.0.0
astor==0.8.1
astunparse==1.6.3
cached-property==1.5.2
cachetools==4.2.4
certifi==2021.10.8
charset-normalizer==2.0.8
Django==2.1.3
flatbuffers==2.0
gast==0.4.0
google-auth==2.3.3
google-auth-oauthlib==0.4.6
google-pasta==0.2.0
grpcio==1.42.0
h5py==3.6.0
idna==3.3
keras==2.7.0
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.2
libclang==12.0.0
Markdown==3.0.1
numpy==1.21.4
oauthlib==3.1.1
opencv-python==4.5.4.60
opt-einsum==3.3.0
Pillow==5.3.0
pkg_resources==0.0.0
protobuf==3.19.1
pyasn1==0.4.8
pyasn1-modules==0.2.8
pytz==2018.7
qrcode==7.3.1
requests==2.26.0
requests-oauthlib==1.3.0
rsa==4.8
scipy==1.7.3
six==1.16.0
tensorboard==2.7.0
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.8.0
tensorflow==2.7.0
tensorflow-estimator==2.7.0
tensorflow-io-gcs-filesystem==0.22.0
termcolor==1.1.0
typing_extensions==4.0.1
urllib3==1.26.7
Werkzeug==2.0.2
wrapt==1.13.3
```

Server is actually debian 10.13

Maybe try to match just the django version, and let other libs be up to date?
Seems to run with uwsgi...


    python3 -m venv venv --system-site-packages

    sudo apt install python3-opencv python3-scipy python3-keras
    sudo apt install uwsgi uwsgi-plugin-python3

    source venv/bin/activate
    pip install django==2.1.3



Set permissions on db

drwxrwxr-x 2 murtaugh www-data 4.0K Nov 21 10:00 db
-rw-rw-r-- 1 murtaugh www-data 1.5M Nov 21 10:00 db.sqlite3


& media folders:
drwxr-xr-x 2 www-data www-data 136K Nov 21 10:00 posts

    sudo chown murtaugh:www-data /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db/db.sqlite3 
    sudo chmod 664 /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db/db.sqlite3 

    sudo chown murtaugh:www-data /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db 
    sudo chmod 775 /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db

    cd media
    sudo chown -R www-data:www-data *

    sudo tail -f /var/log/uwsgi/app/recognitionmachine.uwsgi.log

To restart django:

    sudo systemctl restart uwsgi



192.168.1.23

chromium...
--unsafely-treat-insecure-origin-as-secure="http://example.com"


Holes for camera mount: 5mm.




----

## Feb 2024

https://www.kiwi-electronics.com/nl/iqaudio-digiampplus-10832

Fitting the pieces together realize I need...

Spacer for the HAT...
GPIO SPACER? (need more space for display + camera cables underneath)
Special screws / separators?

should have gotten this one:
https://www.kiwi-electronics.com/nl/mean-well-power-supply-19v-4-74a-10993
OR... maybe not?


screw extenders?
https://www.kiwi-electronics.com/nl/messing-m2-5-afstandhouders-11mm-hoog-zwart-2-pack-1994?search=gpio&page=3


to make a simplified
https://www.kiwi-electronics.com/nl/40-pins-idc-connector-naar-female-jumperwires-20cm-3016?search=gpio&page=4


https://www.kiwi-electronics.com/nl/gpio-edge-connector2x40-pins-uitbreiding-voor-raspberry-pi--11282?search=gpio&page=2



OOEI... or these together ?

https://www.amazon.de/Geekworm-Extension-G341-Adapter-Raspberry/dp/B0BD79QW8K/ref=sr_1_2?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=19IVSAF1MN2LL&keywords=gpio+edge&qid=1706785647&sprefix=gpio+edge%2Caps%2C87&sr=8-2


https://www.amazon.de/GeeekPi-Raspberry-Terminal-Expansion-Connector/dp/B09535KQSC/ref=pd_bxgy_img_d_sccl_1/261-3428942-9813962?pd_rd_w=JVFmN&content-id=amzn1.sym.1fd66f59-86e9-493d-ae93-3b66d16d3ee0&pf_rd_p=1fd66f59-86e9-493d-ae93-3b66d16d3ee0&pf_rd_r=9H30M6TK55BCXCZCCSPE&pd_rd_wg=0sZkB&pd_rd_r=1a14a3a7-24bf-46f0-a1b2-a5643fda805e&pd_rd_i=B09535KQSC&psc=1


## Feb 2024

Working with Stefan, wide camera seems no longer working :~0

Shopping list:
* New camera module 3 wide
* longer camera ribbon cables (100, 200, 45 x2 ?)
* Arcade buttons (red + white) ... also for backs



Bring and leave a cable for a network (debugging) connection.
(it's difficult to remove/add it)

## Feb 19 2024

Final testing and (re)install...

Now based on image: 2023-12-11-raspios-bookworm-armhf-lite.img.xz
with apt-get update + upgrade
Debian is 12.1 (bookworm)
System python is 3.11
Disabled built in audio in config, for the rest it's defaults.

All files are in /var/www/vhosts/recognitionmachine.vandal.ist

Key folders:

* public_html
    * media
* venv
* wsgi
    * recognition_machine : the git
    * sock
    * uwsgi_params


### git remotes

    local	/home/murtaugh/recognition_machine.git/ (fetch)
    local	/home/murtaugh/recognition_machine.git/ (push)
    origin	git@gitlab.constantvzw.org:SICV/recognition_machine.git (fetch)
    origin	git@gitlab.constantvzw.org:SICV/recognition_machine.git (push)



### Systemd services

printbot.service
```
[Unit]
Description=Recognition Machine printbot
After=syslog.target network.target

[Service]
Type=simple
User=murtaugh
Group=murtaugh
WorkingDirectory=/var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/printbot
ExecStart=/var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/printbot/start.local.sh
Restart=always

[Install]
WantedBy=multi-user.target
```

cameraloop.service
```
[Unit]
Description=Recognition Machine cameraloop
After=syslog.target network.target

[Service]
Type=simple
User=murtaugh
Group=murtaugh
WorkingDirectory=/var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/2023
ExecStart=/var/www/vhosts/recognitionmachine.vandal.ist/venv/bin/python3 cameraloop.py
Restart=always

[Install]
WantedBy=multi-user.target
```



    python3 -m venv venv --system-site-packages

    sudo apt install python3-opencv python3-scipy python3-keras
    sudo apt install uwsgi uwsgi-plugin-python3

    source venv/bin/activate
    pip install django==2.1.3



Set permissions on db

drwxrwxr-x 2 murtaugh www-data 4.0K Nov 21 10:00 db
-rw-rw-r-- 1 murtaugh www-data 1.5M Nov 21 10:00 db.sqlite3


& media folders:
drwxr-xr-x 2 www-data www-data 136K Nov 21 10:00 posts

    sudo chown murtaugh:www-data /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db/db.sqlite3 
    sudo chmod 664 /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db/db.sqlite3 

    sudo chown murtaugh:www-data /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db 
    sudo chmod 775 /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/website/db

    cd media
    sudo chown -R www-data:www-data *

    sudo tail -f /var/log/uwsgi/app/recognitionmachine.uwsgi.log

To restart django:

    sudo systemctl restart uwsgi


