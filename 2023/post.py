import requests
import html5lib


def extract_csrf (html_src):
    t = html5lib.parseFragment(html_src, namespaceHTMLElements=False)
    for hinput in t.findall(".//input[@type='hidden']"):
        if hinput.attrib.get("name") == "csrfmiddlewaretoken":
            return hinput.attrib.get("value")

def trm_post (image_path, url="http://localhost/b/"):
    files = {'file': open(image_path, 'rb')}
    form = requests.get(url)
    data = {'csrfmiddlewaretoken': extract_csrf(form.text)}
    resp = requests.post(url, data=data, headers= {'referer': url}, cookies = form.cookies, files=files)
    print (f"resp.url: {resp.url}, {resp}")
    if resp.status_code == 200:
        next_url = resp.url
        data = {'csrfmiddlewaretoken': extract_csrf(resp.text)}
        print (f"DETECT: {next_url}")
        resp2 = requests.post(next_url, data=data, headers={'referer': next_url}, cookies = resp.cookies)
        if resp2.status_code == 200:
            next_url = resp2.url
            data = {'csrfmiddlewaretoken': extract_csrf(resp2.text)}
            print (f"MATCH: {next_url}")
            resp3 = requests.post(next_url, data=data, headers={'referer': next_url}, cookies = resp2.cookies)
            print (f"Result: {resp3.status_code}")
            if resp3.status_code == 200:
                print (f"Match URL: {resp3.url}")
                return resp3.url

if __name__ == "__main__":
    r = trm_post("snapshot_mnm.jpg")


