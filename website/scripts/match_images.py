import os, sys
import cv2
import numpy as np
# used below
#    from keras.models import load_model
from scipy.spatial.distance import cdist

MIN_FACE_SIZE = 100
IMG_SIZE = 64
DEPTH = 16
K = WIDTH = 8
MARGIN = 0.4

def detect_faces (face_cascade, imgpath):
    frame = cv2.imread(imgpath)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray)
    faces = [x for x in faces if x[2] > MIN_FACE_SIZE]
    # ret=[]
    # for (x,y,w,h) in faces:
    #     #print "face", (x, y, w, h)
    #     ret.append({'x': int(x), 'y': int(y), 'w': int(w), 'h': int(h)})
    return frame, gray, faces

def detect_emotions(emotion_classifier, gray, faces):
    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_offsets = (20, 40)
    # fer2013
    # emotion_labels = {0:'angry',1:'disgust',2:'fear',3:'happy', 4:'sad',5:'surprise',6:'neutral'}
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    # img, gray, faces = detect_faces(path)

    results = np.zeros((len(faces), 7), 'float32')
    for i, face_coordinates in enumerate(faces):
        print ("face_coordinates", face_coordinates, file=sys.stderr)
        # ERROR CAUGHT HERE!!!! order of y1 & x2 were incorrectly inverted (BAD: x1, x2, y1, y2)
        x1, y1, x2, y2 = face_coordinates[0], face_coordinates[1], face_coordinates[0]+face_coordinates[2], face_coordinates[1]+face_coordinates[3] # apply_offsets(face_coordinates, emotion_offsets)
        gray_face = gray[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except Exception as e:
            print ("Exception resizing", e)
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        results[i] = emotion_classifier.predict(gray_face)
        # d = {}
        # for label, score in zip(emotion_labels, emotion_prediction[0]):
        #     d[label] = float(score)
        # print ("got emotion_prediction", d)
        # dd.append(d)
    return results

def match_images (paths, face_cascade, emotion_classifier, allemotions, facedetection=False):
    testemotions = np.zeros((len(paths), 7), dtype='float32')
    for ii, image in enumerate(paths):
        if facedetection:
            img, gray, faces = detect_faces(face_cascade, image)
            # print ("Detected {0} faces".format(len(faces)), file=sys.stderr)
            emotions = detect_emotions(emotion_classifier, gray, faces)
        else:
            frame = cv2.imread(image)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = [(0, 0, gray.shape[1], gray.shape[0])]
            # print ("faces", faces)
            emotions = detect_emotions(emotion_classifier, gray, faces)
        testemotions[ii] = emotions    
    cd = cdist(allemotions, testemotions, metric="cityblock")
    minindexes = np.argmin(cd, axis=0)
    return minindexes, testemotions

EMOTION_CHOICES = (
    ('angry', 'angry'),
    ('disgust', 'disgust'),
    ('fear', 'fear'),
    ('happy', 'happy'),
    ('sad', 'sad'),
    ('surprise', 'surprise'),
    ('neutral', 'neutral')
)
def unparse_emotion_vector (e):
    for i in range(7):
        print ("{1:0.01f}% {0}".format(EMOTION_CHOICES[i][0], float(e[i])*100))



if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("--models", default="models")
    ap.add_argument("--array", default="fer2013_emotions.npy")
    ap.add_argument("--facedetection", action="store_true", default=False)
    ap.add_argument("image", nargs="+")
    args = ap.parse_args()

    print ("Loading {0}".format(args.array), file=sys.stderr)
    allemotions = np.load(args.array)

    print ("Loading face_cascade", file=sys.stderr)
    from keras.models import load_model
    face_cascade = cv2.CascadeClassifier(os.path.join(args.models, 'haarcascade_frontalface_default.xml'))
    print ("Loading emotion classifier", file=sys.stderr)
    emotion_classifier = load_model(os.path.join(args.models, "emotion_model.hdf5"))
    print ("Loaded", file=sys.stderr)

    minindexes, testemotions = match_images(args.image, face_cascade, emotion_classifier, allemotions, args.facedetection)

    for i, minindex in enumerate(minindexes):
        print ("IMAGE: {0}".format(args.image[i]))
        unparse_emotion_vector(testemotions[i])
        print ()
        print ("MATCH: ~/machinelearning/datasets/fer2013_public/Train/{0}.jpg".format(minindex+1))
        unparse_emotion_vector(allemotions[minindex])
        print ()

