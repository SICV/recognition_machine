import os, sys
import cv2
import numpy as np



if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("--array", default="fer2013_labels.npy", help="name of output file")
    ap.add_argument("labels")
    args = ap.parse_args()

    # print ("Loading {0}".format(args.array), file=sys.stderr)
    # allemotions = np.load(args.array)

    # print ("Loading face_cascade", file=sys.stderr)
    # face_cascade = cv2.CascadeClassifier(os.path.join(args.models, 'haarcascade_frontalface_default.xml'))

    lpath = os.path.dirname(args.labels)
    # firstpass
    with open(args.labels) as f:
        line1 = f.readline()
        count = 0
        for line in f:
            path,label = line.split(",")
            imgpath = os.path.join(lpath, path)
            imgid = os.path.splitext(os.path.basename(path))[0]
            count += 1
            assert(count == int(imgid))
    total = count
    print ("{0} total images".format(total))
    alllabels = np.zeros((total,), 'int8')
    with open(args.labels) as f:
        line1 = f.readline()
        count = 0
        for line in f:
            path,label = line.split(",")
            count += 1 
            alllabels[count-1] = label
    print ("Saving data to {0}".format(args.array))
    np.save(args.array, alllabels)


