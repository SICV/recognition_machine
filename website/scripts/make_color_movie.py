#!/usr/bin/env python3

import cv2, os, datetime, sys, json
from argparse import ArgumentParser
from time import sleep
import numpy as np

def resize (img, w, h, interpolation = cv2.INTER_CUBIC): # INTER_NEAREST, INTER_CUBIC
    ih, iw, ic = img.shape
    # if (ih > h) or (iw > w):
    # try fitting width
    sw = w
    sh = int(sw * (float(ih)/iw))
    if sh > h:
        # fit height instead
        sh = h
        sw = int(sh * (float(iw)/ih))
    return cv2.resize(img, (sw, sh), interpolation=interpolation)
    # return img

def pad (img, w, h, color=(0, 0, 0)):
    ih, iw, ic = img.shape
    top = (h - ih) // 2
    bottom = h - ih - top
    left = (w - iw) // 2
    right = (w - iw - left)
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

def putTextCentered (img, text, pos, font_scale=1.0, thickness=2, text_color=(255,255,255)):
    (tw, th), baseline = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, font_scale, thickness)    
    cv2.putText(img, text, (pos[0]-(tw//2), pos[1]),
                cv2.FONT_HERSHEY_SIMPLEX, font_scale, text_color, thickness) # cv2.LINE_AA


def putText (img, text, pos, font_scale=1.0, thickness=2, text_color=(255,255,255)):
    cv2.putText(img, text, pos,
                cv2.FONT_HERSHEY_SIMPLEX, font_scale, text_color, thickness) # cv2.LINE_AA


ap = ArgumentParser("")
ap.add_argument("--output", default="color.avi")
ap.add_argument("--duration", type=float, default=1.0)
# ap.add_argument("--labels", default="labels_public.txt")
ap.add_argument("--width", type=int, default=1920)
ap.add_argument("--height", type=int, default=1080)
ap.add_argument("--background", default="255,255,255")
#ap.add_argument("--text", default="128,128,128")
# ap.add_argument("--nopad", default=False, action="store_true")
ap.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
ap.add_argument("--framerate", type=float, default=10, help="output frame rate")
#ap.add_argument("--interpolation", default="cubic", help="nearest,cubic")
args = ap.parse_args()

fourcc = None
try:
    fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
except AttributeError:
    fourcc = cv2.VideoWriter_fourcc(*args.fourcc)
#text_color = tuple([int(x) for x in args.text.split(",")])
background = tuple([int(x) for x in args.background.split(",")])
# INTERPOLATION = cv2.INTER_NEAREST
# if args.interpolation == "cubic":
#     INTERPOLATION = cv2.INTER_CUBIC


width, height = args.width, args.height
frame = np.zeros((height,width,3), np.uint8)

# print (frame)
frame[:] = background
# print (frame)

outpath = args.output
out = cv2.VideoWriter()
out.open(outpath, fourcc, args.framerate, (args.width, args.height))

for i in range(int(args.framerate*args.duration)):
    out.write(frame)

out.release()










