#!/usr/bin/env python3

import os, datetime, sys
from argparse import ArgumentParser
from time import sleep

# from PIL import Image
from PIL import Image, ImageOps

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
import math
from reportlab.pdfbase.ttfonts import TTFont, pdfmetrics
from reportlab.lib.colors import white, black, cssParse, hsl2rgb
import json
from draw_image import render_image_box


LABELS = "angry disgust fear happy sad surprise neutral".split()
HUES = [x*360/7.0 for x in range(7)];

def afm_font_name(path):
    "Extract a font name from an AFM file."
    with open(path) as f:
        for line in f:
            if line.startswith("FontName "):
                return line[9:].rstrip()

def get_emotion_color (index, opacity=0.5):
    return cssParse("hsla({0:0.2f},50%,66%,{1:0.02f})".format(HUES[index], opacity))

def item_iter (jsons):
    """ iterator to return each training image as (label, path)"""
    for filename in jsons:
        # print ("filename", filename)
        yield filename
        # with open(filename) as f:
        #     data = json.load(f)
        #     path = os.path.join(args.media, "photos", data['id'])
        #     yield path, data['faces']

if __name__ == "__main__":
    ### option to sort
    ### base on size of image cell
    ###
    ### include pixel font ?! (to spell things)
    ###

    p = ArgumentParser("")
    p.add_argument("--interpolation", default="cubic", help="nearest,cubic")
    p.add_argument("--top", default="0.5cm")
    p.add_argument("--left", default="0.5cm")
    p.add_argument("--right", default="0.5cm")
    p.add_argument("--bottom", default="0.5cm")
    p.add_argument("--cols", type=int, default=10)
    p.add_argument("--overlay", default=False, action="store_true")
    p.add_argument("--background", default="255,255,255")
    p.add_argument("--font", default="unifont-12.0.01.ttf")
    p.add_argument("--fontsize", default=94, type=int)
    p.add_argument("--limit", default=None, type=int)
    p.add_argument("--sort", default=False, action="store_true")
    p.add_argument("--data", default="media/data/photos/")
    p.add_argument("--media", default="media/")
    args = p.parse_args()

    import glob
    jsons = glob.glob(os.path.join(args.data, "0*.json"))
    jsons.sort()
    print (len(jsons), "photos")

    booth_height = 204*cm
    booth_back = 92*cm
    booth_side = 45*cm
    booth_strip = 22*cm

    # c.save()
    
    # print (len(list(item_iter(args.labels))))
    # cellsize = 5.65*cm
    # cellsize = 5.25*cm
    cellsize = 5.65*cm

    FITBOX = 640

    cols = int(math.floor(booth_back / cellsize))
    rows = int(math.floor(booth_height / cellsize))
    print ("Back: {0}x{1}: {2}".format(cols, rows, cols*rows))
    left = booth_back - (cols*cellsize)
    totalcells = cols*rows
    cols2 = int(math.floor(booth_side / cellsize))
    rows2 = int(math.floor(booth_height / cellsize))
    print ("Side: {0}x{1}: {2}".format(cols2, rows2, cols2*rows2))
    totalcells += cols2*rows2
    print ("Total cells: {0}".format(totalcells))

    # Back: 16x36: 576
    # Side: 7x36: 252
    # 828 cells
    # todo make fit perfectly

    items = item_iter(jsons)

    c = canvas.Canvas("photobooth_rm.pdf", pagesize=(booth_back, booth_height))
    c.setFillColorRGB(0, 0, 0)
    c.rect(0, 0, booth_back, booth_height, fill=1)
    
    row, col = 0, 0
    count = 0
    page = 0
    stretch_to_fit = False
    if stretch_to_fit:
        cellwidth = booth_back / cols
        cellheight = booth_height / rows
        fitboxwidth = FITBOX
        fitboxheight = FITBOX # int(320 * (cellheight / cellwidth))
        # print ("fitbox", fitboxwidth, fitboxheight)
        left = 0
    else:
        cellwidth, cellheight = cellsize, cellsize
        fitboxwidth, fitboxheight = FITBOX, FITBOX
        left = booth_back - (cols*cellsize)
    try:
        while True:
        # for col in range(cols):
        #     for row in range(rows):

            x = left + (col*cellwidth)
            y = booth_height - (row*cellheight) - cellheight

            path = next(items)
            image = render_image_box(path, fitboxwidth, fitboxheight)

            # image = Image.open(path)
            # image = image.resize((100, 100), Image.ANTIALIAS)
            # print ("image", image)
            # square = ImageOps.fit(image, (100, 100), Image.ANTIALIAS)
            c.drawInlineImage(image, x, y, width=cellwidth, height=cellheight)
            print (row, col, path, end="\r")
            count += 1 # count is of cells -- not images

            # Advance position
            col += 1
            if col >= cols:
                col = 0
                row += 1
            if row >= rows:
                if page == 0:
                    print ("Ending page 1 after {0} cells".format(count))
                    c.showPage()
                    c.save()
                    del(c)
                    # Start page 2
                    c = canvas.Canvas("photobooth_rm_side.pdf", pagesize=(booth_side, booth_height))
                    # c.setPageSize((booth_side, booth_height))
                    c.rect(0, 0, booth_side, booth_height, fill=1)

                    left = 0 # booth_side - (cols2*cellsize)

                    row, col = 0, 0
                    rows, cols = rows2, cols2
                    count = 0
                    if stretch_to_fit:
                        cellwidth = booth_side / cols
                        cellheight = booth_height / rows 
                        fitboxwidth = FITBOX
                        fitboxheight = FITBOX # int(320 * (cellheight / cellwidth))
                        # print ("fitbox", fitboxwidth, fitboxheight)
                        left = 0
                    else:
                        left = 0

    except StopIteration:
        c.showPage()
    c.save()

