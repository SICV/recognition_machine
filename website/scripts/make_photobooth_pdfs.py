#!/usr/bin/env python3

import os, datetime, sys
from argparse import ArgumentParser
from time import sleep

from PIL import Image
import cv2
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
import math
from reportlab.pdfbase.ttfonts import TTFont, pdfmetrics
from reportlab.lib.colors import white, black, cssParse, hsl2rgb


LABELS = "angry disgust fear happy sad surprise neutral".split()
HUES = [x*360/7.0 for x in range(7)];

def afm_font_name(path):
    "Extract a font name from an AFM file."
    with open(path) as f:
        for line in f:
            if line.startswith("FontName "):
                return line[9:].rstrip()

def get_emotion_color (index, opacity=0.5):
    return cssParse("hsla({0:0.2f},50%,66%,{1:0.02f})".format(HUES[index], opacity))

def get_emotion_color_cv (index, opacity=0.5):
    rl = cssParse("hsla({0:0.2f},50%,66%,{1:0.02f})".format(HUES[index], opacity))
    r, g, b = rl.bitmap_rgb()
    return b, g, r

def item_iter (path, onlylabel=None):
    """ iterator to return each training image as (label, path)"""
    with open(path) as f:
        f.readline()
        for line in f:
            path, label = line.split(",")
            if path.startswith("Train/"):
                label = int(label)
                if onlylabel == None or label == onlylabel:
                    yield label, path

def sorted_item_iter (lpath):
    """ iterator to return each training image as (label, path)"""
    for label in range(len(LABELS)):
        for label, path in item_iter(lpath, label):
            yield label, path

def render_emotion_color (path, label):
    im = cv2.imread(path)
    overlay = im.copy()
    # label = LABELS.index(f['emotion'])
    color = get_emotion_color_cv(label)
    # print ("face", f['emotion'], label)
    h, w = im.shape[0], im.shape[1]
    cv2.rectangle(overlay, (0, 0), (w, h), color, -1)
    alpha = 0.5
    cv2.addWeighted(overlay, alpha, im, 1 - alpha, 0, im)

    im = cv2.cvtColor(im,cv2.COLOR_BGR2RGB)
    return Image.fromarray(im)

if __name__ == "__main__":
    ### option to sort
    ### base on size of image cell
    ###
    ### include pixel font ?! (to spell things)
    ###

    p = ArgumentParser("")
    p.add_argument("--interpolation", default="cubic", help="nearest,cubic")
    p.add_argument("--labels", default="labels_public.txt")
    p.add_argument("--top", default="0.5cm")
    p.add_argument("--left", default="0.5cm")
    p.add_argument("--right", default="0.5cm")
    p.add_argument("--bottom", default="0.5cm")
    p.add_argument("--cols", type=int, default=10)
    p.add_argument("--overlay", default=False, action="store_true")
    p.add_argument("--background", default="255,255,255")
    p.add_argument("--font", default="unifont-12.0.01.ttf")
    p.add_argument("--fontsize", default=94, type=int)
    p.add_argument("--limit", default=None, type=int)
    p.add_argument("--sort", default=False, action="store_true")
    args = p.parse_args()

    booth_height = 204*cm
    booth_back = 92*cm
    booth_side = 45*cm
    booth_strip = 22*cm

    # c.save()

    # TEXT OVERLAYS
    import cv2
    overlay_pixels = []
    for i in range(len(LABELS)):
        print ("label{0}.png".format(i))
        im = cv2.imread("label{0}.png".format(i))
        overlay_pixels.append((im[:,:,0] > 128))
    overlay_index = 0

    # prep font
    #ttfont =  TTFont('MyFontName', font)
    #pdfmetrics.registerFont(ttfont)
    # c.setFont('MyFontName', fontsize)
    
    # print (len(list(item_iter(args.labels))))
    cellsize = 0.975*cm

    cols = int(math.floor(booth_back / cellsize))
    rows = int(math.floor(booth_height / cellsize))
    print ("Back: {0}x{1}: {2}".format(cols, rows, cols*rows))
    left = booth_back - (cols*cellsize)
    totalcells = cols*rows

    cols2 = int(math.floor(booth_side / cellsize))
    rows2 = int(math.floor(booth_height / cellsize))
    print ("Side: {0}x{1}: {2}".format(cols2, rows2, cols2*rows2))
    totalcells += cols2*rows2
    print ("Total cells: {0}".format(totalcells))

    items = sorted_item_iter(args.labels)
    strip_width = 9
    use_strip_width = strip_width
    last_strip = math.floor(cols/strip_width)
    last_strip_width = cols % strip_width
    print ("strips: {0}, {1}".format(last_strip, last_strip_width))

    c = canvas.Canvas("photobooth_fer_back.pdf", pagesize=(booth_back, booth_height))
    #c.setFillColorRGB(255, 255, 255)
    #c.rect(0, 0, booth_back, booth_height, fill=1)
    strip, row, col = 0, 0, 0
    count = 0
    overlay_label = None
    overlay_seek = False
    overlay_seek_count = 0
    overlay_start_row = None
    overlay_strip = None
    page = 0

    try:
        while True:
        # for col in range(cols):
        #     for row in range(rows):

            actual_col = (strip*strip_width)+col
            x = left + (actual_col*cellsize)
            y = booth_height - (row*cellsize) - cellsize

            draw_image = True
            if overlay_start_row != None:
                overlay_width = overlay_pixels[overlay_label].shape[1]
                overlay_length = overlay_pixels[overlay_label].shape[0]
                # print ("Overlay size {0}x{1}, checking {2}x{3}".format(overlay_length, overlay_width, col, row))
                if strip == overlay_strip:
                    if (row >= overlay_start_row and
                        row < overlay_start_row + overlay_length):
                        overlay_row = row - overlay_start_row
                        if col < overlay_width:
                            # print ("SKIP OVERLAY PIXEL")
                            # draw_image = not overlay_pixels[overlay_label][overlay_row][col]
                            draw_image = not overlay_pixels[overlay_label][overlay_row][col]

            if draw_image:

                label, path = next(items)
                if label != overlay_label:
                    print ("Overlay seek")
                    overlay_label = label
                    overlay_seek = True
                    overlay_seek_count = 0
                    
                # image = Image.open(path)
                image = render_emotion_color(path, label)

                c.drawInlineImage(image, x, y, width=cellsize, height=cellsize)
                #color = get_emotion_color(label)
                #c.setFillColor(color)
                #c.rect(x, y, cellsize, cellsize, stroke=0, fill=1)
                print (row, col, path, end="\r")

            count += 1 # count is of cells -- not images

            # Advance position
            col += 1
            if col >= use_strip_width:
                col = 0
                row += 1
                if overlay_seek:
                    overlay_seek_count += 1
                    if overlay_seek_count >= strip_width:
                        overlay_seek = False
                        overlay_start_row = row
                        overlay_strip = strip
                        print ("overlay start", overlay_start_row)
            if row >= rows:
                strip += 1
                row = 0
            # detect END OF PAGE
            if strip == last_strip:
                use_strip_width = last_strip_width
            elif strip >= last_strip:
                if page == 0:
                    print ("Ending page 1 after {0} cells".format(count))
                    c.showPage()
                    c.save()

                    # Start page 2
                    c = canvas.Canvas("photobooth_fer_side.pdf", pagesize=(booth_side, booth_height))

                    # c.setPageSize((booth_side, booth_height))
                    # c.rect(0, 0, booth_side, booth_height, fill=1)

                    left = 0 # booth_side - (cols2*cellsize)

                    strip, row, col = 0, 0, 0
                    rows, cols = rows2, cols2
                    count = 0
                    use_strip_width = strip_width
                    last_strip = math.floor(cols2/strip_width)
                    last_strip_width = cols2 % strip_width

                    print ("page 2 strips: {0}, {1}".format(last_strip, last_strip_width))

    except StopIteration:
        c.showPage()
    c.save()

