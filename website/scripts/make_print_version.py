#!/usr/bin/env python
from PIL import Image, ImageOps
import argparse

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def make_print_version (path, outputpath):
    im = Image.open(path)
    w, h = im.size 
    if w > h:
        im = im.rotate(-90, expand=1)
    im = im.convert("L")
    rw, rh = fit_size(im, (400, 10000))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.equalize(im)
    im = im.convert("1")
    im.save(outputpath)

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("input")
    ap.add_argument("output")
    args = ap.parse_args()
    make_print_version(args.input, args.output)

