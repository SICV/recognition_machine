#!/usr/bin/env python3

import cv2, os, datetime, sys, json
from argparse import ArgumentParser
from time import sleep
import numpy as np

def resize (img, w, h, interpolation = cv2.INTER_CUBIC): # INTER_NEAREST, INTER_CUBIC
    ih, iw, ic = img.shape
    # if (ih > h) or (iw > w):
    # try fitting width
    sw = w
    sh = int(sw * (float(ih)/iw))
    if sh > h:
        # fit height instead
        sh = h
        sw = int(sh * (float(iw)/ih))
    return cv2.resize(img, (sw, sh), interpolation=interpolation)
    # return img

def pad (img, w, h, color=(0, 0, 0)):
    ih, iw, ic = img.shape
    top = (h - ih) // 2
    bottom = h - ih - top
    left = (w - iw) // 2
    right = (w - iw - left)
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

def putTextCentered (img, text, pos, font_scale=1.0, thickness=2, text_color=(255,255,255)):
    (tw, th), baseline = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, font_scale, thickness)    
    cv2.putText(img, text, (pos[0]-(tw//2), pos[1]),
                cv2.FONT_HERSHEY_SIMPLEX, font_scale, text_color, thickness) # cv2.LINE_AA


def putText (img, text, pos, font_scale=1.0, thickness=2, text_color=(255,255,255)):
    cv2.putText(img, text, pos,
                cv2.FONT_HERSHEY_SIMPLEX, font_scale, text_color, thickness) # cv2.LINE_AA


ap = ArgumentParser("")
ap.add_argument("--ferlabels", default="fer2013_labels.npy")
ap.add_argument("--rmemotions", default="rm_emotions.npy")
ap.add_argument("--rmlabels", default="rm_labels.npy")
ap.add_argument("--media", default="media")
ap.add_argument("--output", default="search.avi")
# ap.add_argument("--labels", default="labels_public.txt")
ap.add_argument("--width", type=int, default=1920)
ap.add_argument("--height", type=int, default=1080)
ap.add_argument("--background", default="0,0,0")
ap.add_argument("--text", default="128,128,128")
ap.add_argument("--nopad", default=False, action="store_true")
ap.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
ap.add_argument("--framerate", type=float, default=10, help="output frame rate")
ap.add_argument("--interpolation", default="cubic", help="nearest,cubic")
args = ap.parse_args()

LABELS = {
    0: 'angry',
    1: 'disgust',
    2: 'fear',
    3: 'happy',
    4: 'sad',
    5: 'surprise',
    6: 'neutral'
}

fourcc = None
try:
    fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
except AttributeError:
    fourcc = cv2.VideoWriter_fourcc(*args.fourcc)
text_color = tuple([int(x) for x in args.text.split(",")])
background = tuple([int(x) for x in args.background.split(",")])
# INTERPOLATION = cv2.INTER_NEAREST
# if args.interpolation == "cubic":
#     INTERPOLATION = cv2.INTER_CUBIC


width, height = args.width, args.height
frame = np.zeros((height,width,3), np.uint8)

outpath = args.output
out = cv2.VideoWriter()
out.open(outpath, fourcc, args.framerate, (args.width, args.height))

for i in range(int(args.framerate*1)):
    out.write(frame)

hw = int(args.width/2)

ferlabels = np.load(args.ferlabels)
rmlabels = np.load(args.rmlabels)
rmemotions = np.load(args.rmemotions)

def draw_rm_frame(i, frame, text_color):
    label = rmlabels[i]
    emotions = rmemotions[i]
    face_id, photo_id = label
    with open(os.path.join(args.media, "data/faces/{0:06d}.json".format(face_id))) as inf:
        face_data = json.load(inf)
    with open (os.path.join(args.media, "data/photos/{0:06d}.json".format(photo_id))) as inf:
        photo_data = json.load(inf)
    photo_path = os.path.join(args.media, "photos", photo_data['id'])

    rmimage = cv2.imread(photo_path)
    rmimage = resize(rmimage, hw, hw)
    sh, sw, _ = rmimage.shape
    # blank rhs
    frame[:,hw:] = 0
    # draw image
    dx = (hw - sw) // 2
    dy = (hw - sh) // 2
    frame[dy:dy+sh,dx+hw:dx+hw+sw] = rmimage[:,:]
    pe = np.argmax(emotions)
    caption = "{0}".format(photo_data['id'])
    putTextCentered(frame, caption, (hw+(hw//2), hw + 24), text_color=text_color)

    pee = np.argsort(emotions)
    # print ("pee", pee)
    caption = "{0} ({1: >04.01f}%) {2} ({3: >04.01f}%) {4} ({5: >04.01f}%)".format( \
        LABELS[pee[-1]], emotions[pee[-1]]*100, \
        LABELS[pee[-2]], emotions[pee[-2]]*100, \
        LABELS[pee[-3]], emotions[pee[-3]]*100)
    putTextCentered(frame, caption, (hw+(hw//2), hw + 64), text_color=text_color)

def draw_fer_frame(i, frame, text_color):
    label =ferlabels[i]
    path = os.path.join(args.media, "datasets", "fer2013", "Train", "{0}.jpg".format(i+1))
    ferimage = cv2.imread(path)
    ferimage = resize(ferimage, hw, hw)
    
    # blank LHS
    frame[:,:hw] = 0
    # draw image
    frame[:hw,:hw] = ferimage[:,:]
    caption = "FER2013 {0}/{1}: \"{2}\"".format(i+1, ferlabels.shape[0], LABELS[label])
    putTextCentered(frame, caption, ((hw//2), hw + 36), text_color=text_color)

for i in range(rmemotions.shape[0]):
    draw_fer_frame(i, frame, text_color=text_color)
    draw_rm_frame(i, frame, text_color=text_color)
    out.write(frame)


# frame = cv2.imread(path)
# frame = resize(frame, args.width, args.height)
# if not args.nopad:
#     frame = pad(frame, args.width, args.height, background)
                
# draw_centered_text(frame, '"{0}" {1}/{2}'.format(label_text.upper(), count+1, counts_by_label[label]), args.height - 5)
# out.write(frame)
out.release()










