import os, sys
import cv2
import numpy as np

MIN_FACE_SIZE = 100
IMG_SIZE = 64
DEPTH = 16
K = WIDTH = 8
MARGIN = 0.4

def detect_faces (face_cascade, imgpath):
    frame = cv2.imread(imgpath)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray)
    faces = [x for x in faces if x[2] > MIN_FACE_SIZE]
    # ret=[]
    # for (x,y,w,h) in faces:
    #     #print "face", (x, y, w, h)
    #     ret.append({'x': int(x), 'y': int(y), 'w': int(w), 'h': int(h)})
    return frame, gray, faces

def detect_emotions(emotion_classifier, gray_face):
    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_offsets = (20, 40)
    # fer2013
    # emotion_labels = {0:'angry',1:'disgust',2:'fear',3:'happy', 4:'sad',5:'surprise',6:'neutral'}
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    # img, gray, faces = detect_faces(path)

    try:
        gray_face = cv2.resize(gray_face, (emotion_target_size))
        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        return emotion_prediction
    except Exception as e:
        print ("Exception resizing", e)


if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("--models", default="models")
    ap.add_argument("--emotions", default="fer2013_emotions.npy", help="name of output file")
    ap.add_argument("--labels", default="fer2013_labels.npy", help="name of output file")
    ap.add_argument("--limit", type=int, default=None)
    ap.add_argument("input")
    args = ap.parse_args()

    lpath = os.path.dirname(args.input)

    # firstpass
    with open(args.input) as f:
        line1 = f.readline()
        count = 0
        for line in f:
            path,label = line.split(",")
            if not path.startswith("Train/"):
                break
            imgpath = os.path.join(lpath, path)
            imgid = os.path.splitext(os.path.basename(path))[0]
            count += 1
            assert(count == int(imgid))
    total = count
    print ("{0} total images".format(total))
    

    print ("Loading emotion classifier", file=sys.stderr)
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(args.models, "emotion_model.hdf5"))
    print ("Loaded", file=sys.stderr)

    allemotions = np.zeros((total, 7), 'float32')
    alllabels = np.zeros((total,), 'int8')

    with open(args.input) as f:
        line1 = f.readline()
        count = 0
        for line in f:
            path,label = line.split(",")
            if not path.startswith("Train/"):
                break
            imgpath = os.path.join(lpath, path)
            imgid = os.path.splitext(os.path.basename(path))[0]
            count += 1
            assert(count == int(imgid))

            frame = cv2.imread(imgpath)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # print ("frame.shape", frame.shape)
            # assert (frame.shape == (48, 48, 3))
            # faces= [(0, 0, 48, 48)]
            emotions = detect_emotions(emotion_classifier, gray)
            alllabels[count-1] = label
            if emotions is not None:
                allemotions[count-1] = emotions
                print ("{0}{1}/{2}".format(count, imgpath, total))
            else:
                print ("{0}{1}/{2} NORESULT=0".format(count, imgpath, total))

            if args.limit and count >= args.limit:
                break
    print ("Saving data to {0}".format(args.emotions))
    np.save(args.emotions, allemotions)
    print ("Saving data to {0}".format(args.labels))
    np.save(args.labels, alllabels)


