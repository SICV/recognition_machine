from django.contrib import admin
from .models import IMDBFace, FER2013Face


class IMDBFaceAdmin (admin.ModelAdmin):
    # search_fields = ["title", "text"]
    list_display = ["path", "width", "name", "gender", "dob", "photo_year"]
    #prepopulated_fields = {"slug": ("title",)}
admin.site.register(IMDBFace, IMDBFaceAdmin)

class FER2013FaceAdmin (admin.ModelAdmin):
    # search_fields = ["title", "text"]
    list_display = ["emotion", "path"]
    #prepopulated_fields = {"slug": ("title",)}
admin.site.register(FER2013Face, FER2013FaceAdmin)
