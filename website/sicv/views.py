import os
from django.shortcuts import render, get_object_or_404
from .models import IMDBFace, FER2013Face
from PIL import Image, ImageOps
from django.http import HttpResponse
from django.conf import settings


def faces (request):
    faces = IMDBFace.objects.order_by("name")
    return render(request, "sicv/faces.html", {'faces': faces})

def face (request, face_id):
    face = get_object_or_404(IMDBFace, pk=face_id)
    return render(request, "sicv/face.html", {'face': face})

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def fer2013face_print (request, face_id):
    face = get_object_or_404(FER2013Face, pk=face_id)
    # if request.GET.get("print"):
    im = Image.open(face.path.path)
    im = im.convert("L")
    rw, rh = fit_size(im, (386, 386))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC
    im = ImageOps.equalize(im)
    im = im.convert("1")
    # im = im.convert("1", dither=Image.NONE)

    # im = ImageOps.equalize(im)
    # im = im.convert("P", colors=2)
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response

def fer2013_direct_print (request, training_id):
    fer_path = os.path.join(settings.MEDIA_ROOT, "datasets/fer2013/Train/{0}.jpg".format(training_id))
    im = Image.open(fer_path)
    im = im.convert("L")
    rw, rh = fit_size(im, (386, 386))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC
    im = ImageOps.equalize(im)
    im = im.convert("1")
    # im = im.convert("1", dither=Image.NONE)

    # im = ImageOps.equalize(im)
    # im = im.convert("P", colors=2)
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response
