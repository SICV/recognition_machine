from django.core.management.base import BaseCommand, CommandError
from sicv.models import FER2013Face
from django.conf import settings
import os


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('input')
        parser.add_argument('--reset', default=False, action="store_true")
        parser.add_argument('--limit', type=int, default=1000)

    def handle(self, *args, **options):
        if options['reset']:
            FER2013Face.objects.all().delete()

        with open(options['input']) as f:
            first = True
            count = 0
            for line in f:
                if first:
                    first = False
                    continue
                path, label = line.split(",")
                if path.startswith("Train/"):
                    rpath = os.path.join("datasets/fer2013", path)
                    f = FER2013Face(path=rpath, emotion=label)
                    f.save()
                    count += 1
                    if options['limit'] and count>=options['limit']:
                        break

