from django.core.management.base import BaseCommand, CommandError
from sicv.models import IMDBFace
from django.conf import settings
from scipy.io import loadmat

#  For both the IMDb and Wikipedia images we provide a separate .mat file which can be loaded with Matlab containing all the meta information. The format is as follows:

#     dob: date of birth (Matlab serial date number)
#     photo_taken: year when the photo was taken
#     full_path: path to file
#     gender: 0 for female and 1 for male, NaN if unknown
#     name: name of the celebrity
#     face_location: location of the face. To crop the face in Matlab run

#     img(face_location(2):face_location(4),face_location(1):face_location(3),:))

#     face_score: detector score (the higher the better). Inf implies that no face was found in the image and the face_location then just returns the entire image
#     second_face_score: detector score of the face with the second highest score. This is useful to ignore images with more than one face. second_face_score is NaN if no second face was detected.
#     celeb_names (IMDB only): list of all celebrity names
#     celeb_id (IMDB only): index of celebrity name

# The age of a person can be calculated based on the date of birth and the time when the photo was taken (note that we assume that the photo was taken in the middle of the year):

# [age,~]=datevec(datenum(wiki.photo_taken,7,1)-wiki.dob); 

from datetime import datetime, timedelta

class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('input')
        parser.add_argument('--reset', default=False, action="store_true")
        parser.add_argument('--limit', type=int, default=1000)

    def handle(self, *args, **options):
        if options['reset']:
            IMDBFace.objects.all().delete()

        d = loadmat(options['input'])
        imdb = d['imdb']
        table = imdb[0][0]
        count = 0
        for dob, photo_taken, full_path, gender, name, face_location, face_score, second_face_score, celeb_names, celeb_id in zip (table[0][0], table[1][0], table[2][0], table[3][0], table[4][0], table[5][0], table[6][0], table[7][0], table[8][0], table[9][0]):
            dob_dt = datetime.fromordinal(int(dob)) + timedelta(days=float(dob)%1) - timedelta(days = 366)
            full_path = full_path[0]
            name = name[0]
            if gender == 1.0:
                gender = "male"
            else:
                gender = "female"
            # face_score = float(face_score)
            if face_score == float('-inf'):
                print ("SKIPPING")
                continue

            print (face_score, dob_dt, photo_taken, full_path, gender, name, face_location, celeb_names, celeb_id)
            f = IMDBFace()
            f.face_score = face_score
            f.second_face_score = second_face_score
            f.celeb_names = ", ".join(celeb_names)
            f.name = name
            f.imdb_id = celeb_id
            f.path = full_path
            x1, y1, x2, y2 = [float(x) for x in face_location[0]]
            f.x = int(x1)
            f.y = int(x2)
            f.width = int(x2-x1)
            f.height = int(y2-y1)
            f.gender = gender
            f.photo_year = photo_taken
            f.dob = dob_dt
            f.save()
            count += 1
            if options['limit'] and count>=options['limit']:
                break

