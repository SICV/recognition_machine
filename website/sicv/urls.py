from django.urls import path
from django.conf import settings

from . import views

app_name = "sicv"
urlpatterns = [
    path('faces/', views.faces, name='faces'),
    path('faces/<int:face_id>', views.face, name="face"),
    path('fer2013/<int:face_id>/print', views.fer2013face_print, name='fer2013face_print'),
    path('fer2013/d/<int:training_id>/print', views.fer2013_direct_print, name='fer2013_direct_print'),
]