import os
from django.conf import settings
import cv2
import numpy as np

face_cascade = None
emotion_classifier = None
genderage_classifier=None

def detect_faces (imgpath):
    # eye_cascade = cv2.CascadeClassifier(os.path.join(tpath, 'haarcascade_eye.xml'))
    global face_cascade
    if face_cascade == None:
        print ("Loading frontal face cascade")
        face_cascade = cv2.CascadeClassifier(os.path.join(settings.MODELS, 'haarcascade_frontalface_default.xml'))
        print ("Done")        

    frame = cv2.imread(imgpath)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray)
    faces = [x for x in faces if x[2] > 100]
    # ret=[]
    # for (x,y,w,h) in faces:
    #     #print "face", (x, y, w, h)
    #     ret.append({'x': int(x), 'y': int(y), 'w': int(w), 'h': int(h)})
    return frame, gray, faces

def detect_emotions(gray, faces):
    global emotion_classifier
    if emotion_classifier == None:
        print ("Loading emotion classifier")
        from keras.models import load_model
        emotion_classifier = load_model(os.path.join(settings.MODELS, "emotion_model.hdf5"))
        print ("Loaded")

    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_offsets = (20, 40)
    # fer2013
    # emotion_labels = {0:'angry',1:'disgust',2:'fear',3:'happy', 4:'sad',5:'surprise',6:'neutral'}
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    # img, gray, faces = detect_faces(path)

    dd = []
    for face_coordinates in faces:
        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        gray_face = gray[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except Exception as e:
            print ("Exception resizing", e)
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        d = {}
        for label, score in zip(emotion_labels, emotion_prediction[0]):
            d[label] = float(score)
        print ("got emotion_prediction", d)
        dd.append(d)
    return dd

def sort_emotions (d):
    vals = []
    for key in d:
        vals.append((d[key]*100, key))
    vals.sort(reverse=True)
    return vals

def detect_genderage(img, faces):
    global genderage_classifier

    IMG_SIZE = 64
    DEPTH = 16
    K = WIDTH = 8
    MARGIN = 0.4
    if genderage_classifier == None:
        print ("Loading genderage classifier")
        # from keras.models import load_model
        from keras.utils.data_utils import get_file
        weight_file = os.path.join(settings.MODELS, "weights.28-3.73.hdf5")
        from sicv.wide_resnet import WideResNet
        genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
        genderage_classifier.load_weights(weight_file)
        print ("Loaded")

    img_h, img_w, _ = np.shape(img)

    # detect faces using dlib detector
    # detected = detector(input_img, 1)
    detected = faces
    faces = np.empty((len(detected), IMG_SIZE, IMG_SIZE, 3))

    data = {}
    if len(detected) > 0:
        for i, d in enumerate(detected):
            x, y, w, h = d
            x1, y1, x2, y2 = x, y, x + w + 1, y + h + 1
            xw1 = max(int(x1 - MARGIN * w), 0)
            yw1 = max(int(y1 - MARGIN * h), 0)
            xw2 = min(int(x2 + MARGIN * w), img_w - 1)
            yw2 = min(int(y2 + MARGIN * h), img_h - 1)
            # cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
            # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i, :, :, :] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (IMG_SIZE, IMG_SIZE))

        # predict ages and genders of the detected faces
        results = genderage_classifier.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages_raw = results[1]
        print ("predicted_ages", predicted_ages_raw, predicted_ages_raw.shape)
        predicted_ages = results[1].dot(ages).flatten()
        # print ("predicted_ages", predicted_ages, predicted_ages[0], type(predicted_ages[0]))
        data['ages'] = [float(x) for x in predicted_ages]
        data['genders'] = [{'female': 100*float(x[0]), 'male': 100*float(x[1])} for x in predicted_genders]
        print ("predicted_genderage", data)
        # print ("predicted_genders", predicted_genders)

    return data
