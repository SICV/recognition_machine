from django.db import models
from django.conf import settings
from django.urls import reverse
import os
from urllib.parse import urljoin
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
# from rm.models import Face


class IMDBFace (models.Model):
    # photo = models.ForeignKey(Rephotograph, related_name="faces", on_delete=models.CASCADE)
    GENDER_CHOICES = (
        ('male', 'male'),
        ('female', 'female')
    )

    face_score = models.FloatField()
    second_face_score = models.FloatField(null=True)
    name = models.CharField(max_length=250)
    celeb_names = models.CharField(max_length=1000)
    imdb_id = models.IntegerField(null=True, blank=True)
    path = models.CharField(max_length=255)

    x = models.IntegerField()
    y = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()

    gender = models.CharField(max_length=32, choices=GENDER_CHOICES, blank=True)
    dob = models.DateTimeField()
    photo_year = models.PositiveIntegerField()

    def age (self):
        return self.photo_year - self.dob.year

    def extract_image_from_original(self):
        im = Image.open(os.path.join(settings.IMDB_PATH, self.path))
        return im.crop(box=(self.x, self.y, self.x+self.width, self.y+self.height))

    def image_url (self):
        return "/static/sicv/imdb_crop/" + self.path

    def to_dict (self):
        return {'x': self.x, 'y': self.y, 'width': self.width, 'height': self.height}

"""
https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data

The data consists of 48x48 pixel grayscale images of faces. The faces have been automatically registered so that the face is more or less centered and occupies about the same amount of space in each image. The task is to categorize each face based on the emotion shown in the facial expression in to one of seven categories (0=Angry, 1=Disgust, 2=Fear, 3=Happy, 4=Sad, 5=Surprise, 6=Neutral).

train.csv contains two columns, "emotion" and "pixels". The "emotion" column contains a numeric code ranging from 0 to 6, inclusive, for the emotion that is present in the image. The "pixels" column contains a string surrounded in quotes for each image. The contents of this string a space-separated pixel values in row major order. test.csv contains only the "pixels" column and your task is to predict the emotion column.

The training set consists of 28,709 examples. The public test set used for the leaderboard consists of 3,589 examples. The final test set, which was used to determine the winner of the competition, consists of another 3,589 examples.

This dataset was prepared by Pierre-Luc Carrier and Aaron Courville, as part of an ongoing research project. They have graciously provided the workshop organizers with a preliminary version of their dataset to use for this contest.
 
(0=Angry, 1=Disgust, 2=Fear, 3=Happy, 4=Sad, 5=Surprise, 6=Neutral).

"""

class FER2013Face (models.Model):
    # photo = models.ForeignKey(Rephotograph, related_name="faces", on_delete=models.CASCADE)
    EMOTION_CHOICES = (
        (0, 'angry'),
        (1, 'disgust'),
        (2, 'fear'),
        (3, 'happy'),
        (4, 'sad'),
        (5, 'surprise'),
        (6, 'neutral'),
    )


    @classmethod
    def label_to_code (cls, label):
        for (code, l) in cls.EMOTION_CHOICES:
            if label == l:
                return code

    path = models.ImageField(upload_to="datasets/fer2013/")
    emotion = models.IntegerField(choices=EMOTION_CHOICES)

    faces = GenericRelation("rm.Face")

    # def image_url (self):
    #    return "/static/sicv/imdb_crop/" + self.path

    #def to_dict (self):
    #    return {'x': self.x, 'y': self.y, 'width': self.width, 'height': self.height}
    def get_print_url (self):
        return urljoin(settings.SITE_URL, reverse("sicv:fer2013face_print", args=(self.id, )))
