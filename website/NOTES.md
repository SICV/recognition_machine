## Notes Sep 2020

Preparing new data for Turin. Items always have at most 1 original -- so the possibility to have an item with multiple "original" images is not used. A simplification (which has been implemented in part) has been to add a file field directly to the Item, in effect making the Item equivilent to "Original image".

Imported new images...
Need to check out:
x WARNING: DUPLICATE ORIGINAL 25, ArchSt-L_KE_00025b_9x12.jpg
x WARNING: DUPLICATE ORIGINAL 500, ArchSt-L_KE_00500_9x12.jpg

Is there an Item for 00025b_9x12 Komosso?
Rephotos: 1094 + 1095 are currently both linked to item ArchSt-L_KE_00025b_9x12
Check originals

Done

## Monday 14 September

Testing the matching stuff... done and found the bug (flipped neutral and suprise in the np array).

shape of rm_emotions.npy is (515,7)
and indeed there are 515 faces linked with a Rephotograph




