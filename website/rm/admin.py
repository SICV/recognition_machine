from django.contrib import admin

# Register your models here.
from .models import OriginalImage, Item, Institution, Rephotograph, Page, Face, Post
from django.contrib.contenttypes.admin import GenericTabularInline

# class ResourceAdmin(admin.ModelAdmin):
#     search_fields = ("url", )
#     # list_display = ("url", "update", 'content_type', 'content_length', 'width', 'height', 'duration', 'pages', 'date')
#     list_display = ("url", "update", 'content_type', 'content_length', '_thumbnail', 'og_title', 'lang_en', 'lang_fr', 'lang_nl')
#     list_editable = ("update", )
#     list_filter = ['content_type', 'date']
#     date_hierarchy = "date"
#     raw_id_fields = ("version_of","thumbnail_for", "is_described_by")
#     # inlines = [
#     #     FeedItemInline,
#     # ]



class OriginalImageAdmin(admin.ModelAdmin):
    model = OriginalImage
    list_display = ["id", "file", "item"]

admin.site.register(OriginalImage, OriginalImageAdmin)

class OriginalImageInline(admin.TabularInline):
    model = OriginalImage

class RephotoInline(admin.TabularInline):
    model = Rephotograph

class ItemAdmin (admin.ModelAdmin):
    list_display = ["institution", "code", "title", "url", "file", "photos_count", "originals_count"]
    list_filter = ["institution"]
    search_fields = ["code", "title", "url", "file"]
    inlines = [OriginalImageInline, RephotoInline]

admin.site.register(Item, ItemAdmin)

class InstitutionAdmin(admin.ModelAdmin):
    list_display = ["slug", "name", "website", "archive_url", "items_count"]

admin.site.register(Institution, InstitutionAdmin)

class FaceInline(GenericTabularInline):
    model = Face

class RephotographAdmin(admin.ModelAdmin):
    list_filter = ["visible", "item__institution"]
    list_display = ["visible", "seq", "item", "file", "faces_count"]
    list_display_links = ["seq", "item", "file"]
    search_fields = ["seq", "item__code", "file"]
    inlines = [FaceInline]

admin.site.register(Rephotograph, RephotographAdmin)

class PageAdmin (admin.ModelAdmin):
    search_fields = ["title", "text"]
    list_display = ["slug", "title"]
    prepopulated_fields = {"slug": ("title",)}
admin.site.register(Page, PageAdmin)

class FaceAdmin (admin.ModelAdmin):
    # search_fields = ["title", "text"]
    list_display = ["image", "width", "emotion", "age_label", "age", "gender"]
    #prepopulated_fields = {"slug": ("title",)}
admin.site.register(Face, FaceAdmin)


class PostAdmin (admin.ModelAdmin):
    # search_fields = ["title", "text"]
    date_hierarchy = "time"
    list_display = ["time", "print", "file", "rephoto", "text"]
    list_filter = ["print"]
    list_editable = ["print"]
    #prepopulated_fields = {"slug": ("title",)}
    inlines = [
        FaceInline,
    ]
admin.site.register(Post, PostAdmin)
