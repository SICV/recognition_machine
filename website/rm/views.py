import os, json
import datetime
import math

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.conf import settings
# import cv2
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.auth.decorators import login_required
from django.views.decorators.clickjacking import xframe_options_exempt
from django.db.models import Count
import qrcode
from PIL import Image, ImageOps

from .forms import UploadOriginalImageForm, PostForm, PhotoCommentForm, PostDeleteForm
from .models import Item, OriginalImage, Institution, Rephotograph, Face, Page, Post


PHOTOBOOTH_IMAGE_RESIZE = 640


def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)



@xframe_options_exempt
def home (request):
    """ home page of the site """
    ctx = {}
    ctx['page'] = get_object_or_404(Page, slug='home')
    return render(request, "rm/home.html", ctx)

@xframe_options_exempt
def page (request, page_slug):
    """ sidebar pages """
    ctx = {}
    ctx['page'] = get_object_or_404(Page, slug=page_slug)
    return render (request, "rm/page.html", ctx)

###########################
## MAP
###########################

@xframe_options_exempt
def map (request, institution=None):
    ctx = {}
    if institution:
        ctx['institution'] = get_object_or_404(Institution, slug=institution)
    else:
        ctx['institution'] =  None

    ctx['institutions'] = Institution.objects.all()

    photo_id = request.GET.get("p")
    if photo_id:
        # photo = get_object_or_404(Rephotograph, pk=int(photo_id))
        # sidebarurl = photo.get_info_url()
        photo_id = int(photo_id)
        ctx['sidebarurl'] = reverse('photo_info', args=[photo_id,])
    else:
        ctx['sidebarurl'] = reverse('page', kwargs={'page_slug': 'start'}) # '/' # STARTPAGE reverse("page", args=)


    return render(request, "rm/map.html", ctx)

@xframe_options_exempt
def map_js (request, institution=None):
    photos = query_photos(institution)
    return render(request, "rm/map.js", {'l0_cols': getl0cols(photos.count())}, content_type="text/javascript")

def query_photos (institution=None):
    """
    As the order is important and used multiply with layout,
    important that the query is consistent
    """
    if institution:
        return Rephotograph.objects.filter(item__institution__slug=institution).order_by("file")
    else:
        return Rephotograph.objects.order_by("file")

def layout_photos (photos):
    photos = [x.to_leaflet_dict() for x in photos]
    cols = math.ceil(math.sqrt(len(photos)))
    l0cols = 2**math.ceil(math.log2(cols))
    x, y = 0, 0
    for i in photos:
        i['x'] = x
        i['y'] = y
        x += 1
        if x >= cols:
            x = 0
            y += 1
    return photos

def map_tiles (request, institution=None):
    data = {}
    data['tilewidth'] = 256
    data['tileheight'] = 256
    data['minzoom'] = 0
    data['maxzoom'] = 10
    photos = query_photos(institution)
    data['images'] = layout_photos(photos)
    return HttpResponse(json.dumps(data, indent=2), content_type="application/json")

def getl0cols (count):
    """
    returns
        next whole power of two >= number of cols 
        where cols = ceil(sqrt(count))
    """
    cols = math.ceil(math.sqrt(count))
    return 2**math.ceil(math.log2(cols))

def photo_redirect_to_map (request, photo_id):
    photo = get_object_or_404(Rephotograph, id=photo_id)
    # query = Rephotograph.objects.count()
    photos = query_photos(photo.item.institution)
    l0cols = getl0cols(photos.count())
    photos = layout_photos(photos)
    for p in photos:
        if p['id'] == photo.basename():
            print ("got p", p)
            break

    cw = 256.0 / l0cols
    mapx = p['x']*cw
    mapy = -p['y']*cw
    scaler = 1.0 / max(photo.width, photo.height)
    scaled_image_width = photo.width * scaler * cw
    scaled_image_height = photo.height * scaler * cw
    mapx += (scaled_image_width/2.0)
    mapy -= (scaled_image_height/2.0)
    # return HttpResponseRedirect(reverse('map')+"?p={0}#6/{1}/{2}".format(photo_id, mapy, mapx))
    query = "?p={0}#6/{1}/{2}".format(photo_id, mapy, mapx)
    print (f"query {query}")
    return HttpResponseRedirect(reverse('institution-map', args=[photo.item.institution.slug])+query)

def qr(request):
    page = Page.objects.get(slug="QR")
    return render(request, "rm/qr.html", { 'page': page })

def qrlogo(request):
    im = qrcode.make(settings.SITE_URL)  
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response

def upload(request):
    if request.method == 'POST':
        form = UploadOriginalImageForm(request.POST, request.FILES)
        if form.is_valid():
            # file is saved
            code = form.cleaned_data.get("code")
            if not code:
                files = request.FILES.getlist("file")
                code = os.path.splitext(os.path.basename(files[0].name))[0]
            item, _ = Item.objects.get_or_create(institution=form.cleaned_data.get("institution"), code=code)
            form.instance.item = item
            form.instance.kind = 'inst'
            form.save()
            return HttpResponseRedirect(reverse("index"))
    else:
        form = UploadOriginalImageForm()
    return render(request, 'rm/upload.html', {'form': form})


@xframe_options_exempt
def photo_comment (request, photo_id):
    rephoto = get_object_or_404(Rephotograph, pk=photo_id)
    if request.method == 'POST':
        form = PhotoCommentForm(request.POST, request.FILES)
        form.instance.rephoto = rephoto
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('photo_info', args=(photo_id, )))

@xframe_options_exempt
def index (request):
    ctx = {}
    ctx['items'] = Item.objects.order_by("institution", "code")
    ctx['originals'] = OriginalImage.objects.order_by("file")
    ctx['photos'] = Rephotograph.objects.order_by("file")
    ctx['institutions'] = Institution.objects.order_by("name")
    ctx['linked_photos'] = Rephotograph.objects.filter(item__isnull=False)

    return render(request, "rm/index.html", ctx)


@xframe_options_exempt
def photos (request):
    i = request.GET.get("i")
    if i:
        institution = get_object_or_404(Institution, slug=i)
        photos = Rephotograph.objects.filter(item__institution__slug=i)
    else:
        institution = None
        photos = Rephotograph.objects.all()

    ii = Institution.objects.order_by("name").annotate(Count('items__photos'))
    o = request.GET.get("o", "photo")
    if o == "photo":
        photos = photos.order_by("file")
    elif o == "item":
        photos = photos.order_by("item__code")

    return render(request, "rm/photos.html", {'o': o, 'photos': photos, 'institutions': ii, 'institution': institution})

@xframe_options_exempt
def originals (request):
    items = OriginalImage.objects.order_by("file")
    return render(request, "rm/originals.html", {'items': items})

@xframe_options_exempt
def original (request, original_id):
    item = get_object_or_404(OriginalImage, id=original_id)
    return render(request, "rm/original.html", {'item': item})

@xframe_options_exempt
def original_print (request, original_id):
    item = get_object_or_404(OriginalImage, id=original_id)
    im = Image.open(item.file.path)
    im = im.convert("L")
    # im.thumbnail((400, 10000))
    # im = im.rotate(90, expand=1)
    rw, rh = fit_size(im, (640, 480))
    im = im.resize((rw, rh))

    im = ImageOps.equalize(im)
    im = im.convert("1")
    
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response

###########################
## ITEMS
###########################

def items (request):
    items = Item.objects.order_by("file")
    return render(request, "rm/items.html", {'items': items})

def item (request, item_id):
    """ used as the "original" view"""
    ctx = {}
    ctx['item'] = get_object_or_404(Item, id=item_id)
    ctx['bodytext'] = Page.objects.get(slug="itempage").text
    return render(request, "rm/item.html", ctx)

def item_print (request, item_id):
    """ used as the "original" view"""
    item = get_object_or_404(Item, id=item_id)
    im = Image.open(item.file.path)
    im = im.convert("L")
    # im.thumbnail((400, 10000))
    # im = im.rotate(90, expand=1)
    rw, rh = fit_size(im, (1000, 1000))
    im = im.resize((rw, rh))

    im = ImageOps.equalize(im)
    im = im.convert("1")
    
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response

###########################



def linker (request):
    return render(request, "rm/linker.html", {})

@xframe_options_exempt
def photo (request, photo_id):
    photo = get_object_or_404(Rephotograph, id=photo_id)
    allphotos = Rephotograph.objects.filter(visible=True).order_by("seq")
    prev_item, cur_item, next_item = None, None, None
    cur_index = None
    for i, x in enumerate(allphotos):
        if cur_item != None:
            next_item = x
            break

        if x == photo:
            cur_item = x
            cur_index = i
        else:
            prev_item = x
    
    ctx = {"photo": photo}

    # for testing...
    ctx['page'] = get_object_or_404(Page, slug='start')

    ctx['faces'] = [p.to_dict() for p in photo.faces.order_by("index")]

    ctx['prev_item'] = prev_item
    if prev_item:
        ctx['prev_url'] = reverse("photo", args=(prev_item.id,))
    ctx['next_item'] = next_item
    if next_item:
        ctx['next_url'] = reverse("photo", args=(next_item.id,))
    ctx['cur_index'] = cur_index+1
    ctx['total_items'] = allphotos.count()
    ctx['commentform'] = PhotoCommentForm()
    return render (request, "rm/photo.html", ctx)


@xframe_options_exempt
def photo_info (request, photo_id):
    photo = get_object_or_404(Rephotograph, id=photo_id)
    ctx = {"photo": photo}
    # ctx['mapx'], ctx['mapy'] = photo.get_map_center()
    ctx['commentform'] = PhotoCommentForm()
    return render (request, "rm/photo_info.html", ctx)

@xframe_options_exempt
def photo_print (request, photo_id):
    photo = get_object_or_404(Rephotograph, id=photo_id)
    im = Image.open(photo.file.path)
    im = im.convert("L")
    # im.thumbnail((400, 10000))
    # im = im.rotate(90, expand=1)
    rw, rh = fit_size(im, (400, 10000))
    im = im.resize((rw, rh))

    im = ImageOps.equalize(im)
    im = im.convert("1")
    
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response
    # return HttpResponseRedirect(photo.file.url)

@xframe_options_exempt
def photo_qr (request, photo_id):
    photo = get_object_or_404(Rephotograph, id=photo_id)
    im = qrcode.make(photo.get_public_url(), border=0) # NB: use the PUBLIC URL!  
    response = HttpResponse(content_type="image/png")
    im = im.resize((386, 386), resample=Image.NEAREST)
    im.save(response, "png")
    return response

@xframe_options_exempt
def photo_face (request, photo_id, face_id):
    photo = get_object_or_404(Rephotograph, id=photo_id)
    face = photo.faces.order_by("index")[face_id]
    face.ensure_file()
    # face = Face.objects.get(image=photo, index=face_id)
    return HttpResponseRedirect(face.file.url)

#from PIL import ImageOps

#def make_print_photo (impath):
#    im = Image.open(impath)
#    im.

# def photo_face_print (request, photo_id, face_id):
#     photo = get_object_or_404(Rephotograph, id=photo_id)
#     face = photo.faces.order_by("index")[face_id]
#     face.ensure_file()
#     im = Image.open(face.file.path)
#     im = im.convert("L")
#     # im = ImageOps.equalize(im)
#     im = im.convert("1")
#     # im = im.convert("P", colors=2)
#     response = HttpResponse(content_type="image/png")
#     im.save(response, "png")
#     return response
#     # return HttpResponse()
#     # face = Face.objects.get(image=photo, index=face_id)
#     #return HttpResponseRedirect(face.file.url)

def face_print (request, face_id):
    face = get_object_or_404(Face, id=face_id)
    face.ensure_file()
    # if request.GET.get("print"):
    im = Image.open(face.file.path)
    im = im.convert("L")
    rw, rh = fit_size(im, (400, 400))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.equalize(im)
    im = im.convert("1")
    # im = im.convert("1", dither=Image.NONE)

    # im = ImageOps.equalize(im)
    # im = im.convert("P", colors=2)
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response
    # else:
    #     return HttpResponseRedirect(face.file.url)

def posts (request):
    ctx = {}
    ctx['posts'] = Post.objects.order_by("-time")
    return render(request, "rm/posts.html", ctx)

def posts_print_feed (request):
    http_host = request.META.get("HTTP_HOST")
    print ("POSTS_FEED", http_host)
    limit= int(request.GET.get("limit", "20"))
    # since = request.GET.get("since")
    # if since is not None:
    #     since = since.replace(" ", "+")
    #     since = datetime.datetime.fromisoformat(since)
    #     query = Post.objects.filter(time__gt=since)
    # else:
    query = Post.objects.filter(print=True)
    feed = {}
    feed['posts'] = [x.to_feed_dict() for x in query.order_by("time")[:limit]]
    return HttpResponse(json.dumps(feed, indent=2), content_type="application/json")

def post_printed (request, post_id):
    post = get_object_or_404(Post, id=post_id)
    resp = {}
    http_host = request.META.get("HTTP_HOST")
    # if http_host in ("10.0.0.1",):
    print ("post_printed", http_host)
    if post.print:
        post.print = False
        post.save()
        resp['msg'] = 'ok'
    else:
        resp['msg'] = 'nothing to do'
    return HttpResponse(json.dumps(resp, indent=2), content_type="application/json")

def photostrip (request, post_id):
    ctx = {}
    ctx['post'] = post = get_object_or_404(Post, id=post_id)
 
    psi = post.get_photostrip()
    if request.GET.get("render") != None:
        return render(request, "rm/photostrip.html", psi)
    else:
        return HttpResponse(json.dumps(psi, indent=2), content_type="application/json")

def post_print (request, post_id):
    post = get_object_or_404(Post, id=post_id)
    im = Image.open(post.file.path)
    im = im.convert("L")
    rw, rh = fit_size(im, (400, 400))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.equalize(im)
    im = im.convert("1")
    # im = im.convert("1", dither=Image.NONE)

    # im = ImageOps.equalize(im)
    # im = im.convert("P", colors=2)
    response = HttpResponse(content_type="image/png")
    im.save(response, "png")
    return response

@login_required
def post_delete (request, post_id):
    post = get_object_or_404(Post, id=post_id)
    if request.method == "POST":
        form = PostDeleteForm(request.POST, request.FILES)
        photo = post.rephoto
        post.delete()
        return HttpResponseRedirect(reverse("photo_info", args=(photo.id, )))
    else:
        form = PostDeleteForm(instance=post)
        return render(request, "rm/post_delete.html", { "post": post, "form": form })


def allfaces (request):
    faces = Face.objects.order_by("file")
    return render(request, "rm/allfaces.html", {"faces" : faces})

def jsondump (request):
    # photos = Rephotograph.objects.exclude(faces__isnull=True).order_by("file")
    photos = Rephotograph.objects.order_by("file")
    data = {}
    images = data['images'] = []
    for p in photos:
        faces = [x.to_dict() for x in p.faces.all()]
        images.append({'path': p.file.url, 'width': p.width, 'height': p.height, 'faces': faces})
    return HttpResponse(json.dumps(data, indent=2), content_type="application/json")




def face (request, face_id):
    faceobj = get_object_or_404(Face, id=face_id)
    return render(request, "rm/face.html", {'face': faceobj})

def comments (request):
    i = request.GET.get("i")
    if i:
        institution = get_object_or_404(Institution, slug=i)
        posts = Post.objects.filter(rephoto__item__institution__slug=i).order_by("-time")
    else:
        institution = None
        posts = Post.objects.order_by("-time")

    ii = Institution.objects.order_by("name").annotate(Count('items__photos__posts'))
    return render(request, "rm/comments.html", {'posts': posts, 'institutions': ii, 'institution': institution})



############################################
# PHOTOBOOTH VIEWS

import base64
from io import BytesIO
import cv2
import numpy as np

from .detection import *
from .image_utils import *

from scipy.spatial.distance import cdist


def match_post (post):
    fer_emotions, fer_labels = init_fer_table()
    rm_emotions, rm_labels = init_rm_table()

    face = post.faces.first()

    if face is not None:
        # this is now done live (in generating the photostrip for instance)
        # cd = cdist(fer_emotions, face.get_emotions_np(), metric="cityblock")
        # fer_match_index = np.argmin(cd, axis=0)[0]
        # # ferpath = os.path.join(settings.MEDIA_ROOT, "datasets/fer2013/Train/{0}.jpg".format(fer_match_index+1))
        # fer_id = fer_match_index+1
        # fer_url = os.path.join(settings.MEDIA_URL, "datasets/fer2013/Train/{0}.jpg".format(fer_id))
        cd = cdist(rm_emotions, face.get_emotions_np(), metric="cityblock")
        rm_match_index = np.argmin(cd, axis=0)[0]
        face_id, photo_id = rm_labels[rm_match_index]
        
        post.matching_face = Face.objects.get(pk=face_id)
        post.rephoto = Rephotograph.objects.get(pk=photo_id)
        post.save()
    else:
        # MATCH with a random photo with no matching face
        r = Rephotograph.objects.filter(faces__isnull=True).order_by("?").first()
        post.matching_face = None
        post.rephoto = r
        post.save()


def photobooth (request):
    ctx = {}
    if request.method == "POST":
        # print ("POST")
        try:
            assert(len(request.FILES)== 1)
            f = request.FILES.get("file")
            im = Image.open(f)
            im = image_transpose_exif(im)
            im = im.convert("RGB")
            im.thumbnail((PHOTOBOOTH_IMAGE_RESIZE,PHOTOBOOTH_IMAGE_RESIZE))

            the_post = Post()
            the_post.save()
            im_path = "posts/post_{0:04d}.jpg".format(the_post.id)
            im.save(os.path.join(settings.MEDIA_ROOT, im_path))
            print ("saved image to {}".format(im_path))
            the_post.file = im_path
            the_post.save()

        # form = PostForm(request.POST, request.FILES)
        # if form.is_valid():
        #     form.save()

        #     # open, resize, rotate resave image            
        #     the_post = form.instance
        #     im = Image.open(the_post.file.path)
        #     im_pil = Image.open(the_post.file.path)
        #     im_pil = image_transpose_exif(im_pil)


            post_url = reverse("photobooth_face", args=(the_post.id, ))
            if request.GET.get("format") == "json":
                d = {'post_url' : post_url }
                return HttpResponse(json.dumps(d), content_type="application/json")
            else:
                return HttpResponseRedirect(post_url)
        except Exception as e:
            print ("Exception {}".format(e))
            return HttpResponseRedirect(reverse("photobooth")) 
            
    else:
        ctx['form'] = PostForm()
    return render(request, "rm/photobooth.html", ctx)

def photobooth_test (request):
    ctx = {}
    return render(request, "rm/photobooth_test.html", ctx)

def photobooth_face (request, post_id):
    ctx = {}
    post = ctx['post'] = get_object_or_404(Post, id=post_id)
    if request.method == "POST":
        # clear existing linked faces...
        post.faces.clear()
        # print ("[post_view] detecting faces...")
        ctx['did_face_detection'] = True

        face_detector = init_face_detector()
        emotion_classifier = init_emotion_classifier()

        d = detect_faces(face_detector, post.file)
        classify_emotions(emotion_classifier, post.file, d['faces'])

        if len(d['faces']) > 0:
            f = d['faces'][0]
            face = Face(image=post)
            face.index = 0
            face.x = f['x']
            face.y = f['y']
            face.width = f['width']
            face.height = f['height']
            # faces.append(face)

            face.emotion_angry = f['emotion_prediction_by_label']['angry']
            face.emotion_disgust = f['emotion_prediction_by_label']['disgust']
            face.emotion_fear = f['emotion_prediction_by_label']['fear']
            face.emotion_happy = f['emotion_prediction_by_label']['happy']
            face.emotion_sad = f['emotion_prediction_by_label']['sad']
            face.emotion_surprise = f['emotion_prediction_by_label']['surprise']
            face.emotion_neutral = f['emotion_prediction_by_label']['neutral']
            face.emotion = f['emotion_prediction_label']

            face.save()
            face.save_to_file(force=True, im=f['im'])
        # redirects even if no face is detected...
        next_url = reverse("photobooth_match", args=(post.id, ))
        return HttpResponseRedirect(next_url)
    # else:
    ctx['faces'] = post.faces.all()
    return render(request, "rm/photobooth_face.html", ctx)

def photobooth_match (request, post_id):
    ctx = {}
    post = ctx['post'] = get_object_or_404(Post, id=post_id)
    if request.method == "POST":
        # clear existing linked faces...
        # post.faces.clear()
        # print ("[post_view] detecting faces...")
        force_rematch = request.GET.get("rematch")
        if post.rephoto is None or force_rematch:
            match_post(post)
            post.print = True
            post.save()
        # return JSON with matching face url to cross fade...
        # and next_url of simple portrait view
        # with comment form ??! / archive links
        next_url = reverse('photo', args=(post.rephoto.id,))
        if request.GET.get("format") == "json":
            d = {}
            d['matching_face_url'] = post.matching_face.file.url
            d['next_url'] = next_url
            return HttpResponse(json.dumps(d), content_type="application/json")
        else:
            return HttpResponseRedirect(next_url)
    # else:
    ctx['face'] = post.faces.first()
    return render(request, "rm/photobooth_match.html", ctx)

