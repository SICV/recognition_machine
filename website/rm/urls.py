from django.urls import path
# from django.contrib.staticfiles import views as staticviews
from django.conf import settings
from django.conf.urls.static import static

from . import views

# app_name = "rm"
urlpatterns = [

    path('', views.home, name='home'),
    path('p/<slug:page_slug>/', views.page, name='page'),

    path('m/', views.map, name='map'),
    path('m/map.js', views.map_js, name="map-js"),
    path('m/tiles.json', views.map_tiles, name="map-tiles"),

    path('m/<slug:institution>/', views.map, name='institution-map'),
    path('m/<slug:institution>/map.js', views.map_js, name="institution-map-js"),
    path('m/<slug:institution>/tiles.json', views.map_tiles, name="institution-map-tiles"),

    path('photos/', views.photos, name='photos'), # photos listing by institution, linked to each photo/leaflet

    # path('originals/', views.originals, name='originals'),
    # path('originals/<int:original_id>/', views.original, name='original'),
    # path('originals/<int:original_id>/print', views.original_print, name='original_print'),

    path('items/', views.items, name='items'),
    path('items/<int:item_id>/', views.item, name='item'),
    path('items/<int:item_id>/print', views.item_print, name='item_print'),

    # path('upload/', views.upload, name='upload'),
    # path('qr/logo', views.qrlogo, name='qrlogo'),
    # path('qr/', views.qr, name='qr'),
    # path('linker', views.linker, name='linker'),

    # path('r/<int:photo_id>/f/<int:face_id>/print', views.photo_face_print, name="photo_face_print"),
    # path('r/<int:photo_id>/f/<int:face_id>', views.photo_face, name="photo_face"),
    path('r/<int:photo_id>/comment', views.photo_comment, name="photo_comment"),
    path('r/<int:photo_id>/print', views.photo_print, name="photo_print"),
    path('r/<int:photo_id>/qr', views.photo_qr, name="photo_qr"),
    path('r/<int:photo_id>/info', views.photo_info, name="photo_info"),
    path('r/<int:photo_id>', views.photo_redirect_to_map, name="photo"),

    # path('f/<int:face_id>', views.face_print, name="face_print"),

    path('b/', views.photobooth, name="photobooth"),
    path('o/<int:post_id>/', views.photobooth_face, name="photobooth_face"),
    path('o/<int:post_id>/match', views.photobooth_match, name="photobooth_match"),

    path('o/', views.posts, name="posts"),
    path('o/<int:post_id>/delete', views.post_delete, name="post_delete"),
    path('o/<int:post_id>/ps', views.photostrip, name="photostrip"),
    path('o/<int:post_id>/print', views.post_print, name="post_print"),
    path('o/<int:post_id>/printed', views.post_printed, name="post_printed"),
    path('o/feed/', views.posts_print_feed, name="posts_print_feed"),

    path('faces/', views.allfaces, name="allfaces"),
    path('faces/<int:face_id>/print', views.face_print, name="face_print"),
    path('faces/<int:face_id>', views.face, name="face"),
    # path('photos/', views.allphotos, name="allphotos"),
    path('json/', views.jsondump, name="jsondump"),

    # path('leaflet/', views.leaflet, name="leaflet"),


    path('comments/', views.comments, name="comments"),

    # new testing...
    path('c/', views.photobooth_test, name="photobooth_test"),

    # path('media/<path:path>', staticviews.serve, {'media_root': settings.MEDIA_ROOT}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# NOV 2023 for local install, adding INIT 
if hasattr(settings, 'INIT_CLASSIFIERS_ON_STARTUP') and settings.INIT_CLASSIFIERS_ON_STARTUP:
    from rm.detection import init_all
    init_all()



