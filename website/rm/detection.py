from django.conf import settings
import cv2
# from keras.models import load_model
import numpy as np
from PIL import Image

from .image_utils import *


face_detector = None
emotion_classifier = None
fer_emotions = None
fer_labels = None
rm_emotions = None
rm_labels = None


def init_face_detector ():
    global face_detector
    if face_detector is None:
        face_detector = cv2.dnn.readNetFromCaffe(settings.FACE_DETECTION_PROTO, settings.FACE_DETECTION_MODEL)
    return face_detector

def init_emotion_classifier():
    from keras.models import load_model
    global emotion_classifier
    if emotion_classifier is None:
        emotion_classifier = load_model(settings.EMOTION_CLASSIFIER_MODEL)
    return emotion_classifier

def init_all(verbose=True):
    print ("Initializing face detector...")
    m1 = init_face_detector()
    print ("Initializing emotion classifier...") 
    m2 = init_emotion_classifier()
    return m1, m2

def init_fer_table():
    global fer_emotions, fer_labels
    if fer_emotions is None:
        fer_emotions = np.load(settings.FER_EMOTIONS)
        fer_labels = np.load(settings.FER_LABELS)
    return fer_emotions, fer_labels

def init_rm_table():
    global rm_emotions, rm_labels
    if rm_emotions is None:
        rm_emotions = np.load(settings.RM_EMOTIONS)
        rm_labels = np.load(settings.RM_LABELS)
    return rm_emotions, rm_labels

def detect_faces (face_detector, path, exclude_partial=True):
    ret = {}
    im_pil = Image.open(path)
    im_pil = image_transpose_exif(im_pil)
    im_pil = im_pil.convert("RGB")
    im = pil2cv(im_pil)
    # im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    # imbuf = np.asarray(bytearray(f.read()), dtype=np.uint8)
    # im = cv2.imdecode(imbuf,0)

    (h, w) = im.shape[:2]
    ret['w']= w
    ret['h'] = h
    blob = cv2.dnn.blobFromImage(cv2.resize(im, (300, 300)), 1.0,
        (300, 300), (104.0, 177.0, 123.0))

    face_detector.setInput(blob)
    ret['faces'] = faces = []
    detections = face_detector.forward()
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > settings.FACE_DETECTION_MIN_CONFIDENCE:
            # compute the (x, y)-coordinates of the bounding box
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            if exclude_partial:
                if startX < 0 or startY < 0 or endX > w or endY > h:
                    continue     
            # text = "{:.2f}%".format(confidence * 100)

            f = {}
            f['confidence'] = confidence
            f['x'] = startX
            f['y'] = startY
            f['width'] = endX - startX
            f['height'] = endY - startY

            square(f)
            ensure_in_frame(f, im_pil.size)
            f['im'] = im_pil.crop((f['x'], f['y'], f['x']+f['width'], f['y']+f['height']))

            # output face as base64 image
            # output = BytesIO()
            # crop.save(output, "JPEG", quality=89)
            # f['dataurl'] = "data:image/jpeg;base64,"+base64.b64encode(output.getvalue()).decode()

            faces.append(f)

    # sort faces by confidence (high first)
    faces.sort(key=lambda x: x['confidence'], reverse=True)

    return ret


def classify_emotions(emotion_classifier, path, faces):
    """
    modifies: faces, adding emotion predictions scores
    """
    im_pil = Image.open(path)
    im_pil = image_transpose_exif(im_pil)
    im_pil = im_pil.convert("RGB")
    im = pil2cv(im_pil)
    im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    dd = []
    emotionsarray = np.zeros((len(faces), 7), dtype="float32")
    for i, f in enumerate(faces):
        x1, y1, x2, y2 = f['x'], f['y'], f['x']+f['width'], f['y']+f['height']
        gray_face = im_gray[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except Exception as e:
            print ("Exception resizing", e)
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        emotionsarray[i] = emotion_prediction
        f['emotion_prediction_np'] = emotion_prediction
        f['emotion_prediction'] = sorted(zip(emotion_prediction[0], emotion_labels), reverse=True)
        f['emotion_prediction_by_label'] = {}
        max_score, max_label = None, None
        for score, label in f['emotion_prediction']:
            f['emotion_prediction_by_label'][label] = score 
            if max_score is None or score > max_score:
                max_score, max_label = score, label
        f['emotion_prediction_label'] = max_label
    return emotionsarray