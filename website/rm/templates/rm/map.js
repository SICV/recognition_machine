function init_map (max_zoom) {

var map = L.map('map', {
        maxZoom: 20, // max_zoom || 9,
        minZoom: 0,
        zoom: 4,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});
window.mapo = map;

map.attributionControl.setPrefix("<a target='sidebarframe' href='/p/start/'>The Recognition Machine</a>");

var markers = [];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers),
    comments_layer = L.layerGroup();

var hash = new L.Hash(map);

function show_sidebar() {
    sidebar.style.display = "block";
}

var sidebar = document.getElementById("sidebar"),
    sidebarframe = document.getElementById("sidebarframe");

sidebarframe.addEventListener("load", function () {
    show_sidebar();
})

var wedge = 360/7;
var HUES = {
    "angry": 0,
    "disgust": wedge*1,
    "fear": wedge*2,
    "happy": wedge*3,
    "sad": wedge*4,
    "surprise": wedge*5,
    "neutral": wedge*6
};

function scale_rect (r, scale) {
    r.x *= scale;
    r.y *= scale;
    r.width *= scale;
    r.height *= scale;
}

function center_map (mx, my) {
    var h = window.parent.location.hash,
        m = h.match(/#(\d+)/);
    if (m) {
        // console.log("holding zoom at", m[1]);
        window.parent.location.hash = `#${m[1]}/${my}/${mx}`;
    } else {
        window.parent.location.hash = `#6/${my}/${mx}`;    
    }
}

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);
request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    // L0 cols is the the number of columns in the grid at zoom 0
    var x = 0,
        y = 0,
        l0_cols = {{l0_cols}},
        items = data['images'],
        cell_width = data['tilewidth'],
        cell_height = data['tileheight'],
        colwidth = 256 / l0_cols;

    items.forEach(function (item) {

        item.item = item;
        // Create Markers for faces
        var scale = 1/Math.max(item.imageWidth, item.imageHeight);
        // if (i == 0) { console.log("scale", scale); }
        // clickable area
        var boxWidth = item.imageWidth * scale * colwidth,
            boxHeight = item.imageHeight * scale * colwidth;
        var mox = item.x * colwidth,
            moy = -(item.y * colwidth),
            mx = mox,
            my = moy - boxHeight,
            mw = boxWidth,
            mh = boxHeight,
            onclick = function () {
                // console.log("click", item.url);
                window.open(item.url, "sidebarframe");
                show_sidebar();                
                center_map(mox+(boxWidth/2),moy-(boxHeight/2));
            };

        // Add opaque click layer on top of each image
        L.rectangle([[my, mx], [(my+mh),mx+mw]], {
            stroke: false,
            fill: true, 
            color: "hsl(200,100%,50%)",
            fillOpacity: 0.0,
            weight: 1
        })
        .addTo(map) // was markers_layer
        .bindPopup(onclick);            

        // FACES
        for (var fi=0, fl=item.faces.length; fi<fl; fi++) {
            var face = item.faces[fi];
            // scale to unit box
            scale_rect(face, scale);
            // move origin to bottom left and flip y axis...
            face.y = 1.0 - face.y - face.height;
            // scale to colwidth
            scale_rect(face, colwidth);
            // translate based on item.x, y
            face.x += (item.x*colwidth);
            // face.y = -(item.y*colwidth) - colwidth + face.y;
            face.y += (-item.y - 1)*colwidth
            // console.log("face", face);
            L.rectangle([[face.y, face.x], [(face.y+face.height),face.x+face.width]], {
                stroke: false,
                fill: true, 
                color: "hsl("+HUES[face.emotion]+",50%,66%)",
                fillOpacity: 0.5,
                weight: 1
            })
            .addTo(markers_layer)
            .bindPopup(onclick);            
        }

        // COMMENTS
        if (item.comments) {
            //var commentscount = L.divIcon({className: 'comments-icon', html: item.comments});
            // L.marker([(my+mh), mx], {icon: commentscount}).addTo(markers_layer);
            L.marker([(my+mh-0.3), mx+(colwidth*0.5)])
            .addTo(comments_layer)
            .bindPopup(onclick);
        }


    });
    
    var vt = leafygal.layout(items, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);
    map.addLayer(markers_layer);
    L.control.layers(null, {
        "fer2013 emotions": markers_layer,
        "comments": comments_layer }).addTo(map);

    var inf = document.querySelector("#institutions_filter");
    var llist = document.querySelector(".leaflet-control-layers-list");
    llist.appendChild(inf);
    // ADD INSTITUTION SELECTION DIV
    //var idiv = document.createElement("div");
    //document.createElement("a");

  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();

}