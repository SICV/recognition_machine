from django import template
from .timecode import timecode_fromsecs
import markdown as md 
from django.utils.safestring import mark_safe
import xml.etree.ElementTree as ET
from django.urls import reverse
from urllib.parse import urlencode
# import html5lib


def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + "".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])

register = template.Library()

@register.filter
def timecode(value, default=""):
    if value:
        return timecode_fromsecs(value, alwayshours=True, fractdelim=".")
    else:
        return default

@register.filter
def markdown(value):
    return mark_safe(md.markdown(value))

@register.filter
def classname_for_response_code (value):
    if type(value) != int:
        return ""
    if value >= 100 and value < 200:
        return "label label-info"
    elif value >= 200 and value < 300:
        return "label label-success"
    elif value >= 300 and value < 400:
        return "label label-primary"
    elif value >= 400 or value < 600:
        return "label label-danger"
    else:
        return ""

# https://docs.djangoproject.com/en/2.0/topics/http/urls/#reverse-resolution-of-urls

# @register.filter
# def wraplinks (value):
#     t = html5lib.parseFragment(value, namespaceHTMLElements=False)
#     for a in t.findall(".//a[@href]"):
#         href = a.attrib.get("href")
#         a.attrib['href'] = reverse('search')+"?"+urlencode({"url": href})
#     return innerHTML(t)

import json as JSON
@register.filter
def json (value):
    return JSON.dumps(value)

