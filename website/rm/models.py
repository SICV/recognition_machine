from django.db import models
from django.urls import reverse
import os, math, re
from django.conf import settings
from PIL import Image
# Generic Relationships
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from urllib.parse import urljoin
from sicv.models import FER2013Face
import numpy as np
from collections import OrderedDict


#####################
# 

def splitext2 (base):
    return base.split(".", 1)

def parse_filename (path, split2=True):
    _, basename = os.path.split(path)        
    if split2:
        basename, ext = splitext2(basename)
    else:
        basename, ext = os.path.splitext(basename)
    parts = basename.split("_", 2)
    if len(parts) > 3:
        parts = parts[0], parts[1], "_".join(parts[2:])
    try:
        seq, inst, code = parts
        try:
            seq = int(seq)
        except ValueError:
            seq, _ = seq.split("-", 1)
            seq = int(seq)
        return (seq, inst, code)
    except ValueError:
        print ("Exceptional name {0}".format(basename))
        return None

#####################
# 

class Face (models.Model):
    # photo = models.ForeignKey(Rephotograph, related_name="faces", on_delete=models.CASCADE)
    EMOTION_CHOICES = (
        ('angry', 'angry'),
        ('disgust', 'disgust'),
        ('fear', 'fear'),
        ('happy', 'happy'),
        ('sad', 'sad'),
        ('surprise', 'surprise'),
        ('neutral', 'neutral')
    )
    GENDER_CHOICES = (
        ('male', 'man'),
        ('female', 'woman')
    )
    AGE_CHOICES = (
        ('0,5', 'infant'),
        ('5,12', 'infant'),
        ('12,20', 'adolescent'),
        ('20,35', 'young'),
        ('35,50', 'middle-aged'),
        ('50,65', 'older'),
        ('65,110', 'elderly')
    )

    @classmethod
    def map_age (cls, n, display=False):
        for val, label in cls.AGE_CHOICES:
            low, high = [float(x) for x in val.split(",")]
            # print ("age_map {0} {1} {2} {3}".format(x, low, high, val, label))
            if n >= low and n < high:
                if display:
                    return label
                else:
                    return val

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    image = GenericForeignKey('content_type', 'object_id')

    file = models.FileField(upload_to="photos/faces/", blank=True)
    index = models.IntegerField(null=True)
    x = models.IntegerField()
    y = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()

    age = models.FloatField(null=True, blank=True)
    age_label = models.CharField (max_length=32, choices=AGE_CHOICES, blank=True)

    gender = models.CharField(max_length=32, choices=GENDER_CHOICES, blank=True)
    gender_m = models.FloatField(null=True, blank=True)
    gender_f = models.FloatField(null=True, blank=True)

    emotion = models.CharField(max_length=32, choices=EMOTION_CHOICES, blank=True)
    emotion_angry = models.FloatField(null=True, blank=True)
    emotion_disgust = models.FloatField(null=True, blank=True)
    emotion_fear = models.FloatField(null=True, blank=True)
    emotion_happy = models.FloatField(null=True, blank=True)
    emotion_sad = models.FloatField(null=True, blank=True)
    emotion_surprise = models.FloatField(null=True, blank=True)
    emotion_neutral = models.FloatField(null=True, blank=True)

    def extract_image_from_original(self, max_size=256):
        im = Image.open(self.image.file.path)
        im = im.crop(box=(self.x, self.y, self.x+self.width, self.y+self.height))
        if max_size:
            im.thumbnail((max_size, max_size))
        return im

    def calc_filename (self):
        basename = os.path.basename(self.image.file.path)
        basename, _ = os.path.splitext(basename)
        return "{0}_face{1}.jpg".format(basename, self.index)

    def ensure_file (self):
        self.save_to_file()
        return self.file

    def save_to_file (self, force=False, im=None):
        rname = os.path.join("photos", "faces", self.calc_filename())
        fname = os.path.join(settings.MEDIA_ROOT, rname)
        if force or not os.path.exists(fname):
            if im is None:
                im = self.extract_image_from_original()
            try:
                os.makedirs(os.path.split(fname)[0])
            except OSError:
                pass
            im.save(fname)
            self.file = rname
            self.save()
        if os.path.exists(fname) and self.file != rname:
            self.file = rname
            self.save()
    
    def to_dict (self):
        return {
            'x': self.x,
            'y': self.y,
            'width': self.width,
            'height': self.height,
            'emotion': self.emotion
            # 'age': self.age_label,
            # 'gender': self.gender
        }

    def emotion_diff (self, other):
        if self.emotion_angry ==None or other.emotion_angry == None:
            return None
        ret = 0.0
        ret += abs(other.emotion_angry - self.emotion_angry)        
        ret += abs(other.emotion_disgust - self.emotion_disgust)        
        ret += abs(other.emotion_fear - self.emotion_fear)        
        ret += abs(other.emotion_happy - self.emotion_happy)        
        ret += abs(other.emotion_sad - self.emotion_sad)        
        ret += abs(other.emotion_surprise - self.emotion_surprise)        
        ret += abs(other.emotion_neutral - self.emotion_neutral)        
        return ret

    def match_with_rephotograph (self):
        """ uses the Face.emotion_diff to find matchin Face object linked to a Rephotograph """
        # print ("match_with_rephotograph", self)
        # search for closest match rephotograph based on gender & emotion classification data
        rephotograph_type = ContentType.objects.get(app_label='rm', model='rephotograph')
        min_diff = None
        cur_match = None
        for f in Face.objects.filter(content_type=rephotograph_type):
            cur_diff = self.emotion_diff(f)
            # print ("cur_diff", cur_diff)
            if cur_diff != None and (min_diff == None or cur_diff < min_diff):
                min_diff = cur_diff
                cur_match = f
        return cur_match

    def get_default_url (self):
        return urljoin(settings.SITE_URL, reverse("face", args=(self.id, )))

    def get_print_url (self):
        return urljoin(settings.SITE_URL, reverse("face_print", args=(self.id, )))

    def get_emotions_sorted (self):
        ret = []
        ret.append((self.emotion_angry, "angry"))
        ret.append((self.emotion_disgust, "disgust"))
        ret.append((self.emotion_fear, "fear"))
        ret.append((self.emotion_happy, "happy"))
        ret.append((self.emotion_sad, "sad"))
        ret.append((self.emotion_surprise, "surprise"))
        ret.append((self.emotion_neutral, "neutral"))
        ret.sort(reverse=True)
        return ret

    def get_emotions_dict (self):
        return OrderedDict({
            'angry': self.emotion_angry,
            'disgust': self.emotion_disgust,
            'fear': self.emotion_fear,
            'happy': self.emotion_happy,
            'sad': self.emotion_sad,
            'surprise': self.emotion_surprise,
            'neutral': self.emotion_neutral
        })

    def get_emotions_np (self):
        x = np.array([
            self.emotion_angry,
            self.emotion_disgust,
            self.emotion_fear,
            self.emotion_happy,
            self.emotion_sad,
            self.emotion_surprise,
            self.emotion_neutral
        ], dtype="float32")
        y = np.expand_dims(x, axis=0)
        return y

    def make_caption(self, max_length=32):
        lines = []
        if self.gender:
            if self.gender == "male":
                n = "man"
            else:
                n = "woman"
            title = "\"{0} {1} {2}\"".format(self.emotion, Face.map_age(self.age, display=True), self.get_gender_display())
            lines.append(title.upper())
        if self.gender_m != None:
            lines.append("male({0:0.02f}%)/female({1:0.02f}%)".format(self.gender_m, self.gender_f))
        if self.age:
            lines.append("age {0:0.2f}".format(self.age))
        if self.emotion:
            es = self.get_emotions_sorted()
            lines.append("/".join(["{0}({1:0.02f}%)".format(label, value*100.0) for value, label in es[:2]]))

        if max_length:
            lines = [x[:max_length] for x in lines]

        return "\n".join(lines)

    def make_emotion_caption (self):
        es = self.get_emotions_sorted()
        return (", ".join(["{0} ({1:0.01f}%)".format(label, value*100.0) for value, label in es[:3]]))

    def __str__(self):
        return f"<Face {self.image} {self.x},{self.y} {self.width}x{self.height}>"

class Institution (models.Model):
    slug = models.SlugField(max_length=20)
    name = models.CharField(max_length=200, blank=True)
    website = models.URLField(blank=True)
    archive_url = models.URLField(blank=True)
    text = models.TextField(blank=True)

    class Meta:
        ordering = ["name", "id"]

    def __str__ (self):
        return self.slug

    def items_count (self):
        return self.items.count()
    items_count.short_description = "items"

class Item (models.Model):
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE, related_name="items")
    code = models.CharField(max_length=200)
    title = models.CharField(max_length=200, blank=True)
    url = models.URLField(blank=True)

    date = models.DateTimeField('date published', null=True, blank=True)

    file = models.FileField(upload_to='images/', null=True, blank=True)
    faces = GenericRelation(Face)

    class Meta:
        ordering = ["file", "id"]

    def __str__(self):
        if self.file:
            return os.path.basename(self.file.path)
        else:
            return "{0}:{1}".format(self.institution.slug, self.code)

    def thumbnail (self):
        return "/media/thumbs/" + self.file.name.replace(".o.jpg", ".thumb.jpg")

    # def oimages_count (self):
    #     return self.oimages.count()
    # oimages_count.short_description = "oimages"

    def photos_count (self):
        return self.photos.count()
    photos_count.short_description = "rephotos"

    def originals_count (self):
        return self.oimages.count()
    originals_count.short_description = "originals"

    def display_name (self):
        if self.file:
            base,ext = os.path.splitext(self.file.path)
            return os.path.basename(base)
        else:
            return "{0}:{1}".format(self.institution.slug, self.code)



# IMAGE_KINDS = (
#     ('re', 'rephotograph'),
#     ('inst', 'original from institution')
# )

def replace_ext (f, newext):
    return os.path.splitext(f)[0] + newext




class Rephotograph (models.Model):
    @classmethod
    def getl0cols (kls):
        photos_count = Rephotograph.objects.count()
        cols = math.ceil(math.sqrt(photos_count))
        # l0cols is basically the next whole power of two
        return 2**math.ceil(math.log2(cols))

    visible = models.BooleanField(default=True)
    item = models.ForeignKey(Item, related_name="photos", on_delete=models.SET_NULL, null=True, blank=True)
    file = models.FileField(upload_to="photos/")
    seq = models.IntegerField (null=True, blank=True)
    faces = GenericRelation(Face)

    # fer2013_match = models.ForeignKey(Face, related_name="photo", on_delete=models.SET_NULL, null=True, blank=True)

    width = models.IntegerField(null=True, blank=True)
    height = models.IntegerField(null=True, blank=True)

    # POSITION data (for leaflet map)
    # deprecate these!!!
    mapcol = models.IntegerField(null=True, blank=True)
    maprow = models.IntegerField(null=True, blank=True)
    mapx = models.FloatField(null=True, blank=True)
    mapy = models.FloatField(null=True, blank=True)


    # def get_map_center (self):
    #     l0cols = Rephotograph.getl0cols()
    #     cw = 256.0 / l0cols
    #     mapx = self.mapcol*cw
    #     mapy = -self.maprow*cw
    #     scaler = 1.0 / max(self.width, self.height)
    #     scaled_image_width = self.width * scaler * cw
    #     scaled_image_height = self.height * scaler * cw
    #     mapx += (scaled_image_width/2.0)
    #     mapy -= (scaled_image_height/2.0)
    #     return mapx, mapy

    def display_filename (self):
        ret = os.path.basename(self.file.path)
        ret = re.sub(r"\.o\.jpg$", "", ret)
        return ret

    def faces_count (self):
        return self.faces.count()
    faces_count.short_description = "faces"

    def __str__(self):
        return self.file.name

    def basename (self):
        if self.file:
            return os.path.split(self.file.name)[1]

    def thumbnail (self):
        return "/media/" + self.file.name.replace(".o.jpg", ".thumb.jpg")

    def get_default_url(self):
        return urljoin(settings.SITE_URL, reverse("photo", args=(self.id, )))

    def get_public_url(self):
        if hasattr(settings, 'PUBLIC_SITE_URL'):
            return urljoin(settings.PUBLIC_SITE_URL, reverse("photo", args=(self.id, )))
        else:
            return self.get_default_url()

    def get_info_url(self):
        return urljoin(settings.SITE_URL, reverse("photo_info", args=(self.id, )))

    def get_print_url (self):
        return urljoin(settings.SITE_URL, reverse("photo_print", args=(self.id, )))

    def get_qr_url (self):
        #if hasattr(settings, 'PUBLIC_SITE_URL'):
        #    return urljoin(settings.PUBLIC_SITE_URL, reverse("photo_qr", args=(self.id, )))
        #else:
        return urljoin(settings.SITE_URL, reverse("photo_qr", args=(self.id, )))

    def to_leaflet_dict (self):
        d = {}
        d['tiles'] = "/media/" + self.file.name.replace("photos/", "photos/tiles/") + "/z{z}y{y}x{x}.jpg"
        d['id'] = d['name'] = os.path.basename(self.file.name)
        d['url'] = self.get_info_url()
        d['faces'] = [f.to_dict() for f in self.faces.all()]
        d['imageWidth'] = self.width 
        d['imageHeight'] = self.height
        # d['x'] = self.mapcol
        # d['y'] = self.maprow
        d['comments'] = self.posts.count()
        return d

class OriginalImage (models.Model):
    # rephotograph = models.BooleanField(default=False)
    item = models.ForeignKey(Item, related_name="oimages", on_delete=models.CASCADE)
    # kind = models.CharField(max_length=200, choices=IMAGE_KINDS, help_text="rephoto or original")
    # path = models.CharField(max_length=200)
    file = models.FileField(upload_to='images/')
    faces = GenericRelation(Face)

    def __str__(self):
        return self.file.name

    def thumbnail (self):
        return "/media/" + replace_ext(self.file.name, ".thumb.jpg")

    def faces_count (self):
        return self.faces.count()
    faces_count.short_description = "faces"



class Page (models.Model):
    slug = models.SlugField(max_length=64)
    title = models.CharField(max_length=255)
    text = models.TextField(blank=True)

    def __str__ (self):
        return self.slug

    def get_absolute_url (self):
        return reverse("page", kwargs={'page_slug': self.slug})

class FaceMatch (models.Model):
    rephoto = models.ForeignKey(Rephotograph, on_delete=models.SET_NULL, null=True, blank=True, related_name="matches")
    rephoto_face = models.ForeignKey(Face, on_delete=models.SET_NULL, null=True, blank=True, related_name="matches_as_rephoto")
    other_face = models.ForeignKey(Face, on_delete=models.SET_NULL, null=True, blank=True, related_name="matches_as_other")

from .detection import init_fer_table
from scipy.spatial.distance import cdist

class Post (models.Model):
    class Meta:
        ordering = ['-time']

    visible = models.BooleanField(default=True)
    time = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to="posts/", blank=True)
    text = models.TextField(blank=True)
    matching_face = models.ForeignKey(Face, on_delete=models.SET_NULL, null=True, blank=True)
    rephoto = models.ForeignKey(Rephotograph, on_delete=models.SET_NULL, null=True, blank=True, related_name="posts")
    faces = GenericRelation(Face)

    print = models.BooleanField(default=False)

    def to_feed_dict (self):
        ret = {}
        ret['id'] = self.id
        ret['photostrip_url'] = self.get_photostrip_url()
        ret['photostrip_rendered_url'] = self.get_photostrip_url()+"?render"
        ret['time'] = self.time.isoformat()
        ret['post_print_url'] = urljoin(settings.SITE_URL, reverse("post_printed", args=(self.id, )))
        return ret

    def first_face(self):
        for face in self.faces.order_by("index"):
            return face

    def get_print_url (self):
        return urljoin(settings.SITE_URL, reverse("post_print", args=(self.id, )))

    def get_photostrip_url(self, render=False):
        r = urljoin(settings.SITE_URL, reverse("photostrip", args=(self.id, )))
        if render:
            r += "?render"
        return r

    def get_photostrip(self):
        ps = {}
        ps['items'] = psi = []
        psi.append({'text': 'The Recognition Machine'})
        psi.append({'text': '@ Cinema Nova 2023'})
        # psi.append({'text': self.time.strftime("%d %b %Y %H:%M:%S+UTC")})

        face = self.first_face()
        if face:
            psi.append({'image': face.get_print_url()})
            psi.append({'text': face.make_caption()})
        else:
            psi.append({'image': self.get_print_url()})
            psi.append({'text': "No face detected"})

        if self.matching_face:
            # OLD METHOD
            # Linking dataset image
            # dsface = FER2013Face.objects.filter(emotion=FER2013Face.label_to_code(face.emotion)).order_by("?").first()
            # psi.append({'image': dsface.get_print_url()})
            # psi.append({'text': '"{0}" (FER2013)'.format(face.emotion.upper())})

            fer_emotions, fer_labels = init_fer_table()
            cd = cdist(fer_emotions, face.get_emotions_np(), metric="cityblock")
            fer_match_index = np.argmin(cd, axis=0)[0]
            label = fer_labels[fer_match_index]
            emotion = Face.EMOTION_CHOICES[label][0]
            fer_url = os.path.join(settings.MEDIA_URL, "datasets/fer2013/Train/{0}.jpg".format(fer_match_index+1))
            training_id = fer_match_index+1
            fer_url_print = urljoin(settings.SITE_URL, reverse("sicv:fer2013_direct_print", args=(training_id, )))
            # TODO: create a dynamic print view of these images
            print ("fer_url*", fer_url)
            psi.append({'image': fer_url_print})
            psi.append({'text': f'FER2013 "{emotion}"'})

            # Matching Face
            # option 1: JUST THE FACE
            # psi.append({'image': self.matching_face.get_print_url()})
            # option 2: full frame
            psi.append({'image': self.rephoto.get_print_url()})
            psi.append({'text': self.matching_face.make_caption()})
        else:
            # SHOW FULL REPHOTO
            psi.append({'image': self.rephoto.get_print_url()})
            psi.append({'text': "No face detected"})

        # QR CODE
        if self.rephoto:
            # psi.append({'image': self.rephoto.get_print_url()})
            psi.append({'image': self.rephoto.get_qr_url(), 'type': 'qr'})
            psi.append({'url': self.rephoto.get_public_url()})

        return ps

    def has_uploaded_image (self):
        if self.file:
            try:
                im = Image.open(self.file.path)
                s = max(im.size[0], im.size[1])
                return s <= 640 # views.PHOTOBOOTH_IMAGE_RESIZE
            except Exception as e:
                return False
        return False
