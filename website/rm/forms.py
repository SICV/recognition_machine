from django.forms import ModelForm
from rm.models import OriginalImage, Institution, Post
from django import forms


class UploadOriginalImageForm(ModelForm):
    institution = forms.ModelChoiceField(queryset=Institution.objects.order_by("slug"), required=True)
    code = forms.CharField(max_length=200, required=False)
    class Meta:
        model = OriginalImage
        fields = ['institution', 'file']
    # title = forms.CharField(max_length=50)
    # file = forms.FileField()

class PostForm(ModelForm):
    # institution = forms.ModelChoiceField(queryset=Institution.objects.order_by("slug"), required=True)
    # code = forms.CharField(max_length=200, required=False)
    # age = forms.FloatField()

    # gender_male = forms.FloatField(min_value=0, max_value=1.0,
    #     widget=forms.NumberInput(attrs={'id': 'form_gender_male', 'step': "0.01"}))
    # ['angry','disgust','fear','happy','sad','surprise','neutral']
    #classification_data = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Post
        fields = ['file', 'text']

class PhotoCommentForm(ModelForm):
    # institution = forms.ModelChoiceField(queryset=Institution.objects.order_by("slug"), required=True)
    # code = forms.CharField(max_length=200, required=False)
    # age = forms.FloatField()

    # gender_male = forms.FloatField(min_value=0, max_value=1.0,
    #     widget=forms.NumberInput(attrs={'id': 'form_gender_male', 'step': "0.01"}))
    # ['angry','disgust','fear','happy','sad','surprise','neutral']
    class Meta:
        model = Post
        fields = ['text', 'file']
        widgets = {
            'text': forms.Textarea(attrs={'required': True, 'placeholder': 'Please write something on your journey, your search,\nits emotional impact, or\nwrite a short biography, or\nwrite in the voice\nof the person(s) in the image\n\nUpload a file\na drawing, photo, video, sound'})
        }

    # class Meta:
    #     model = MyModel
    #     widgets = {
    #         'name': forms.TextInput(attrs={'placeholder': 'Name'}),
    #         'description': forms.Textarea(
    #             attrs={'placeholder': 'Enter description here'}),
    #     }

class PostDeleteForm(ModelForm):
    class Meta:
        model = Post
        fields = ['text']
