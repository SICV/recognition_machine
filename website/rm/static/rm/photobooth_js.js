/*
async function load() {
    console.log("load");
    await faceapi.nets.ssdMobilenetv1.loadFromUri('weights');
    console.log("loaded");
    const detections = await faceapi.detectAllFaces(input)
    const detectionsWithExpressions = await faceapi.detectAllFaces(input).withFaceLandmarks().withFaceExpressions()

faceapi.draw.drawDetections(canvas, resizedResults)
// draw a textbox displaying the face expressions with minimum probability into the canvas
const minProbability = 0.05
faceapi.draw.drawFaceExpressions(canvas, resizedResults, minProbability)

}
document.addEventListener("DOMContentLoaded", load);
*/

function q (q, elt) { return (elt||document).querySelector(q); }

var messages = q("#messages"),
    camera = q("#camera"),
    video = q("video"),
    canvas = q("canvas"),
    countdown = q("#countdown"),
    number = q(".number", countdown),
    loaded_models = false,
    scaled_width = 640,
    scaled_height = null,
    form = q("form");


function log (msg) {
    console.log(msg);
    messages.innerHTML = msg;
}

async function load_models () {
    log("loading models...");
    await faceapi.nets.ssdMobilenetv1.loadFromUri(WEIGHTS_URL);
    // await faceapi.nets.tinyFaceDetector.loadFromUri('weights');
    await faceapi.nets.faceLandmark68Net.loadFromUri(WEIGHTS_URL);
    await faceapi.nets.faceExpressionNet.loadFromUri(WEIGHTS_URL);
    log("loaded");
    loaded_models = true;
    // q("#take_picture").disabled = false;    
}

var userMediaConstraints = {
    audio: false,
    video: {
        facingMode: "user"
    }
};

function log (msg) {
    messages.innerHTML = msg;
}

async function go_take_photo () {
    try {
        let stream = await navigator.mediaDevices.getUserMedia(userMediaConstraints);
        await video_set_srcObject_and_wait_until_canplay(video, stream);

        video.style.display = "block";
        scaled_height = video.videoHeight / (video.videoWidth/scaled_width);
        canvas.width = scaled_width;
        canvas.height = scaled_height;


        log(`photomaton: scaled size from video: ${scaled_width}x${scaled_height}`);

        await wait (1000);
        number.innerHTML = "3";
        countdown.style.display = "flex";
        await wait (1000);
        number.innerHTML = "2";
        await wait (1000);
        number.innerHTML = "1";
        await wait (1000);

        canvas.getContext('2d').drawImage(video, 0, 0, scaled_width, scaled_height);
        close_stream(stream);
    } catch (error) {
        log("getUserMedia(), exception: " + error.toString());
    }        
    canvas.style.display = "block";
    video.style.display="none";
    countdown.style.display = "none";
}


function post (canvas, form, data) {
    return new Promise((resolve, reject) => {
        canvas.toBlob(function (blob) {
            console.log("blob", blob);
            var csrf_token = form.querySelector("input[name='csrfmiddlewaretoken']").value,
                formData = new FormData();
            formData.append("file", blob, new Date().toISOString()+".jpg");
            formData.append("classification_data", JSON.stringify(data) );
            formData.append("csrfmiddlewaretoken", csrf_token);
            var xhr = new XMLHttpRequest;
            xhr.open( "POST", form.action );
            xhr.send(formData);
            xhr.addEventListener("load", function () {
                resolve(xhr.responseText);
                // console.log("post.success");
            }); 
            xhr.addEventListener("error", function() {
                reject();          
            });
            xhr.addEventListener("progress", function updateProgress (oEvent) {
              if (oEvent.lengthComputable) {
                var percentComplete = oEvent.loaded / oEvent.total * 100;
                console.log("post: percentComplete", percentComplete);
              } else {
                // Unable to compute progress information since the total size is unknown
              }
            })
        }, "image/jpeg", 0.95);

    })
}

async function loaded () {
    let detections;
    log("loaded");
    await wait(3);
    log("loading models...");
    await load_models();

    while (true) {
        log("taking photo...");
        await go_take_photo();
        detections = [];
        log("detecting faces");
        await wait(10);
        log("detecting faces...");
        detections = await faceapi
                .detectAllFaces(canvas)
                .withFaceExpressions();
        console.log("detections", detections);
        if (detections.length > 0) {
            break;
        }
        log("trying again...");
        await wait(2);
    }
    
    // const detections = await faceapi.ssdMobilenetv1(canvas, { minConfidence: 0.1 })
    // const detections = await faceapi.tinyFaceDetector(canvas, {inputSize: 416, scoreThreshold: 0.5});

    log("detected faces..."+detections.length);

    // const displaySize = { width: input.width, height: input.height }
    // const resizedResults = faceapi.resizeResults(detectionsWithExpressions, displaySize)
    // const resizedResults = detections;

    faceapi.draw.drawDetections(canvas, detections);
    // faceapi.draw.drawFaceLandmarks(canvas, detections);
    // await wait(2000);
    const minProbability = 0;
    faceapi.draw.drawFaceExpressions(canvas, detections, minProbability);
    // convert detections to post data...
    var post_data = {},
        post_faces = [];
    post_data.faces = post_faces;
    for (var i=0; i<detections.length; i++) {
        var box = detections[i].detection._box;
        post_faces.push({
            x: box._x,
            y: box._y,
            width: box._width,
            height: box._height,
            emotion: detections[i].expressions
        });
    }
    var response = JSON.parse(await post(canvas, form, post_data));
    console.log("response", response);
    //  window.location = response.post_url;
}

document.addEventListener("DOMContentLoaded", loaded);
