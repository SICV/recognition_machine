function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function q (q, elt) { return (elt||document).querySelector(q); }
function log (msg) {
    console.log(msg);
    step_text.innerHTML = msg;
}


///////////////////////////////////////////
// PHONE VERSION

var form = q("form"),
    next_button = q("#next_button"),
    file_input = q("input#file"),
    img = q("img.full"),
    step_text = q("#step_text"),
    progress = q("progress");

form.onsubmit = function (e) {
    progress.removeAttribute("value");
}

function upload_form(form) {
    var oReq = new XMLHttpRequest();
    oReq.onload = function (e) {
        console.log("loaded", oReq);
        var resp = JSON.parse(oReq.responseText);
        window.location = resp.post_url;
    };
    oReq.open("post", form.action);
    oReq.send(new FormData(form));
    oReq.onprogress = function (e) {
        console.log("progress", e);
        var progressBar = document.querySelector("progress");  
        if (e.lengthComputable) {
            console.log("progress", e.loaded)
            progressBar.max = e.total;  
            progressBar.value = e.loaded;  
        }  
    }
}

file_input.addEventListener("change", x=> {
    if (file_input.value) {
        // upload
        upload_form(form);
        // next_button.disabled = false;
        // img.src = "{%static 'rm/camera_OK.png' %}";
    }
});


///////////////////////////////////////////

const csrftoken = getCookie('csrftoken'),
    userMediaConstraints = {
        audio: false,
        video: {
            facingMode: "user"
        }
    };

var camerafull = q("#camerafull"),
    camera = q("#camera"),
    video = q("video"),
    canvas = q("canvas"),
    countdown = q("#countdown"),
    number = q(".number", countdown),
    scaled_width = 640,
    scaled_height = null;

async function go_take_photo () {
    try {
        let stream = await navigator.mediaDevices.getUserMedia(userMediaConstraints);
        await video_set_srcObject_and_wait_until_canplay(video, stream);

        video.style.display = "block";

        log(`photomaton: scaled size from video: ${scaled_width}x${scaled_height}`);

        await wait (1000);
        number.innerHTML = "3";
        countdown.style.display = "flex";
        await wait (1000);
        number.innerHTML = "2";
        await wait (1000);
        number.innerHTML = "1";
        await wait (1000);

        scaled_height = video.videoHeight / (video.videoWidth/scaled_width);
        canvas.width = scaled_width;
        canvas.height = scaled_height;
        canvas.getContext('2d').drawImage(video, 0, 0, scaled_width, scaled_height);
        close_stream(stream);
    } catch (error) {
        log("getUserMedia(), exception: " + error.toString());
        return false;
    }        
    canvas.style.display = "block";
    video.style.display="none";
    countdown.style.display = "none";
    return true;
}

async function canvas_to_blob (canvas, mime_type, quality) {
    return new Promise((resolve) => {
        canvas.toBlob(resolve, mime_type, quality);
    });
}

async function post (canvas, form, data) {
    // + "?format=json" now in form itself (used for both paths)
    const request = new Request(
        form.action,
        {headers: {'X-CSRFToken': csrftoken}}
    );
    var blob = await canvas_to_blob(canvas, "image/jpeg", 0.95);
    var formData = new FormData();
    formData.append("file", blob, new Date().toISOString()+".jpg");
    // formData.append("format", "json");
            // formData.append("classification_data", JSON.stringify(data) );
            // var csrf_token = form.querySelector("input[name='csrfmiddlewaretoken']").value,  
            //formData.append("csrfmiddlewaretoken", csrf_token);

    var resp = await fetch(request, {
        method: 'POST',
        mode: 'same-origin',  // Do not send CSRF token to another domain.
        body: formData
    });
    resp = await resp.json();
    return resp;

            // var xhr = new XMLHttpRequest;
            // xhr.open( "POST", form.action );
            // xhr.send(formData);
            // xhr.addEventListener("load", function () {
            //     resolve(xhr.responseText);
            //     // console.log("post.success");
            // }); 
            // xhr.addEventListener("error", function() {
            //     reject();          
            // });
            // xhr.addEventListener("progress", function updateProgress (oEvent) {
            //   if (oEvent.lengthComputable) {
            //     var percentComplete = oEvent.loaded / oEvent.total * 100;
            //     console.log("post: percentComplete", percentComplete);
            //   } else {
            //     // Unable to compute progress information since the total size is unknown
            //   }
            // })

}

async function start_camera () {
    await wait(1);
    var result = await go_take_photo();
    // console.log("go_take_photo, result:", result);
    if (result) {
        // make the progress bar active/indeterminate
        progress.removeAttribute("value");
        step_text.innerHTML = "Uploading photo...";
        // console.log("posting canvas using form");
        var resp = await post(canvas, form);
        //console.log("got response", resp);
        window.location = resp.post_url;
    }
}
console.log("20200914: 10:22 photobooth.js");
function init () {
    // return;
    if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        console.log("[photobooth] desktop mode");
        
        form.style.display = "none";
        camerafull.style.display = "block";
        start_camera();
    } else {
        console.log("[photobooth] phone mode");
    }
}

progress.value = 0;
next_button.disabled = true;
document.addEventListener("DOMContentLoaded", init);
