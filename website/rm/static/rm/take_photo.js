function wait (t) {
    return new Promise(resolve => {
        window.setTimeout(resolve, t)
    })
}

function once (object, event_name, callback) {
    var event_handler = function () {
        console.log("event_handler", event_name);
        object.removeEventListener(event_name, event_handler);
        callback();
    }
    object.addEventListener(event_name, event_handler);
}
function video_set_srcObject_and_wait_until_canplay (video, srcObject) {
    // resolve when video canplay
    return new Promise((resolve, reject) => {
        once(video, "canplay", resolve);
        once(video, "error", reject);
        video.srcObject = srcObject;
        video.play();
    })
}
function close_stream (stream) {
    stream.getTracks().forEach(function(track) {
        try {
          track.stop();
          log("called track.stop");
        } catch (error) {
          log("track.stop() threw an exception: "+error.toString());
        }
    });
}

async function take_photo (opts) {
    var userMediaConstraints = {
            audio: false,
            video: {
                facingMode: "user"
            }
        },
        video = opts.video,
        canvas = opts.canvas,
        scaled_width = opts.width || 320,
        scaled_height = null;

    function log (msg) {
        console.log(msg);
        if (opts.on_message) { opts.on_message.call(this, msg); }
    }


    try {
        let stream = await navigator.mediaDevices.getUserMedia(userMediaConstraints);
        // console.log("got usermedia", stream);
        if (!video) {
            console.log("creating video element");
            video = document.createElement("video");
        }
        if (!canvas) {
            console.log("creating canvas element");
            canvas = document.createElement("canvas");
        }
        console.log("1");
        if (opts.on_preview) {
            console.log("on_preview", video);
            opts.on_preview.call(this, video);
        }
        await video_set_srcObject_and_wait_until_canplay(video, stream);
        console.log("2");
        scaled_height = video.videoHeight / (video.videoWidth/scaled_width);
        canvas.width = scaled_width;
        canvas.height = scaled_height;

        log(`photomaton: scaled size from video: ${scaled_width}x${scaled_height}`);
        await wait (1000);

        log("3...");
        await wait (1000);
        log("2...");
        await wait (1000);
        log("1...");
        await wait (1000);
        log("taking picture");

        var context = canvas.getContext('2d');
        context.drawImage(video, 0, 0, scaled_width, scaled_height);

        // stream.getTracks().forEach(function(track) {
        //     try {
        //       track.stop();
        //       log("called track.stop");
        //     } catch (error) {
        //       log("track.stop() threw an exception: "+error.toString());
        //     }
        // });
        return true;
        // return canvas.toDataURL('image/png');
    } catch (error) {
        log("getUserMedia(), exception: " + error.toString());
    }        
}
