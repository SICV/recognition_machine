from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, Face, FaceMatch
from django.conf import settings
# from sicv.models import FER2013Face
import os, json

"""
Add comparison / match step with fer2013
Ensure item created for matching SICV/Fer2013 Face

"""

class Command(BaseCommand):
    help = 'Dump data to id based files'

    def add_arguments(self, parser):
        parser.add_argument('--path', default="media/data")

    def handle(self, *args, **options):
        
        path = os.path.join(options['path'], "photos")
        try:
            os.makedirs(path)
        except OSError:
            pass
        for p in Rephotograph.objects.all():
            ppath = os.path.join(path, "{0:06d}.json".format(p.id))
            with open (ppath, "w") as f:
                json.dump(p.to_leaflet_dict(), f, indent=2)

        # REPHOTOS WITH NO FACES
        path = os.path.join(path, "nofaces.json")
        nofaces = []
        for p in Rephotograph.objects.filter(faces__isnull=True):
            nofaces.append(p.id)
        with open (path, "w") as fout:
            json.dump(nofaces, fout)

        path = os.path.join(options['path'], "faces")
        try:
            os.makedirs(path)
        except OSError:
            pass
        # really only want faces of rephotographs
        for face in Face.objects.all():
            if isinstance(face.image, Rephotograph): 
                fpath = os.path.join(path, "{0:06d}.json".format(face.id))
                with open (fpath, "w") as f:
                    json.dump(face.to_dict(), f, indent=2)

