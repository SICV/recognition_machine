from django.core.management.base import BaseCommand, CommandError
from rm.models import Face


class Command(BaseCommand):
    help = 'ensure faces have files'

    # def add_arguments(self, parser):
    #     parser.add_argument('institution')

    def handle(self, *args, **options):
        # inst = options['institution']
        #inst = Institution.objects.get(slug=inst)

        for f in Face.objects.filter(file=""):
            print (f.id)
            f.ensure_file()

        # item, _ = Item.objects.get_or_create(institution=inst, code=code)
