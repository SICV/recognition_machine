from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, parse_filename
from django.conf import settings
from shutil import copyfile
import os


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        print ("media root", settings.MEDIA_ROOT)
        for p in Rephotograph.objects.order_by("file"):
            print (p, p.get_default_url())
            # ITEM
            if not p.item:
                print("    NO ITEM", file=self.stderr)
                ii = list(Institution.objects.order_by("name"))
                print ("    0. Skip creating item")
                for i, n in enumerate(ii):
                    print ("    {0}. {1}".format(i+1, n.name))
                instnum = int(input("    Enter institution of item:"))
                if instnum == 0:
                    continue
                item = Item(institution=ii[instnum-1])
                item.code = input("    Code?")
                item.save()
                p.item = item
                p.save()
            else:
                print("    has item {0}".format(p.item))

            # ITEM URL
            if not p.item.url:
                print ("    MISSING URL")
                url = input("    url:")
                if url != "":
                    p.item.url = url
                    p.item.save()
            else:
                print ("    has url {0}".format(p.item.url))

            if not p.item.file:
                print ("    MISSING ITEM FILE")
                while True:
                    ifile = input ("    file:")
                    if ifile != "":
                        if os.path.exists(os.path.join(settings.MEDIA_ROOT, ifile)):
                            p.item.file = ifile
                            p.item.save()
                            break
                        else:
                            print ("    FILE NOT FOUND")
                    if ifile == "":
                        break
            else:
                print ("    has file {1}".format(p.item, p.item.file))

            # print ("r/{0}\t{1}\t{2}\t{3}".format(p.id, p.item, p.item.file, p.item.url))
            print ()