from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph
from django.conf import settings
import os


def splitext2 (base):
    return base.split(".", 1)

def parse_filename (path):
    _, basename = os.path.split(path)
    basename, ext = splitext2(basename)
    parts = basename.split("_", 2)
    if len(parts) > 3:
        parts = parts[0], parts[1], "_".join(parts[2:])
    try:
        seq, inst, code = parts
        try:
            seq = int(seq)
        except ValueError:
            seq, _ = seq.split("-", 1)
            seq = int(seq)
        return (seq, inst, code)
    except ValueError:
        print ("Exceptional name {0}".format(basename))
        return None

INST = {
    'WereldculturenRMV': 'RMV',
    'Wereldculturen': "RMV"
}

class Command(BaseCommand):
    help = 'create items for rephotos that miss one'

    # def add_arguments(self, parser):
    #     parser.add_argument('institution')

    def handle(self, *args, **options):
        # inst = options['institution']
        #inst = Institution.objects.get(slug=inst)

        for r in Rephotograph.objects.filter(item__isnull=True):
            print (r.file.path)
            parse = parse_filename(r.file.path)
            if parse == None:
                print ("CANNOT PARSE FILENAME, SKIPPING", r.file.path, file=self.stderr)
                continue
            seq, inst, code = parse
            inst_slug = INST[inst]
            inst = Institution.objects.get(slug=inst_slug)
            item = Item(institution=inst)
            item.code = code
            item.save()

            if r.seq != seq:
                r.seq = seq
            r.item = item
            r.save()


        # item, _ = Item.objects.get_or_create(institution=inst, code=code)
