from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, parse_filename
from django.conf import settings
from shutil import copyfile
import os, re

II = {
    "rmv": 1,
    "africa": 2,
    "prm": 3,
    "joest": 4
}

class Command(BaseCommand):
    help = """
    GO through all the rephotos...
    Scan for filename for one of the institution keys above...
    Link to the related Institution (DB_ID's are the values)
    
    Match the filename on an institution specific pattern...
    Extracting sequence and code if possible
    if code : get_or_create Item with institution + code and link to rephoto
    else: create a new Item with the institution only and link to rephoto

    Effects all rephotos whose file contains a key
    (this seems potentially dangerous)

    """

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        ii = list(Institution.objects.order_by("name"))
        for p in Rephotograph.objects.order_by("file"):
            print (p, p.get_default_url())
            # ITEM
            if not p.item:
                print("    NO ITEM", file=self.stderr)
                i = None
                for key in II:
                    if key in p.file.path.lower():
                        i = Institution.objects.get(pk=II[key])
                        print ("USING", i)
                        break
                if i != None:
                    filename = os.path.split(p.file.path)[1]
                    seq, code = None, None
                    if key == "prm":
                        m = re.search("(\d+)_PRM_1998_(.+)\.o\.jpg$", filename)
                        if m:
                            seq, code = m.groups()
                    elif key == "joest":
                        m = re.search(r"(\d+)_Rautenstrauch_Joest_(.+)\.o\.jpg$", filename)
                        if m:
                            seq, code = m.groups()
                    elif key == "africa":
                        m = re.search(r"(\d+)_Africamuseum_(.+)\.o\.jpg$", filename)
                        if m:
                            seq, code = m.groups()
                    else:
                        m = re.search(r"(\d+)_WereldculturenRMV_(.+)\.o\.jpg", filename)
                        if m:
                            seq, code = m.groups()
                    if code != None:
                        item, created = Item.objects.get_or_create(institution=i, code=code)
                        item.save()
                    else:
                        item = Item(institution=i)
                        item.save()
                    p.item = item
                    p.save()
                    
                # item = Item(institution=ii[instnum-1])
                # item.code = input("    Code?")
                # item.save()
                # p.item = item
                # p.save()
            print ()