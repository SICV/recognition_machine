from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph
from django.conf import settings
import os


def merge_items (into_item, item):
    # merge item -> into_item
    # deletes item
    # copy oimages, photos
    for o in item.oimages.all():
        print ("Move OriginalImage {0} to {1}".format(o, into_item))
        o.item = into_item
        o.save()

    for p in item.photos.all():
        print ("Move Rephotograph {0} to {1}".format(p, into_item))
        p.item = into_item
        p.save()
        # item.photos.remove(p)
        # into_item.photos.add(p)

    changed = False
    if not into_item.title and item.title:
        into_item.title = item.title
        changed = True
    if not into_item.url and item.url:
        into_item.url = item.url
        changed = True
    if changed:
        into_item.save()
    item.delete()

class Command(BaseCommand):
    help = """
    Merge items with the duplicate codes,
    Taking care to move linked Rephotos to the new merged item

    Takes no arguments
    """

    # def add_arguments(self, parser):
    #     parser.add_argument('institution')

    def handle(self, *args, **options):
        # inst = options['institution']
        #inst = Institution.objects.get(slug=inst)
        last_code = None
        last_id = None
        runs = []
        merges = []
        for i in Item.objects.order_by("code"):
            if i.code == last_code:
                # print ("REPEAT", i.code, i.id, last_id)
                if len(runs) == 0:
                    runs.append(last_id)
                runs.append(i.id)
            else:
                if len(runs) > 0:
                    # print (last_code, runs)
                    merges.append((last_code, runs))
                    runs = []
            last_code = i.code
            last_id = i.id
        for code, ids in merges:
            print (code, ids)
            keep_item = Item.objects.get(pk=ids[0])
            for oid in ids[1:]:
                other_item = Item.objects.get(pk=oid)
                merge_items(keep_item, other_item)