from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, parse_filename
from django.conf import settings
from shutil import copyfile
import os, re, cv2


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('--dry-run', action="store_true", default=False)

    def handle(self, *args, **options):
        for r in Rephotograph.objects.all():
            if r.file:
                if " " in r.file.name:
                    old_name = r.file.name
                    new_name = old_name.replace(" ", "_")
                    os.rename(os.path.join(settings.MEDIA_ROOT, old_name), os.path.join(settings.MEDIA_ROOT, new_name))
                    r.file = new_name
                    r.save()
                    print (f"{r.id} {old_name} -> {new_name}")
