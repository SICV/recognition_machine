from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, parse_filename
from django.conf import settings
from shutil import copyfile
import os, re, cv2
from copy import copy

from .patterns import patterns

"""
mc: rephotos:
1080_ArchSt-L_KE_00196_13x18
1220_ARCH-ST_KEA-5762588
originals:
ARCH-ST_KEA-100565
ArchSt-L_KE_00008_13x18 Karoli
ArchSt-L_KE_00419_9x12 Ngùru
"""

# "Missione della Consolata"

class Command(BaseCommand):
    help = """
    Holistic importer (matching rephotographs + Item/Originals on import)

    Last version: 2020-09-12 (turin)
    """

    def add_arguments(self, parser):
        parser.add_argument('--institution', default="MC")
        parser.add_argument('rephotos')
        parser.add_argument('originals')
        parser.add_argument('--dry-run', action="store_true", default=False)

    def handle(self, *args, **options):
        inst_code = options['institution']
        inst = Institution.objects.get(slug=inst_code)

        # process rephotos
        rephotos = []
        rephoto_pat = re.compile(patterns[inst_code]['rephoto'])
        for filename in sorted(os.listdir(options['rephotos'])):
            m = rephoto_pat.search(filename)
            if m:
                d = m.groupdict()
                d['filename'] = filename
                d['code'] = d['code'].strip("-_")
                d['original'] = int(d['original'])
                d['seq'] = int(d['seq'])
                rephotos.append(d)
            else:
                print (f"WARNING {filename} doesn't match rephoto match, ignoring")

        # process originals
        originals = []
        originals_by_id = {}

        original_pat = re.compile(patterns[inst_code]['original'])
        for filename in sorted(os.listdir(options['originals'])):
            m = original_pat.search(filename)
            if m:
                d = m.groupdict()
                d['filename'] = filename
                d['code'] = d['code'].strip("-_")
                d['original'] = int(d['original'])
                originals.append(d)
                if d['original'] in originals_by_id:
                    print (f"WARNING: DUPLICATE ORIGINAL {d['original']}, {filename}")
                originals_by_id[d['original']] = d
            else:
                print (f"WARNING {filename} doesn't match original pattern, ignoring")


        used_originals = {}
        unused_originals = copy(originals_by_id)

        for x in rephotos:
            # look for matching original
            oid = x['original']
            o = originals_by_id.get(oid)
            if not o:
                print (f"Missing original ({oid}) for {x['filename']}")
            else:
                # x['original'] = o
                used_originals[oid] = True
                if oid in unused_originals:
                    del unused_originals[oid]

        used_originals = used_originals.keys()
        print (f"Used originals ({len(used_originals)})")
        for x in sorted(used_originals):
            print (x)
        print ()
        unused_originals = unused_originals.keys()
        print (f"Unused originals ({len(unused_originals)})")
        for x in sorted(unused_originals):
            print (x)

        # Create Items for each used original
        items_by_original_id = {}
        print ("Creating items for used originals")
        for oid in used_originals:
            x = originals_by_id[oid]
            srcpath = os.path.join(options['originals'], x['filename'])
            destpath = os.path.join(settings.MEDIA_ROOT, "images", inst_code, x['filename'])

            if os.path.exists(destpath):
                print (f"Item for {x['filename']} already exists, skipping")
                continue
            print (f"Creating Item for {x}")
            i = Item()
            i.institution = inst
            i.code = x['original']
            print (f"copying {srcpath} -> {destpath}")
            if not options['dry_run']:
                os.makedirs(os.path.split(destpath)[0], exist_ok=True)
                copyfile(srcpath, destpath)

            i.file = os.path.relpath(destpath, settings.MEDIA_ROOT)
            print ("code", i.code)
            print ("institution", i.institution)
            print ("file", i.file)
            print ()
            if not options['dry_run']:
                i.save()

            if x['original'] not in items_by_original_id:
                items_by_original_id[x['original']] = i

        # Create Rephotos with linked items
        for x in rephotos:
            srcpath = os.path.join(options['rephotos'], x['filename'])
            destpath = os.path.join(settings.MEDIA_ROOT, "photos", x['filename'])

            if os.path.exists(destpath):
                print (f"Rephoto for {x['filename']} already exists, skipping")
                continue

            print (f"Creating Rephoto for {x}")
            print (f"copying {srcpath} -> {destpath}")
            if not options['dry_run']:
                os.makedirs(os.path.split(destpath)[0], exist_ok=True)
                copyfile(srcpath, destpath)

            r = Rephotograph()
            oid = x['original']
            print ("oid", oid)
            r.item = items_by_original_id[x['original']]
            r.file = os.path.relpath(destpath, settings.MEDIA_ROOT)
            r.seq = x['seq']
            # load the image to set the size
            im = cv2.imread(srcpath)
            r.width = im.shape[1]
            r.height = im.shape[0]
            print ("seq", r.seq)
            print ("item", r.item)
            print ("file", r.file)
            print ("width", r.width)
            print ("height", r.height)
            print ()
            if not options['dry_run']:
                r.save()


        # for path in options['image']:
        #     _, basename = os.path.split(path)
        #     basename, ext = os.path.splitext(basename)
        #     rpath = os.path.join("photos", basename + ".o" + ext)
        #     fpath = os.path.join(settings.MEDIA_ROOT, rpath)
        #     exists = os.path.exists(fpath)
        #     seq = self.parse_seq(basename)

        #     # pp = parse_filename(path, split2=False)
        #     # if pp != None:
        #     #     seq, inst, code = pp
        #     # try:
        #     #     rp = Rephotograph.objects.get(file=rpath)
        #     #     print ("EXISTS {0} {1}".format(rp.seq, rpath))
        #     # except Rephotograph.DoesNotExist:
        #     #     print ("CREATE {0} {1}".format(seq, rpath))

        #     rephoto, created = Rephotograph.objects.get_or_create(file=rpath, defaults={'seq':seq})
        #     # print ("{0} {1} {2}".format(path, fpath, exists, pp), file=self.stdout)
        #     if created:
        #         copyfile(path, fpath)
        #         # print ("created rephoto {0} {1}".format(rephoto.seq, rephoto.file.path), file=self.stdout)
        #         print ("CREATE {0} {1}".format(seq, rpath))

        #         im = cv2.imread(path)
        #         rephoto.width = im.shape[1]
        #         rephoto.height = im.shape[0]
        #         rephoto.save()

        #     else:
        #         # print ("{0} {1} already present".fo, file=self.stderr)
        #         print ("EXISTS {0} {1}".format(rephoto.seq, rpath))
