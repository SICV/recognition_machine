from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os
from rm.detection import init_face_detector, init_emotion_classifier


class Command(BaseCommand):
    help = 'Initialize face detector + emotion classifiers'

    # def add_arguments(self, parser):
    #     parser.add_argument('institution')

    def handle(self, *args, **options):
        print ("Initializing face detector")
        model1 = init_face_detector()
        print (f"Done, {model1}")

        print ("Initializing emotion classifier")
        model2 = init_emotion_classifier()
        print (f"Done, {model2}")
        