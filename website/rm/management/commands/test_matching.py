from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from rm.models import *
from rm.detection import init_rm_table, init_fer_table, init_emotion_classifier, classify_emotions


def test_equals (x, y):
    assert(x == y)
    if (x == y):
        print (f"T {x} == {y}: PASS")
    else:
        print (f"T {x} == {y}: FAIL!")

def test_rm_table ():
    rm_emotions, rm_labels = init_rm_table()
    print (f"rm_emotions: {rm_emotions.shape}")
    print (f"rm_labels: {rm_labels.shape}")
    
    test_equals(rm_emotions.shape[1], 7)
    test_equals(rm_labels.shape[1], 2)

    # The better way to get all Rephotograph faces...
    # from django.contrib.contenttypes.models import ContentType
    # rephotograph_type = ContentType.objects.get(app_label='rm', model='rephotograph')
    # Face.objects.filter(content_type=rephotograph_type)

    print ("Counting faces linked to rephotographs")
    faces = 0
    for r in Rephotograph.objects.all():
        for f in r.faces.all():
            faces += 1
    print (f"{faces} faces")
    test_equals(rm_emotions.shape[0], faces)
    test_equals(rm_labels.shape[0], faces)

def test_fer_table():
    fer_emotions, fer_labels = init_fer_table()
    print (f"fer_emotions: {fer_emotions.shape}")
    print (f"fer_labels: {fer_labels.shape}")
    test_equals(fer_emotions.shape[1], 7)
    test_equals(len(fer_labels.shape), 1)
    fer_count = fer_emotions.shape[0]
    test_equals(fer_labels.shape[0], fer_count)

from rm.detection import *
from rm.image_utils import *
from scipy.spatial.distance import cdist
from pprint import pprint


def test_match_post (post):
    print (f"Testing post {post.id}")
    face = post.faces.first()
    print ("Face.get_emotions_dict()")
    pprint (face.get_emotions_dict())
    print ()
    npemotions = face.get_emotions_np()
    print ("Face.get_emotions_np()")
    print (npemotions)

    fer_emotions, fer_labels = init_fer_table()
    rm_emotions, rm_labels = init_rm_table()

    # test based on stored emotions
    cd = cdist(rm_emotions, face.get_emotions_np(), metric="cityblock")
    rm_match_index = np.argmin(cd, axis=0)[0]
    face_id, photo_id = rm_labels[rm_match_index]
    print (f"From face {face.id} stored values:\nF{face_id}/R{photo_id}")
    diff = face.emotion_diff(Face.objects.get(pk=face_id))
    print (f"diff: {diff}")
    print ()

    # compare with Face.match_with_rephotograph
    print ("Calling Face.match_with_rephotograph")
    matching_face = face.match_with_rephotograph()
    print (f"F{matching_face.id}/R{matching_face.image.id}")
    diff = face.emotion_diff(matching_face)
    print (f"diff: {diff}")
    print ()

    if post.matching_face:
        print (f"Current match is face:\nF{post.matching_face.id}/R{post.rephoto.id}")
        diff = face.emotion_diff(post.matching_face)
        print (f"diff: {diff}")
    # post.matching_face = Face.objects.get(pk=face_id)
    # post.rephoto = Rephotograph.objects.get(pk=photo_id)
    # post.save()

def test_match_post_live (post):
    # Compare with live emotion classifier
    face = post.faces.first()

    rm_emotions, rm_labels = init_rm_table()
    print ("Loading live classifier")
    emotion_classifier = init_emotion_classifier()

    faces = [face.to_dict()]
    emoarray = classify_emotions(emotion_classifier, post.file.path, faces)
    print ("Live classifier...")
    print (emoarray)

    cd = cdist(rm_emotions, emoarray, metric="cityblock")
    rm_match_index = np.argmin(cd, axis=0)[0]
    face_id, photo_id = rm_labels[rm_match_index]
    print (f"From face {face.id} using live classifer:\nF{face_id}/R{photo_id}")
    # diff = face.emotion_diff(Face.objects.get(pk=face_id))
    # print (f"diff: {diff}")
    print ()

def test_match_fer (post):
    print (f"Testing post {post.id}")
    face = post.faces.first()
    fer_emotions, fer_labels = init_fer_table()

    # test based on stored emotions
    cd = cdist(fer_emotions, face.get_emotions_np(), metric="cityblock")
    fer_match_index = np.argmin(cd, axis=0)[0]
    fer_label = fer_labels[fer_match_index]
    fer_emotion = Face.EMOTION_CHOICES[fer_label][0]
    print (f"FER {fer_match_index}, label: {fer_label} {fer_emotion}")
    diff = face.emotion_diff(Face.objects.get(pk=face_id))
    print (f"diff: {diff}")
    print ()

class Command(BaseCommand):
    help = """

    Last version: 2020-09-12 (turin)
    """

    # def add_arguments(self, parser):
        # parser.add_argument('--institution', default="MC")
        # parser.add_argument('rephotos')
        # parser.add_argument('originals')
        # parser.add_argument('--dry-run', action="store_true", default=False)

    def handle(self, *args, **options):
        # test_rm_table()
        # test_fer_table()

        lastpost = Post.objects.order_by("-time")[0]
        # test_match_post(lastpost)
        # test_match_post_live(lastpost)
        test_match_fer(lastpost)

        # fer_emotions, fer_labels = init_fer_table()
        # for x in fer_labels:
        #     print (x)
