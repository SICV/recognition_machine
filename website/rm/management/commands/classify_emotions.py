from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, Face, FaceMatch
from django.conf import settings
from sicv.models import FER2013Face

import os
import cv2
import numpy as np
from scipy.spatial.distance import cdist


"""
Add comparison / match step with fer2013
Ensure item created for matching SICV/Fer2013 Face

"""

from rm.detection import init_face_detector, init_emotion_classifier, init_fer_table, classify_emotions



# def detect_emotions(emotion_classifier, gray, faces):
#     """
#     faces is a list of tuples (x1, y1, x2, y2)
#     """
#     emotion_target_size = emotion_classifier.input_shape[1:3]
#     emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

#     def preprocess_input(x, v2=True):
#         x = x.astype('float32')
#         x = x / 255.0
#         if v2:
#             x = x - 0.5
#             x = x * 2.0
#         return x

#     dd = []
#     emotionsarray = np.zeros((len(faces), 7), dtype="float32")
#     for i, face_coordinates in enumerate(faces):
#         x1, y1, x2, y2 = face_coordinates[0], face_coordinates[1], face_coordinates[0]+face_coordinates[2], face_coordinates[1]+face_coordinates[3] # apply_offsets(face_coordinates, emotion_offsets)
#         gray_face = gray[y1:y2, x1:x2]
#         try:
#             gray_face = cv2.resize(gray_face, (emotion_target_size))
#         except Exception as e:
#             print ("Exception resizing", e)
#             continue

#         gray_face = preprocess_input(gray_face, True)
#         gray_face = np.expand_dims(gray_face, 0)
#         gray_face = np.expand_dims(gray_face, -1)
#         emotion_prediction = emotion_classifier.predict(gray_face)
#         emotionsarray[i] = emotion_prediction
#         d = {}
#         for label, score in zip(emotion_labels, emotion_prediction[0]):
#             d[label] = float(score)
#         print ("got emotion_prediction", d)
#         dd.append(d)
#     return dd, emotionsarray

def max_key (d):
    maxv, maxk = None, None
    for key in d:
        ev = d[key]
        if maxv == None or ev > maxv:
            maxv = ev
            maxk = key
    return maxk

class Command(BaseCommand):
    help = """
 
    Last Modified Sep 2020 (Turin)
    """

    def add_arguments(self, parser):
        parser.add_argument('--start-id', type=int, default=None)
        parser.add_argument('--rmemotions', default="rm_emotions.npy", help="output path to rm recognition matrix of emotion predictions")
        parser.add_argument('--rmlabels', default="rm_labels.npy", help="output path to rm matrix of labels")

        # parser.add_argument('--genimages', action="store_true", default=False)
        # parser.add_argument('--force', action="store_true", default=False)
        parser.add_argument('--limit', type=int, default=None)
        # parser.add_argument('--feremotions', default="fer2013_emotions.npy", help="path to fer2013 matrix of emotion predictions")
        # parser.add_argument('--ferlabels', default="fer2013_labels.npy", help="path to fer2013 matrix of labels")

    def handle(self, *args, **options):
        query = Rephotograph.objects.all()
        if options['start_id']:
            query = query.filter(id__gte=options['start_id'])
        query_count = query.count()
        print ("Loading emotion classifier", file=self.stdout)
        emotion_classifier = init_emotion_classifier()
        feremotions, ferlabels = init_fer_table()
        if options['rmemotions']:
            rmemotions = np.zeros((query_count*5, 7), dtype="float32")
            rmlabels = np.zeros((query_count*5, 2), dtype="int32")
        else:
            rmemotions = None
            rmlabels = None
        print ("Loaded", file=self.stdout)

        count = 0
        faceindex = -1

        for photo in query:
            print (f"{photo.id} {photo}")
            # img, gray, faces = detect_faces(face_cascade, photo.file.path)
            faces = photo.to_leaflet_dict()['faces']
            print (f"  faces: {faces}")
            emotionsarray = classify_emotions(emotion_classifier, photo.file.path, faces)

            # print ("emotions {0}".format(emotions))

            photo.matches.all().delete()
            # MATCH with fer2013
            if feremotions is not None:
                cd = cdist(feremotions, emotionsarray, metric="cityblock")
                fer2013_minindexes = np.argmin(cd, axis=0)
                print ("FER2013 MATCHES", fer2013_minindexes)

            for face, d in zip(photo.faces.all(), faces):
                for val, _ in Face.EMOTION_CHOICES:
                    setattr(face, "emotion_"+val, d['emotion_prediction_by_label'].get(val))
                face.emotion = d['emotion_prediction_label']
                face.save()

                if rmemotions is not None:
                    # RECORD THIS FACE + LABELS IN THE RMMATRIX
                    faceindex += 1
                    rmemotions[faceindex] = emotionsarray[face.index]
                    rmlabels[faceindex] = [face.id, photo.id]
                    # photo.save()

                if feremotions is not None:
                    # ENSURE FER2013 ITEM
                    # consult corresponding label
                    fer2013_index = fer2013_minindexes[face.index]
                    fer2013_label = ferlabels[fer2013_index]
                    rpath = "datasets/fer2013/Train/{0}.jpg".format(fer2013_index+1)
                    ferface, created = FER2013Face.objects.get_or_create(path=rpath, emotion=fer2013_label)
                    # make a face object
                    ferface.faces.all().delete()
                    ff = Face(image=ferface, file=rpath, index=0, x=0, y=0, width=48, height=48)
                    # transfer emotion values to ff
                    femotions = feremotions[fer2013_index]
                    for i, (val, _) in enumerate(Face.EMOTION_CHOICES):
                        setattr(ff, "emotion_"+val, femotions[i])
                    ff.save()
                    # CREATE A MATCH ITEM linking faces...
                    match = FaceMatch()
                    match.rephoto = photo
                    match.rephoto_face = face
                    match.other_face = ff
                    match.save()
                    # photo.fer2013_match = ff

            faces = photo.to_leaflet_dict()['faces']
            print (f"  faces: {faces}")

            count += 1
            if options['limit'] and count>= options['limit']:
                break

        # reshape arrays to actual data size
        # rmemotions.reshape((faceindex+1, 7))
        # rmlabels.reshape((faceindex+1, 2))
        print("saving rm matrices")
        print ("rmemotions shape:", rmemotions.shape, "faceindex:", faceindex)
        assert (faceindex+1 <= rmemotions.shape[0])
        rmemotions = rmemotions[:faceindex+1]
        rmlabels = rmlabels[:faceindex+1]
        print ("rmemotions shape:", rmemotions.shape, "faceindex:", faceindex)
        print ("Saving {0}".format(options['rmemotions']))
        np.save(options['rmemotions'], rmemotions)
        np.save(options['rmlabels'], rmlabels)

    # ctx['faces'] = [{'x': int(x[0]), 'y': int(x[1]), 'width': int(x[2]), 'height': int(x[3])} for x in faces]

