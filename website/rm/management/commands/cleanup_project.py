from django.core.management.base import BaseCommand, CommandError
from rm.models import Rephotograph
from sicv.models import FER2013Face, IMDBFace
from django.conf import settings
import os


class Command(BaseCommand):
    help = 'delete unused stuff'

    # def add_arguments(self, parser):
    #     parser.add_argument('institution')

    def handle(self, *args, **options):
        # inst = options['institution']
        #inst = Institution.objects.get(slug=inst)
        # delete all posts
        # Post.objects.all().delete()
        # FER2013Face.objects.all().delete()
        # IMDBFace.objects.all().delete()

        # Remove Rephoto objects with no file
        delme = []
        for p in Rephotograph.objects.all():
            if not os.path.exists(p.file.path):
                print ("missing file", p.file.url)
                delme.append(p)
        for p in delme:
            p.delete()