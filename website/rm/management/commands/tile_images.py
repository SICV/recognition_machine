from django.core.management.base import BaseCommand, CommandError
from rm.models import Rephotograph
from django.conf import settings
import os, re, sys
from PIL import Image

from .tileimages import tile_image, expand_template


class Command(BaseCommand):
    help = """
    Tile the rephoto images for use with leaflet
    NB: sets width,height if not already
    Last modified 2020 09 13 (turin)
    """

    def add_arguments(self, ap):
        # ap.add_argument("--background", default=None, help="Color to use as background, in red,green,blue e.g., default None (black for jpg output, transparent for png)")
        ap.add_argument("--tilespath", default="tiles", help="name of path to create in the same folder as the original")
        ap.add_argument("--tilewidth", type=int, default=256)
        ap.add_argument("--tileheight", type=int, default=256)
        ap.add_argument("--zoom", type=int, default=3)
        ap.add_argument("--tilename", default="z{z}y{y}x{x}")
        ap.add_argument("--extension", default=".jpg")
        ap.add_argument("--force", default=False, action="store_true")

    def handle(self, *args, **options):
        tilenamex = expand_template(options['tilename']+options['extension'])
        bgcolor = (0,0,0) # (199, 199, 199)

        for r in Rephotograph.objects.all():
            imgpath = os.path.join(settings.MEDIA_ROOT, r.file.name)
            parent = os.path.split(imgpath)[0]
            basename = os.path.basename(imgpath)
            path = os.path.join(parent, options['tilespath'], basename)
            tile0 = os.path.join(path, tilenamex.format({'x': 0, 'y': 0, 'z': 0}))
            # items.append(item)
            if not os.path.exists(tile0) or options['force']:
                print (f"Tiling {r}", file=sys.stderr)
                try:
                    im = Image.open(imgpath)
                    os.makedirs(path, exist_ok=True)
                    imageWidth, imageHeight, scaledWidth, scaledHeight = tile_image(
                            im,
                            options['zoom'],
                            options['tilewidth'],
                            options['tileheight'],
                            path+"/",
                            tilenamex,
                            bgcolor)
                    if not r.width:
                        print (f"{r}: Setting width, height")
                        r.width = imageWidth
                        r.height = imageHeight
                        r.save()

                except IOError as e:
                    print (r"{r}: IOError: {e}", file=sys.stderr)
