from django.core.management.base import BaseCommand, CommandError
from rm.models import Post, Face


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    # def add_arguments(self, parser):
    #     parser.add_argument('--rephotographs', action="store_true", default=False)
    #     parser.add_argument('--institution', default=None)
    #     parser.add_argument('image', nargs="+")

    def handle(self, *args, **options):
        last_post = Post.objects.order_by("-time")[0]
        face = last_post.faces.all()[0]
        print ("MATCH", face.match_with_rephotograph())
