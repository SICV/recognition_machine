from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, parse_filename
from django.conf import settings
from shutil import copyfile
import os, re, cv2


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('--rephotographs', action="store_true", default=False)
        parser.add_argument('--institution', default=None)
        parser.add_argument('image', nargs="+")

    def parse_seq (self, path):
        return int(re.search("\d+", path).group(0))

    def handle(self, *args, **options):
        for path in options['image']:
            _, basename = os.path.split(path)
            basename, ext = os.path.splitext(basename)
            rpath = os.path.join("photos", basename + ".o" + ext)
            fpath = os.path.join(settings.MEDIA_ROOT, rpath)
            exists = os.path.exists(fpath)
            seq = self.parse_seq(basename)

            # pp = parse_filename(path, split2=False)
            # if pp != None:
            #     seq, inst, code = pp
            # try:
            #     rp = Rephotograph.objects.get(file=rpath)
            #     print ("EXISTS {0} {1}".format(rp.seq, rpath))
            # except Rephotograph.DoesNotExist:
            #     print ("CREATE {0} {1}".format(seq, rpath))

            rephoto, created = Rephotograph.objects.get_or_create(file=rpath, defaults={'seq':seq})
            # print ("{0} {1} {2}".format(path, fpath, exists, pp), file=self.stdout)
            if created:
                copyfile(path, fpath)
                # print ("created rephoto {0} {1}".format(rephoto.seq, rephoto.file.path), file=self.stdout)
                print ("CREATE {0} {1}".format(seq, rpath))

                im = cv2.imread(path)
                rephoto.width = im.shape[1]
                rephoto.height = im.shape[0]
                rephoto.save()

            else:
                # print ("{0} {1} already present".fo, file=self.stderr)
                print ("EXISTS {0} {1}".format(rephoto.seq, rpath))
            # Rephotograph.objects.get(file=rpath)
            # else:
            #     print ("path not parsed", path, file=self.stderr)
        # inst = options['institution']
        # if inst:
        #     inst, _ = Institution.objects.get_or_create(slug=inst)
        #     for path in options['image']:
        #         path = os.path.abspath(path)
        #         _, basename = os.path.split(path)
        #         code, ext = os.path.splitext(basename)
        #         item, _ = Item.objects.get_or_create(institution=inst, code=code)
        #         # item.save()
        #         relpath = os.path.join("images", basename)
        #         newpath = os.path.join(settings.MEDIA_ROOT, relpath)
        #         copyfile(path, newpath)
        #         image, created = OriginalImage.objects.get_or_create(file=relpath, defaults=(('item', item),))
        #         self.stdout.write(self.style.SUCCESS('Created image {0}'.format(item.code)))
        # else:
        #     for path in options['image']:
        #         path = os.path.abspath(path)
        #         _, basename = os.path.split(path)
        #         basename, ext = os.path.splitext(basename)
        #         parts = basename.split("_")
        #         if len(parts) > 3:
        #             parts = parts[0], parts[1], "_".join(parts[2:])
        #         try:
        #             seq, inst, code = parts
        #             seq = int(seq)
        #             try:
        #                 item = Item.objects.get(code=code)
        #             except Item.DoesNotExist:
        #                 item=None
        #             print (seq, inst, code, item)
        #         except ValueError:
        #             print ("Exceptional name {0}".format(basename))
        #             item=None
        #             seq=None
        #         # item, _ = Item.objects.get_or_create(institution=inst, code=code)
        #         relpath = os.path.join("photos", basename+".o.jpg")
        #         newpath = os.path.join(settings.MEDIA_ROOT, relpath)
        #         copyfile(path, newpath)
        #         rephoto, _ = Rephotograph.objects.get_or_create(file=relpath)
        #         rephoto.seq = seq
        #         rephoto.item = item
        #         rephoto.save()
        #         # # item.save()
        #         # image, created = OriginalImage.objects.get_or_create(file=relpath, defaults=(('item', item),))
        #         # self.stdout.write(self.style.SUCCESS('Created image {0}'.format(item.code)))
