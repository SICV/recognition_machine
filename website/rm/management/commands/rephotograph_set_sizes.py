from django.core.management.base import BaseCommand, CommandError
from rm.models import Rephotograph
from django.conf import settings
import cv2


class Command(BaseCommand):
    help = 'delete unused stuff'

    # def add_arguments(self, parser):
    #     parser.add_argument('institution')

    def handle(self, *args, **options):
        for p in Rephotograph.objects.all():
            im = cv2.imread(p.file.path)
            p.width = im.shape[1]
            p.height = im.shape[0]
            p.save()
