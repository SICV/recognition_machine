from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, Face, FaceMatch
from django.conf import settings
from sicv.models import FER2013Face

import os
import cv2
import numpy as np
from scipy.spatial.distance import cdist


"""
Add comparison / match step with fer2013
Ensure item created for matching SICV/Fer2013 Face

"""

from rm.detection import init_face_detector, detect_faces

MIN_FACE_SIZE = 100


# def detect_faces (face_cascade, imgpath):
#     frame = cv2.imread(imgpath)
#     gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     faces = face_cascade.detectMultiScale(gray)
#     faces = [x for x in faces if x[2] > MIN_FACE_SIZE]
#     # ret=[]
#     # for (x,y,w,h) in faces:
#     #     #print "face", (x, y, w, h)
#     #     ret.append({'x': int(x), 'y': int(y), 'w': int(w), 'h': int(h)})
#     return frame, gray, faces


# def detect_emotions(emotion_classifier, gray, faces):
#     """
#     faces is a list of tuples (x1, y1, x2, y2)
#     """
#     emotion_target_size = emotion_classifier.input_shape[1:3]
#     emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

#     def preprocess_input(x, v2=True):
#         x = x.astype('float32')
#         x = x / 255.0
#         if v2:
#             x = x - 0.5
#             x = x * 2.0
#         return x

#     dd = []
#     emotionsarray = np.zeros((len(faces), 7), dtype="float32")
#     for i, face_coordinates in enumerate(faces):
#         x1, y1, x2, y2 = face_coordinates[0], face_coordinates[1], face_coordinates[0]+face_coordinates[2], face_coordinates[1]+face_coordinates[3] # apply_offsets(face_coordinates, emotion_offsets)
#         gray_face = gray[y1:y2, x1:x2]
#         try:
#             gray_face = cv2.resize(gray_face, (emotion_target_size))
#         except Exception as e:
#             print ("Exception resizing", e)
#             continue

#         gray_face = preprocess_input(gray_face, True)
#         gray_face = np.expand_dims(gray_face, 0)
#         gray_face = np.expand_dims(gray_face, -1)
#         emotion_prediction = emotion_classifier.predict(gray_face)
#         emotionsarray[i] = emotion_prediction
#         d = {}
#         for label, score in zip(emotion_labels, emotion_prediction[0]):
#             d[label] = float(score)
#         print ("got emotion_prediction", d)
#         dd.append(d)
#     return dd, emotionsarray


class Command(BaseCommand):
    help = """
    Run Face detection and create Face objects linked to Rephotographs

    Add --drop-existing-faces to drops any existing/linked Face objects
    NB: This will (partially) orphan objects like linked Posts

    start_id: starting Rephoto id -- only process id's >= this value

    Last Modified Sep 2020 (Turin)
    """

    def add_arguments(self, parser):
        parser.add_argument('--nogen', action="store_true", default=False, help="skip generating the face image")
        parser.add_argument('--drop-existing', action="store_true", default=False, help="use this option to skip photos that already have detected faces, the default is to drop/reset them")
        parser.add_argument('--start-id', type=int, default=None)
        parser.add_argument('--limit', type=int, default=None)

    def handle(self, *args, **options):
        query = Rephotograph.objects.all()
        if options['start_id']:
            query = query.filter(id__gte=options['start_id'])
        query_count = query.count()

        print ("Loading face detector", file=self.stdout)
        # face_cascade = cv2.CascadeClassifier(os.path.join(settings.MODELS, 'haarcascade_frontalface_default.xml'))
        face_detector = init_face_detector()
        count = 0

        for photo in query:
            print (f"{photo.id} {photo}")
            if photo.faces.count() > 0:
                if options['drop_existing']:
                    print (f"Dropping existing {photo.faces.count()} faces")
                    photo.faces.all().delete()
                else:
                    print (f"    skipping: already has {photo.faces.count()} faces, use --drop-existing to reset them")
                    continue

            d = detect_faces(face_detector, photo.file.path)
            faces = d['faces']
            print (f"    {len(faces)} faces detected")

            for i, face in enumerate(faces):
                f = Face(image=photo, index=i, x=face['x'], y=face['y'], width=face['width'], height=face['height'])
                f.save()
                print ("    created:",f)
                if not options['nogen']:
                    f.save_to_file(force=True)

            count += 1
            if options['limit'] and count>= options['limit']:
                break
