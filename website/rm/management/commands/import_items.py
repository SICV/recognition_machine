from django.core.management.base import BaseCommand, CommandError
from rm.models import Item, Institution, OriginalImage, Rephotograph, parse_filename
from django.conf import settings
from shutil import copyfile
import os, re, cv2

from .patterns import patterns


class Command(BaseCommand):
    help = """
    Import Items/Original images

    Last version: 2020-09-12 (turin)
    """

    def add_arguments(self, parser):
        parser.add_argument('--institution', default="MC")
        parser.add_argument('image', nargs="+")
        parser.add_argument('--dry-run', action="store_true", default=False)

    def handle(self, *args, **options):
        inst_code = options['institution']
        inst = Institution.objects.get(slug=inst_code)

        originals = []
        originals_by_id = {}

        original_pat = re.compile(patterns[inst_code]['original'])
        for filename in options['image']:
            m = original_pat.search(filename)
            if m:
                d = m.groupdict()
                d['filename'] = os.path.split(filename)[1]
                d['code'] = d['code'].strip("-_")
                d['original'] = int(d['original'])
                originals.append(d)
                if d['original'] in originals_by_id:
                    print (f"WARNING: DUPLICATE ORIGINAL {d['original']}, {filename}")
                originals_by_id[d['original']] = d

                x = d
                srcpath = filename
                destpath = os.path.join(settings.MEDIA_ROOT, "images", inst_code, x['filename'])

                if os.path.exists(destpath):
                    print (f"Item for {x['filename']} already exists, skipping")
                    continue
                print (f"Creating Item for {x}")
                i = Item()
                i.institution = inst
                i.code = x['original']
                print (f"copying {srcpath} -> {destpath}")
                if not options['dry_run']:
                    os.makedirs(os.path.split(destpath)[0], exist_ok=True)
                    copyfile(srcpath, destpath)

                i.file = os.path.relpath(destpath, settings.MEDIA_ROOT)
                print ("code", i.code)
                print ("institution", i.institution)
                print ("file", i.file)
                print ()
                if not options['dry_run']:
                    i.save()


            else:
                print (f"WARNING {filename} doesn't match original pattern, ignoring")

