import functools

from PIL import Image
import numpy as np


def ensure_in_frame(r, frame_size):
    if r['x'] < 0:
        r['x'] = 0
    if r['y'] < 0:
        r['y'] = 0
    frame_width, frame_height = frame_size
    if r['x'] + r['width'] > frame_width:
        r['x'] = frame_width - r['width']
    if r['y'] + r['height'] > frame_height:
        r['y'] = frame_height - r['height']

def square (r):
    sw = max(r['width'],r['height'])
    if r['width'] != sw: # expand width
        r['x'] -= ((sw-r['width'])//2)
        r['width'] = sw
    else: # expand height
        r['y'] -= ((sw-r['height'])//2)
        r['height'] = sw

def pil2cv (im_pil):
    im = np.array(im_pil)
    im = im[:, :, ::-1].copy() 
    return im

def image_transpose_exif(im):
    """
    Apply Image.transpose to ensure 0th row of pixels is at the visual
    top of the image, and 0th column is the visual left-hand side.
    Return the original image if unable to determine the orientation.

    As per CIPA DC-008-2012, the orientation field contains an integer,
    1 through 8. Other values are reserved.

    Parameters
    ----------
    im: PIL.Image
       The image to be rotated.
    """

    exif_orientation_tag = 0x0112
    exif_transpose_sequences = [                   # Val  0th row  0th col
        [],                                        #  0    (reserved)
        [],                                        #  1   top      left
        [Image.FLIP_LEFT_RIGHT],                   #  2   top      right
        [Image.ROTATE_180],                        #  3   bottom   right
        [Image.FLIP_TOP_BOTTOM],                   #  4   bottom   left
        [Image.FLIP_LEFT_RIGHT, Image.ROTATE_90],  #  5   left     top
        [Image.ROTATE_270],                        #  6   right    top
        [Image.FLIP_TOP_BOTTOM, Image.ROTATE_90],  #  7   right    bottom
        [Image.ROTATE_90],                         #  8   left     bottom
    ]

    try:
        seq = exif_transpose_sequences[im._getexif()[exif_orientation_tag]]
    except Exception as e:
        # print ("image_transpose_exif: EXCEPTION", e)
        return im
    else:
        return functools.reduce(type(im).transpose, seq, im)