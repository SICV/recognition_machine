from printing import print_photostrip, print_image
import datetime
import argparse
from urllib.request import urlopen, Request
from urllib.parse import urlencode
import json
from time import sleep

# from urllib import request, parse
# data = parse.urlencode(<your data dict>).encode()
# req =  request.Request(<your url>, data=data) # this will make the method "POST"
# resp = request.urlopen(req)


def log (msg):
    now = datetime.datetime.now()
    print (f'{now.strftime("%Y-%m-%d %H:%M:%S")}: {msg}')

def init_ps ():
    ps = {}
    ps['items'] = items = []
    items.append({
        'imagef': "printer/init2.png"
        })
    items.append({'text': "The Recognition Machine"})
    items.append({'text': "READY"})
    return ps

def poll_and_print (url, time, verbose=False, copies=1):
    # print_image("printer/init2.png", cut=False)
    log("poll and print... starting up")

    while True:
        try:
            print_photostrip(init_ps())
            break
        except Exception as e:
            log(f"Exception: {e}")
            sleep(time)

    # f = urlopen(url)
    # d = json.load(f)
    # args = {}
    # args['since'] = d['posts'][0]['time']
    # print ("last_post_time", args['since'])
    args = {}
    args['mark'] = '1'
    post_data = urlencode(args).encode()

    sleep(time)

    while True:
        # purl = url + "?" + urlencode(args)
        try:
            f = urlopen(url)
            d = json.load(f)
            if 'posts' in d and len(d['posts']) > 0:
                for p in d['posts']:
                    log (f"PRINTING {p['photostrip_url']}")
                    f = urlopen(p['photostrip_url'])
                    ps = json.load(f)
                    print_photostrip(ps, copies)
                    # resp = urlopen(Request(p['post_print_url'], data=post_data))
                    resp = urlopen(p['post_print_url'])
                    log (f"got response {resp} ({resp.code})")
            else:
                if verbose:
                    log("no new posts")
        except Exception as e:
            log("Exception {}...".format(e))
        sleep(time)

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("--url", default="https://recognitionmachine.vandal.ist/o/feed/")
    ap.add_argument("--verbose", action="store_true", default=False)
    # ap.add_argument("--url", default="https://recognitionmachine.vandal.ist/o/feed/")
    ap.add_argument("--polltime", type=float, default=5.0)
    ap.add_argument("--copies", type=int, default=1)
    args = ap.parse_args()

    poll_and_print(args.url, args.polltime, verbose=args.verbose, copies=args.copies)


