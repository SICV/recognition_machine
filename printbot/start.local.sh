#!/bin/bash
# changed copies to 1 seeing duplicates left over and fact that people are taking many multiples
# single copy makes the choice of leaving or taking more interesting / whole process less wasteful by default
# (person can choose to make two of similar emotion, or try ;)
python3 -u poll_and_print.py --url http://localhost/o/feed/ --copies 1 --verbose > /var/www/vhosts/recognitionmachine.vandal.ist/wsgi/recognition_machine/printbot/poll_and_print.log
