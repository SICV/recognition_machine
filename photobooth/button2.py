import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)#Button
GPIO.setup(19, GPIO.OUT)  #LED
led = False
try:
    while True:
         button_state = GPIO.input(26)
         if button_state == False:
             # GPIO.output(24, True)
             print('Button Pressed...')

             while True:
                 button_state = GPIO.input(26)
                 if button_state == True:
                     break
             led = not led
             print ("led", led)
             GPIO.output(19, led)
             time.sleep(0.2)
         # else:
             # GPIO.output(24, False)


except:
    GPIO.cleanup()
