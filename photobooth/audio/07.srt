1
00:00:00,000 --> 00:00:02,789
Nun zu Ihren Hausaufgaben:

2
00:00:02,789 --> 00:00:05,580
Der QR-code und der Link unter Ihrem Bild

3
00:00:05,580 --> 00:00:07,826
bringt Sie zur Originalfotografie

4
00:00:07,826 --> 00:00:09,999
und zu dem Archiv, dem sie entnommen ist.

5
00:00:09,999 --> 00:00:11,660
Ich möchte, dass Sie dazu recherchieren,

6
00:00:11,660 --> 00:00:14,502
ob wissenschaftlich, ob von Ihren Gefühlen
geleitet oder beides,

7
00:00:14,502 --> 00:00:17,970
und das dokumentieren, indem Sie
einen Kommentar hinterlassen.

8
00:00:17,970 --> 00:00:19,760
Die Form ist Ihnen freigestellt,

9
00:00:19,760 --> 00:00:29,760
ob schriftlich, als Foto
oder als Zeichnung ...

