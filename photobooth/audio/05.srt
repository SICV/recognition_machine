1
00:00:00,000 --> 00:00:03,634
Wir suchen nun mithilfe von Modell FER2013

2
00:00:03,634 --> 00:00:08,451
nach einem Treffer in der Datenbank
für Fotografien aus der Kolonialzeit.

3
00:00:08,451 --> 00:00:13,556
Die Künstlerin hat von jedem dieser Bilder
einen Ausschnitt gewählt, es abfotografiert
und von Hand entwickelt und bearbeitet.

4
00:00:13,556 --> 00:00:18,950
Die Originalfotografien wurden unter
ungleichen Machtverhältnissen aufgenommen,

5
00:00:18,950 --> 00:00:20,936
in denen das Modell oft zu
etwas gezwungen wurde,

6
00:00:20,936 --> 00:00:24,584
während der Fotograf/die Fotografin
– Teil der herrschenden Kultur –

7
00:00:24,584 --> 00:00:29,281
darüber entschied, wie Menschen
dargestellt werden sollen.

8
00:00:29,281 --> 00:00:31,794
Dieser fotografische Stil schlägt
sich noch heute in der Art und Weise nieder,

9
00:00:31,794 --> 00:00:34,829
wie Menschen, die von anderen
Kontinenten kommen, dargestellt werden,

10
00:00:34,829 --> 00:00:40,956
in anthropologischer Fotografie,
aber auch auf Touristen- und Werbefotos.

11
00:00:40,956 --> 00:00:50,956
Diese Wiederholung wiederholt die
Gewaltsamkeit der ursprünglichen Aufnahmen.

