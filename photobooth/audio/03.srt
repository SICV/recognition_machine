1
00:00:00,000 --> 00:00:01,934
Ihr Bild ist nun aufgenommen worden.

2
00:00:01,934 --> 00:00:05,894
Wir kategorisieren Sie nun nach Ihrer Emotion.

3
00:00:05,894 --> 00:00:09,479
Wir verwenden das Modell FER2013.

4
00:00:09,479 --> 00:00:13,551
Die 28.709 Gesichter, mit denen dieses
Modell gespeist wurde,

5
00:00:13,551 --> 00:00:16,959
sind mithilfe der Google-Bildersuche
zusammengetragen worden.

6
00:00:16,959 --> 00:00:20,919
Jedes Bild wurde mit einem
der folgenden Schlagwörter versehen:

7
00:00:20,919 --> 00:00:25,731
Wut, Ekel, Angst, Freude,
Trauer, Überraschung, neutral.

8
00:00:25,731 --> 00:00:35,731
Die Verschlagwortung wurde von einem
nicht weiter spezifizierten Mitarbeiter vorgenommen.

