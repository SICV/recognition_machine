from __future__ import print_function
from urllib2 import urlopen
import requests, html5lib, sys, json
from common import face_to_dict
import datetime

def post (image, face, url="http://localhost:8000/post/"):
    if type(image) == str:
        # path
        files = {'file': open(image, 'rb')}
    else:
        # assume imagedata, e.g. _, image = cv2.imencode(".jpg", image)
        files = {'file': ('image.jpg', image, 'image/jpeg', {'Expires': '0'})}
    form = requests.get(url)
    data = {}
    # print (form.text, file=sys.stderr)
    t = html5lib.parseFragment(form.text, namespaceHTMLElements=False)
    for hinput in t.findall(".//input[@type='hidden']"):
        if hinput.attrib.get("name") == "csrfmiddlewaretoken":
            data['csrfmiddlewaretoken'] = hinput.attrib.get("value")
    # face=>data
    if face:
        data['classification_data'] = json.dumps(face_to_dict(face))
    else:
        data['classification_data'] = '{}'
    print ("pre-post check url", url)
    print ("pre-post check data", data)
    print ("pre-post check files", files)

    response = requests.post(url, \
        data=data, \
        cookies = form.cookies, \
        files=files)
    return response

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("--url", default="http://localhost:8000/post/")
    ap.add_argument("image")
    args = ap.parse_args()

    post(args.image, {}, args.url)
