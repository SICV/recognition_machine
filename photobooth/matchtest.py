from __future__ import print_function, division
from scipy.spatial.distance import cdist
from common import *
from printing import *
import cv2
import datetime
import json
from PIL import Image, ImageOps
import shutil

ap = argparse.ArgumentParser("")
ap.add_argument("image")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument('--feremotions', default="../website/fer2013_emotions.npy", help="path to fer2013 matrix of emotion predictions")
ap.add_argument('--ferlabels', default="../website/fer2013_labels.npy", help="path to fer2013 matrix of labels")
ap.add_argument('--rmemotions', default="../website/rm_emotions.npy", help="path to rm recognition matrix of emotion predictions")
ap.add_argument('--rmlabels', default="../website/rm_labels.npy", help="path to rm matrix of labels")
# ap.add_argument('--data', default="../website/media/data", help="path to db dumps")
ap.add_argument('--media', default="../website/media", help="path to website media")
args = ap.parse_args()

MODELS = os.path.expanduser(args.models)

face_cascade = cv2.CascadeClassifier(os.path.join(MODELS, 'haarcascade_frontalface_default.xml'))
# print ("Loading shape predictor")
# shape_predictor = dlib.shape_predictor(os.path.join(MODELS, "shape_predictor_68_face_landmarks.dat"))

if args.emotion:
    print("loading emotion classifier")
    # print ("Loading emotion classifier", file=sys.stderr)
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(MODELS, "emotion_model.hdf5"))

# Load the comparison matrices
feremotions = np.load(args.feremotions)
ferlabels = np.load(args.ferlabels)
rmemotions = np.load(args.rmemotions)
rmlabels = np.load(args.rmlabels)

EMOTION_CHOICES = (
    ('angry', 'angry'),
    ('disgust', 'disgust'),
    ('fear', 'fear'),
    ('happy', 'happy'),
    ('sad', 'sad'),
    ('surprise', 'surprise'),
    ('neutral', 'neutral')
)
def unparse_emotion_vector (e):
    for i in range(7):
        print ("{1:0.01f}% {0}".format(EMOTION_CHOICES[i][0], float(e[i])*100))

def unparse_emotion_summary (v1, v2, v3):
    """
    12345678901234567890123456789012
               YOU    FER     RM
    angry    53.7%  53.5%  46.9%
    disgust   1.0%   0.0%
    fear      5.6%
    happy     0.1%
    sad       8.7% 
    surprise  2.7% 
    neutral  28.1% 
    """
    ret=[]
    ret.append("*            YOU    FER     RM")
    for i in range(7):
        label = EMOTION_CHOICES[i][0]
        line = " {0: <8}  {1: >04.01f}%  {2: >04.01f}%  {3: >04.01f}%".format(label, v1[i]*100, v2[i]*100, v3[i]*100)
        ret.append(line)
    return ret

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def make_print_version(im):
    im = im.convert("L")
    rw, rh = fit_size(im, (400, 10000))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.equalize(im)
    im = im.convert("1")
    return im

# current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
current_image_bgr = cv2.imread(args.image)
current_image = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2GRAY)
current_faces = get_faces(face_cascade, current_image_bgr)
n = datetime.datetime.now()
base = n.strftime("%Y%m%d_%H%M%S")
print (datetime.datetime.now())
if len(current_faces) > 0:
    if len(current_faces) > 1:
        s = 's'
    else:
        s = ''
    print ("{0} face{1}".format(len(current_faces), s))

    if args.emotion:
        get_emotions(emotion_classifier, current_image, current_faces)

    for i, face in enumerate(current_faces):
        # cv2.imwrite("image.jpg", current_image)
        fe = face['emotionarray']
        # print ("face.emotionarray", fe)
        fr = face['rect']
        # fr = current_faces[0]['rect']
        x1, y1, x2, y2 = fr.left(), fr.top(), fr.right(), fr.bottom()
        current_face = current_image[y1:y2, x1:x2]
        current_face_bgr = current_image_bgr[y1:y2, x1:x2]

        cd = cdist(feremotions, fe, metric="cityblock")
        fer_match_index = np.argmin(cd, axis=0)[0]

        cd = cdist(rmemotions, fe, metric="cityblock")
        rm_match_index = np.argmin(cd, axis=0)[0]
        # print ("Matches {0} fer, {1} rm".format(fer_match_index, rm_match_index))

        face_id, photo_id = rmlabels[rm_match_index]

        # DEBUG PRINTING
        # print ("ORIGINAL FACE")
        # unparse_emotion_vector(fe[0])
        # print ()
        # print ("FER ~/machinelearning/datasets/fer2013_public/Train/{0}.jpg".format(fer_match_index+1))
        # print ("label: {0}({1})".format(EMOTION_CHOICES[ferlabels[fer_match_index]][0], fer_match_index))
        # unparse_emotion_vector(feremotions[fer_match_index])
        # print ()
        # print ("RM http://localhost:8000/r/{0} http://localhost:8000/faces/{1}".format(photo_id, face_id))
        # unparse_emotion_vector(rmemotions[rm_match_index])
        # print ()

        # crop photo to match the portrait
        with open(os.path.join(args.media, "data/faces/{0:06d}.json".format(face_id))) as inf:
            face_data = json.load(inf)
        with open (os.path.join(args.media, "data/photos/{0:06d}.json".format(photo_id))) as inf:
            photo_data = json.load(inf)

        # photo_size = photo_data['imageWidth'], photo_data['imageHeight']
        # facepos = face_data['x'], face_data['y'], face_data['width'], face_data['height']
        # facepos = float(facepos[0]) / photo_size[0], \
        #     float(facepos[1]) / photo_size[1], \
        #     float(facepos[2]) / photo_size[0], \
        #     float(facepos[3]) / photo_size[1]
        # print ("Relative facepos face in photo", facepos)
        # rw, rh = facepos[2], facepos[3]

        # niw, nih = current_image.shape[1], current_image.shape[0]
        # print ("Original size", niw, nih)
        # fw, fh = fr.width(), fr.height()
        # print ("Face width, height", fw, fh)

        # additional_width = (1.0 - rw) * niw
        # additional_height = (1.0 - rh) * nih
        # new_width = additional_width + fr.width()
        # new_height = additional_height + fr.height() 
        # new_x = facepos[0] * new_width
        # new_y = facepos[1] * new_height

        # print ("NEW IMAGE RECT", new_x, new_y, new_width, new_height)


        rotate = photo_data['imageWidth']>photo_data['imageHeight']
        current_face_pil = Image.fromarray(cv2.cvtColor(current_face_bgr,cv2.COLOR_BGR2RGB))
        current_face_pil = make_print_version(current_face_pil)
        if rotate:
            current_face_pil = current_face_pil.rotate(90, expand=1)
        current_face_pil.save("print01.jpg")

        # copy / prepare the fer image
        ferpath = os.path.join(args.media, "datasets/fer2013/Train/{0}.jpg".format(fer_match_index+1))
        im = Image.open(ferpath)
        im = make_print_version(im)
        if rotate:
            im = im.rotate(-90, expand=1)
        im.save("print02.jpg")

        # copy the original
        opath = os.path.join(args.media, "photos", photo_data['id'].replace(".o.jpg", ".print.jpg"))
        shutil.copyfile(opath, "print03.jpg")

        ps = {}
        ps['items'] = psi = []
        psi.append({'text': 'The Recognition Machine'})
        psi.append({'text': datetime.datetime.now().strftime("%d %b %Y %H:%M:%S+UTC")})
        psi.append({'imagef': "print01.jpg"})
        psi.append({'imagef': "print02.jpg"})
        psi.append({'imagef': "print03.jpg"})
        lines = unparse_emotion_summary(fe[0], feremotions[fer_match_index], rmemotions[rm_match_index])
        for l in lines:
            psi.append({'text': l})        
        print_photostrip(ps)
        # face = self.first_face()
        # if face:
        #     psi.append({'text': face.make_caption()})
else:
    # NO MATCH
    with open(os.path.join(args.media, "data/photos/nofaces.json")) as inf:
        nofaces = json.load(inf)
    from random import choice 
    photo_id = choice(nofaces)
    with open (os.path.join(args.media, "data/photos/{0:06d}.json".format(photo_id))) as inf:
        photo_data = json.load(inf)

    rotate = photo_data['imageWidth']>photo_data['imageHeight']

    im = Image.open(args.image)
    if rotate:
        # print ("rotating")
        im = im.rotate(-90, expand=1)
    # print ("im", im, im.size)
    im = make_print_version(im)
    im.save("print01.jpg")

    opath = os.path.join(args.media, "photos", photo_data['id'].replace(".o.jpg", ".print.jpg"))
    shutil.copyfile(opath, "print02.jpg")
   
    ps = {}
    ps['items'] = psi = []
    psi.append({'text': 'The Recognition Machine'})
    psi.append({'text': datetime.datetime.now().strftime("%d %b %Y %H:%M:%S+UTC")})
    psi.append({'text': 'NO FACES DETECTED'})
    psi.append({'text': ''})
    psi.append({'imagef': "print01.jpg"})
    psi.append({'imagef': "print02.jpg"})
    print_photostrip(ps)
