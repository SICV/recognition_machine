(function () {

var socket = io.connect('http://' + document.domain + ':' + location.port);
var width = 480,
    height = 640;
var connected_checkbox = $("#connected"),
    photo_button = $("#photo"),
    faces_button = $("#faces"),
    shapes_button = $("#shapes"),
    emotions_button = $("#emotions"),
    genderage_button = $("#genderage"),
    canvas = $("#canvas"),
    ctx = canvas.getContext("2d");
var current_faces = [],
    current_shapes = [],
    current_emotions = [],
    current_genderage = [];

canvas.width = 480;
canvas.height = 640;

function $(s) {
    return document.querySelector(s);
}

socket.on('connect', function() {
    console.log("connected");
    connected_checkbox.checked = true;
});
socket.on('disconnect', function() {
    console.log("disconnected");
    connected_checkbox.checked = false;
});

/*
    socket.on('cameraframe', function (data) {
        // console.log("cameraframe", data);
        // console.log("cameraframe", data.faces);
        document.getElementById("image").src = data.image;
        socket.emit('camera', {data: 'connected!'});
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.strokeStyle = "hsla(0, 100%, 50%, 0.5)";
        ctx.lineWidth = 3;
        for (var fi=0, fl=data.faces.length; fi<fl; fi++) {
            var rect = data.faces[fi];
            ctx.strokeRect(rect[0], rect[1], rect[2], rect[3]);
        }

    });
*/


 
photo_button.addEventListener("click", function () {
    console.log("emitting photo");
    var sleep = parseFloat($("#sleep").value);
    socket.emit("photo", {sleep: sleep});
});
faces_button.addEventListener("click", function () {
    socket.emit("faces", {});
});
shapes_button.addEventListener("click", function () {
    console.log("emitting shapes");
    socket.emit("shapes", {});
});
emotions_button.addEventListener("click", function () {
    console.log("emitting emotions");
    socket.emit("emotions", {});
});
genderage_button.addEventListener("click", function () {
    console.log("emitting genderage");
    socket.emit("genderage", {});
});

// SOCKET EVENT HANDLERS
socket.on("photo", function (data) {
    console.log("photo", data);
    document.getElementById("image").src = data.image;
    $("#exposure_mode").innerHTML = data.exposure_mode;
    $("#shutter_speed").innerHTML = data.shutter_speed;
    $("#awb_mode").innerHTML = data.awb_mode;
    $("#awb_gains").innerHTML = data.awb_gains;
    current_faces = [];
    current_shapes = [];
    current_emotions = [];
    current_genderage = [];
});

socket.on("faces", function (data) {
    console.log("faces", data);
    current_faces = data.faces;
    redraw();
});
socket.on("shapes", function (data) {
    console.log("shapes", data);
    current_shapes = data.shapes;
    redraw();
});
socket.on("emotions", function (data) {
    console.log("emotions", data);
    current_emotions = data.emotions;
    redraw();
});
socket.on("genderage", function (data) {
    console.log("genderage", data);
    current_genderage = data.genderage;
    redraw();
});

function draw_face68 (ctx, f) {
    // chin/face
    ctx.beginPath();
    for (var i=0; i<17; i++) {
        var p = f[i];
        (i == 0) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // left brow
    ctx.beginPath();
    for (var i=17; i<22; i++) {
        var p = f[i];
        (i == 17) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // right brow
    ctx.beginPath();
    for (var i=22; i<27; i++) {
        var p = f[i];
        (i == 22) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // nose bridge
    ctx.beginPath();
    for (var i=27; i<31; i++) {
        var p = f[i];
        (i == 27) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // undernose
    ctx.beginPath();
    for (var i=31; i<36; i++) {
        var p = f[i];
        (i == 31) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // left eye
    ctx.beginPath();
    for (var i=36; i<42; i++) {
        var p = f[i];
        (i == 36) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();

    // right eye
    ctx.beginPath();
    for (var i=42; i<48; i++) {
        var p = f[i];
        (i == 42) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();

    // outer lips
    ctx.beginPath();
    for (var i=48; i<60; i++) {
        var p = f[i];
        (i == 48) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();

    // inner lips
    ctx.beginPath();
    for (var i=60; i<68; i++) {
        var p = f[i];
        (i == 60) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();
}

function draw_emotion_pie (dd, context, x, y, radius) {
    // console.log("draw_pie", x, y, radius);
    radius = 100;
    var colors = ["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"];

    var arc = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(0)
        .context(context);

    var labelArc = d3.arc()
        .outerRadius(radius - 40)
        .innerRadius(radius - 40)
        .context(context);

    var pie = d3.pie()
        .sort(null)
        .value(function(d) { return d.value; });

    context.translate(100, 100);
    var data = [];
    for (var key in dd[0]) {
        data.push({label: key, value: dd[0][key]});
    }
    console.log("data", data);
    var arcs = pie(data);

    arcs.forEach(function(d, i) {
        context.beginPath();
        arc(d);
        context.fillStyle = colors[i];
        context.fill();
    });

    context.beginPath();
    arcs.forEach(arc);
    context.strokeStyle = "#fff";
    context.stroke();

    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = "#000";
    arcs.forEach(function(d) {
    var c = labelArc.centroid(d);
        context.fillText(d.data.label, c[0], c[1]);
    });
    context.translate(-100, -100);

}

function redraw () {
    console.log("redraw", current_faces, current_shapes);
    ctx.clearRect(0, 0, width, height);
    // ctx.fillStyle = "#FFFF00";
    // ctx.fillRect(0, 0, 100, 100);
    if (current_faces.length > 0) {
        ctx.lineWidth = 5;
        ctx.strokeStyle = "rgb(255,0,0)";
        for (var fi=0, fl=current_faces.length; fi<fl; fi++) {
            ctx.beginPath();
            var f = current_faces[fi];
            ctx.strokeRect(f.x, f.y, f.width, f.height);
        }
    }
    
    if (current_shapes) {
        ctx.strokeStyle = "rgb(0,255,0)";
        ctx.lineWidth = 2;
        for (var si=0, sl=current_shapes.length; si<sl; si++) {
            var cs = current_shapes[si];
            draw_face68(ctx, cs);
        }
    }
    if (current_emotions) {
        draw_emotion_pie(current_emotions, ctx, width/2, height/2, width/2);
    }
            
}

console.log('ye', d3);  

})();
