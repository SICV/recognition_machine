var face_state = ""; // "wait";
var content = document.querySelector("#content");
var canvas = document.getElementById("canvas"),
    ctx = canvas.getContext("2d"),
    width,
    height,
    camera_width,
    camera_height;
var image = document.getElementById("image");
// var socket = io.connect('http://' + document.domain + ':' + location.port);
var current_faces, current_landmarks;


function resize() {
    canvas.width = width = window.innerWidth;
    canvas.height = height = window.innerHeight;    
    console.log("resize", width, height);
    if (rm) { redraw(rm) };
}
window.addEventListener("DOMContentLoaded", function () {
    console.log("loaded");
    resize();
});
window.addEventListener("resize", resize);



function set_camera_size(w, h) {
    camera_width = w;
    camera_height = h;
    content.style.gridTemplateColumns = "1fr "+w+"px 1fr";
    content.style.gridTemplateRows = "1fr "+h+"px 1fr";
}


function face_is_centered (f) {
	console.log("face_is_centered", image.width, image.height);
    var fx = f[0],
        fy = f[1],
        fw = f[2],
        fh = f[3],
        cfx = fx + (fw / 2),
        cfy = fy + (fh / 2),
        cx = image.width/2,
        cy = image.height/2,
        dx = Math.abs(cx - cfx),
        dy = Math.abs(cy - cfx);
    console.log("is_centered", dx, dy);
    if (dx < 100 && dy < 100) {
        return true;
    }

    return false;
}

function draw_face68 (ctx, f, ox, oy) {
    ox = ox || 0;
    oy = oy || 0;
    // chin/face
    ctx.beginPath();
    for (var i=0; i<17; i++) {
        var p = f[i];
        (i == 0) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // left brow
    ctx.beginPath();
    for (var i=17; i<22; i++) {
        var p = f[i];
        (i == 17) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // right brow
    ctx.beginPath();
    for (var i=22; i<27; i++) {
        var p = f[i];
        (i == 22) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // nose bridge
    ctx.beginPath();
    for (var i=27; i<31; i++) {
        var p = f[i];
        (i == 27) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // undernose
    ctx.beginPath();
    for (var i=31; i<36; i++) {
        var p = f[i];
        (i == 31) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();

    // left eye
    ctx.beginPath();
    for (var i=36; i<42; i++) {
        var p = f[i];
        (i == 36) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();

    // right eye
    ctx.beginPath();
    for (var i=42; i<48; i++) {
        var p = f[i];
        (i == 42) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();

    // outer lips
    ctx.beginPath();
    for (var i=48; i<60; i++) {
        var p = f[i];
        (i == 48) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();

    // inner lips
    ctx.beginPath();
    for (var i=60; i<68; i++) {
        var p = f[i];
        (i == 60) ? ctx.moveTo(p.x, p.y) : ctx.lineTo(p.x, p.y);
    }
    ctx.closePath();
    ctx.stroke();
}


function redraw (rm) {
    var image_offset = image.getBoundingClientRect();
    // console.log("image_offset", image_offset);
    ctx.clearRect(0, 0, width, height);
    if (face_state == "wait") {
        console.log("drawing wait");
        ctx.lineWidth = 5;
        ctx.strokeStyle = "rgb(255,255,255)";
        ctx.beginPath();
        ctx.moveTo(width/2, 0);
        ctx.lineTo(width/2, height);
        // ctx.closePath();
        ctx.stroke();
        ctx.moveTo(0, height/2);
        ctx.lineTo(width, height/2);
        ctx.stroke();

    } else if (face_state == "reject") {
        ctx.lineWidth = 5;
        ctx.strokeStyle = "rgb(255,0,0)";
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(width, height);
        ctx.stroke()
        ctx.moveTo(width, 0);
        ctx.lineTo(0, height);
        ctx.stroke();
    } else if (face_state == "accept") {
        ctx.lineWidth = 5;
        ctx.strokeStyle = "rgb(0,255,0)";
        ctx.beginPath();
        var arcR = height / 2;
        ctx.arc(width/2, height/2, arcR, 0, Math.PI*2);
        ctx.stroke()
    }
    if (current_faces) {
        ctx.strokeStyle = "hsla(0, 100%, 50%, 0.5)";
        ctx.lineWidth = 3;
        for (var fi=0, fl=current_faces.length; fi<fl; fi++) {
            var rect = current_faces[fi];
            ctx.strokeRect(image_offset.x + rect.x, image_offset.y + rect.y, rect.width, rect.height);
        }

    }
    if (current_landmarks) {
        ctx.strokeStyle = "hsla(0, 100%, 50%, 0.5)";
        ctx.lineWidth = 3;
        ctx.translate(image_offset.x, image_offset.y);
        draw_face68(ctx, current_landmarks[0]);
        var vor = d3.voronoi();
        vor.x(function (d) { return d.x; });
        vor.y(function (d) { return d.y; });
        var f=current_faces[0];
        var x1 = f.x, y1 = f.y, x2 = f.x+f.width, y2 = f.y+f.height;
        vor.extent([[x1-50, y1-50], [x2+50, y2+50]])
        var polys = vor.triangles(current_landmarks[0]);
        console.log("polys", polys);
        polys.forEach(function (p) {
            ctx.beginPath();
            for (var i=0, l=p.length; i<l; i++) {
                if (i == 0) {
                    //ctx.moveTo(p[i][0], p[i][1]);
                    ctx.moveTo(p[i].x, p[i].y);
                } else {
                    //ctx.lineTo(p[i][0], p[i][1]);
                    ctx.lineTo(p[i].x, p[i].y);
                }
            }
            ctx.closePath();
            ctx.stroke();
        })
        ctx.translate(-image_offset.x, -image_offset.y);        
    }

}

var rm = new StateMachine({
    init: "wait",
    transitions: [
        {name: 'tolook', from: 'wait', to: 'look'},
        {name: 'towait', from: 'look', to: 'wait'},
    ],
    methods: {
        onWait: function (transition) {
            // request a frame
            // if there's a face move to the reject state to start the control phase
            window.setTimeout(function () {
                transition.fsm.tolook();
            }, 2500)
            // image.style.visibility = "hidden";
        },
        onLook: function (transition) {
            console.log("emitting photo");
            d3.json("/photo").then(function (data) {
                if (!camera_width) {
                    set_camera_size(data.width, data.height);
                }
                image.src = data.image;
                d3.json("/faces").then(function (data) {
                    console.log("faces", data);
                    if (data.faces.length == 1) {
                        current_faces = data.faces;
                        redraw();
                        d3.json("/landmarks").then(function (data) {
                            current_landmarks = data.landmarks;
                            redraw();
                        });
                    } else {
                        transition.fsm.towait();
                    }
                })
            });
        },

        onEnterState: function (transition) {
            console.log("entering state", transition.to);
            // console.log(transition.transition, "entering state", transition.to, "from", transition.from);
            var b = document.body;
            b.classList.remove("state-"+transition.from);
            b.classList.add("state-"+transition.to);
            redraw(transition.fsm);
        }
    }
});

// console.log("rm", rm.state);
redraw(rm);

