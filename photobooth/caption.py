AGE_CHOICES = (
    ('0,5', 'infant'),
    ('5,12', 'infant'),
    ('12,20', 'adolescent'),
    ('20,35', 'young'),
    ('35,50', 'middle-aged'),
    ('50,65', 'older'),
    ('65,110', 'elderly')
)
def map_age (n):
    for val, label in AGE_CHOICES:
        low, high = [float(x) for x in val.split(",")]
        # print ("age_map {0} {1} {2} {3}".format(x, low, high, val, label))
        if n >= low and n < high:
            return label

def get_emotions_sorted (d):
    ret = []
    ret.append((d['emotion']['angry'], "angry"))
    ret.append((d['emotion']['disgust'], "disgust"))
    ret.append((d['emotion']['fear'], "fear"))
    ret.append((d['emotion']['happy'], "happy"))
    ret.append((d['emotion']['sad'], "sad"))
    ret.append((d['emotion']['neutral'], "neutral"))
    ret.append((d['emotion']['surprise'], "surprise"))
    ret.sort(reverse=True)
    return ret

def caption(d, max_length=32):
    lines = []
    if 'gender' in d:
        if d['gender']['label'] == "male":
            n = "man"
        else:
            n = "woman"
        if 'emotion' in d:
            elabel = d['emotion']['label']+" "
        else:
            elabel = ""
        title = "\"{0}{1} {2}\"".format(elabel, map_age(d['age']), n)
        lines.append(title.upper())
        if d['gender']['male'] != None:
            lines.append("male({0:0.01f}%)/female({1:0.01f}%)".format(d['gender']['male']*100.0, d['gender']['female']*100.0))

    if 'age' in d:
        lines.append("age {0:0.1f}".format(d['age']))
    if 'emotion' in d:
        es = get_emotions_sorted(d)
        lines.append("/".join(["{0}({1:0.01f}%)".format(label, value*100.0) for value, label in es[:2]]))

    if max_length:
        lines = [x[:max_length] for x in lines]

    return "\n".join(lines)
