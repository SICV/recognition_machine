from flask import Flask, render_template
from flask_socketio import SocketIO

import cv2
from time import sleep
from imutils.video import VideoStream


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route("/")
def hello():
    return """<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.6/socket.io.min.js"></script>
<script type="text/javascript" charset="utf-8">
    var socket = io.connect('http://' + document.domain + ':' + location.port);
    socket.on('connect', function() {
        console.log("connected");
        socket.emit('camera', {data: 'connected!'});
    });
</script>"""

@socketio.on('camera')
def handle_camera(json):
    print('camera json: {0}', str(json))

if __name__ == '__main__':
    socketio.run(app)
