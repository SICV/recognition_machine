from imutils.video import VideoStream
import cv2, os
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
import dlib
import numpy as np
from time import sleep


IMG_SIZE = 64
DEPTH = 16
K = WIDTH = 8
MARGIN = 0.4


def take_photo(camera, rotate, width, height, sleept=2.0, iso=400):
    print ("take photo")
    if camera == -1:
        import picamera, picamera.array
        with picamera.PiCamera(resolution=(width, height)) as camera:
            # camera.resolution = ()
            camera.iso = iso
            if sleept:
                print ("sleep", sleep)
                sleep(sleept)
            ss = camera.exposure_speed
            camera.shutter_speed = ss
            camera.exposure_mode = 'off'
            g = camera.awb_gains
            camera.awb_mode = 'off'
            camera.awb_gains = g

            with picamera.array.PiRGBArray(camera) as frame:
                camera.capture(frame, 'bgr')
                print("got frame", frame)
                current_image_bgr = frame.array
                gray = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2GRAY)
                if rotate:
                    gray = imutils.rotate_bound(gray, rotate)
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, rotate)
                else:
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, 0)
                current_image = gray
                # print('Captured %dx%d image' % (
                #         output.array.shape[1], output.array.shape[0]))
                # _, imdata = cv2.imencode(".jpg", gray)
                # data["height"], data["width"] = gray.shape[:2]
                # data["image"] = make_jpeg_data_url(imdata)
                # emit("photo", data)
                # return jsonify(data)
                return current_image, current_image_bgr
    else:
        stream = cv2.VideoCapture(camera)
        (grabbed, frame) = stream.read()
        current_image_bgr = frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if rotate:
            gray = imutils.rotate_bound(gray, rotate)
            current_image_bgr = imutils.rotate_bound(current_image_bgr, rotate)
        current_image = gray
        _, imdata = cv2.imencode(".jpg", gray)
        # data={}
        # data["height"], data["width"] = gray.shape[:2]
        # data["image"] = make_jpeg_data_url(imdata)
        # emit("photo", data)
        stream.release()
        return current_image, current_image_bgr


# def make_jpeg_data_url(binary_data):
#     prefix = 'data:image/jpeg;base64,'
#     # fin = open(filename, 'rb')
#     # contents = fin.read()
#     return prefix + base64.b64encode(binary_data)

def get_faces (face_cascade, current_image, scaleFactor=None, minNeighbors=None, minSize=None):
    print ("get_faces")

    # input_img = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2RGB)
    # img_h, img_w, _ = np.shape(input_img)

    # detect faces using dlib detector
    # img_size = 64
    # faces = face_detector(input_img, 1)
    # faces = np.empty((len(detected), img_size, img_size, 3))
    # return faces

    args = dict()
    if scaleFactor:
        args['scaleFactor'] = scaleFactor
    if minNeighbors:
        args['minNeighbors'] = minNeighbors
    if minSize:
        args['minSize'] = (minSize, minSize)
    gray = current_image
    faces = face_cascade.detectMultiScale(gray, **args)
    ret=[]
    current_faces = []
    for rect in faces:
        current_faces.append(rect)
        x, y, w, h = rect
        #ret.append([int(x), int(y), int(w), int(h)])
        ret.append(dlib.rectangle(x, y, x+w, y+h))
    return ret

def get_landmarks(shape_predictor, current_image, current_faces):
    print ("get_landmarks")
    shapes = []
    for rect in current_faces:
        ss = shape_predictor(current_image, rect)
        ss = face_utils.shape_to_np(ss)
        print ("got shape", ss)
        shapes.append([{'x': int(x[0]), 'y': int(x[1])} for x in ss])
    data = {}
    data['landmarks'] = shapes
    return data

def get_emotions(emotion_classifier, current_image, current_faces):
    print ("get_emotions")
    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_offsets = (20, 40)
    # fer2013
    # emotion_labels = {0:'angry',1:'disgust',2:'fear',3:'happy', 4:'sad',5:'surprise',6:'neutral'}
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    dd = []
    for rect in current_faces:
        face_coordinates = (rect.left(), rect.top(), rect.width(), rect.height())
        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        gray_face = current_image[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except:
            print ("exception resizing gray_face")
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        # emotion_probability = np.max(emotion_prediction)
        # emotion_label_arg = np.argmax(emotion_prediction)
        # emotion_text = emotion_labels[emotion_label_arg]
        # emotion_window.append(emotion_text)
        d = {}
        for label, score in zip(emotion_labels, emotion_prediction[0]):
            d[label] = float(score)
        print ("got emotion_prediction", d)
        dd.append(d)
    return {'emotions': dd}

def get_genderage(genderage_classifier, current_image_bgr, current_faces):
    print ("get_genderage")

    img = current_image_bgr
    img_h, img_w, _ = np.shape(img)

    # detect faces using dlib detector
    # detected = detector(input_img, 1)
    detected = current_faces
    faces = np.empty((len(detected), IMG_SIZE, IMG_SIZE, 3))

    data = {}
    if len(detected) > 0:
        for i, d in enumerate(detected):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            xw1 = max(int(x1 - MARGIN * w), 0)
            yw1 = max(int(y1 - MARGIN * h), 0)
            xw2 = min(int(x2 + MARGIN * w), img_w - 1)
            yw2 = min(int(y2 + MARGIN * h), img_h - 1)
            # cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
            # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i, :, :, :] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (IMG_SIZE, IMG_SIZE))

        # predict ages and genders of the detected faces
        results = genderage_classifier.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages_raw = results[1]
        print ("predicted_ages", predicted_ages_raw, predicted_ages_raw.shape)
        predicted_ages = results[1].dot(ages).flatten()
        # print ("predicted_ages", predicted_ages, predicted_ages[0], type(predicted_ages[0]))
        data['ages'] = [float(x) for x in predicted_ages]
        data['genders'] = [{'female': float(x[0]), 'male': float(x[1])} for x in predicted_genders]
        print ("predicted_genderage", data)
        # print ("predicted_genders", predicted_genders)

    return data

# if __name__ == "__main__":

ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=270)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument("--genderage", default=False, action="store_true")
args = ap.parse_args()

MODELS = os.path.expanduser(args.models)

print ("Loading face detector")
# face_detector = dlib.get_frontal_face_detector()
face_cascade = cv2.CascadeClassifier(os.path.join(MODELS, 'haarcascade_frontalface_default.xml'))
print ("Loading shape predictor")
shape_predictor = dlib.shape_predictor(os.path.join(MODELS, "shape_predictor_68_face_landmarks.dat"))
if args.emotion:
    print ("Loading emotion classifier")
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(MODELS, "emotion_model.hdf5"))
if args.genderage:
    print ("Loading genderage classifier")
    # from keras.models import load_model
    from keras.utils.data_utils import get_file
    weight_file = os.path.join(MODELS, "weights.28-3.73.hdf5")
    from wide_resnet import WideResNet
    genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
    genderage_classifier.load_weights(weight_file)
print ("All loaded")


while True:
    current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
    current_faces = get_faces(face_cascade, current_image_bgr)
    print ("got_faces", current_faces)
    if len(current_faces) == 1:
        cv2.imwrite("image.jpg", current_image)
        fr = current_faces[0]
        x1, y1, x2, y2 = fr.left(), fr.top(), fr.right(), fr.bottom()
        current_face = current_image[y1:y2, x1:x2]
        cv2.imwrite("face.jpg", current_face)
        landmarks = get_landmarks(shape_predictor, current_image, current_faces)
        if args.emotion:
            emotion = get_emotions(emotion_classifier, current_image, current_faces)
        if args.genderage:
            genderages = get_genderage(genderage_classifier, current_image_bgr, current_faces)
        print("sleeping for 10")
        sleep(10.0)

    else:
        print ("no faces")
