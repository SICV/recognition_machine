from __future__ import print_function
from imutils.video import VideoStream
import cv2, os, sys, json
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
# import dlib
import numpy as np
from time import sleep
import datetime

from common import *
# import subprocess

# if __name__ == "__main__":

ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=0)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument('--gpiobutton', type=int, default=None, help="use given gpio pin as button")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
args = ap.parse_args()

face_cascade = cv2.CascadeClassifier(os.path.join(args.models, 'haarcascade_frontalface_default.xml'))

def wait_for_button(pin):
    while True:
        button_state = GPIO.input(args.gpiobutton)
        if button_state == False:
            return True
        sleep(0.1)

def p (msg=""):
    if msg.strip():
        os.system("espeak \"{0}\" 2> /dev/null".format(msg))
    print (msg)

def tone ():
    os.system("aplay beep.wav 2> /dev/null")


if args.gpiobutton:
    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    # LIGHT
    GPIO.setup(4, GPIO.OUT)
    # BUTTON
    GPIO.setup(args.gpiobutton, GPIO.IN, pull_up_down=GPIO.PUD_UP)#Button to GPIO23

while True:
    if args.gpiobutton:
        wait_for_button(args.gpiobutton)
    else:
        raw_input("Press THE BUTTON to take a photo")

    tone()

    # LIGHT
    if args.gpiobutton:
        GPIO.output(4, GPIO.HIGH)

    current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
    if args.gpiobutton:
        GPIO.output(4, GPIO.LOW)
    n = datetime.datetime.now()
    base = n.strftime("%Y%m%d_%H%M%S")
    image_fname = os.path.join("images", base + ".jpg")
    print ("image saved to {0}".format(image_fname), file=sys.stderr)
    cv2.imwrite(image_fname, current_image)

    current_faces = get_faces(face_cascade, current_image_bgr)
    n = datetime.datetime.now()
    if len(current_faces) > 0:
        if len(current_faces) > 1:
            s = 's'
        else:
            s = ''

        p ("{0} face{1}".format(len(current_faces), s))
    else:
        p("No face detected")

    sleep(3.0)

if args.gpiobutton:
    GPIO.cleanup()
