import RPi.GPIO as gpio
from time import sleep

import argparse
ap = argparse.ArgumentParser("")
ap.add_argument("--on", action="store_true", default=False)
ap.add_argument("--off", action="store_true", default=False)
ap.add_argument("--pin", type=int, default=4)
args = ap.parse_args()

pin = args.pin
gpio.setmode(gpio.BCM)
gpio.setwarnings(False)
gpio.setup(pin, gpio.OUT)
pwm = gpio.PWM(pin, 100)
pwm.start(0)

try:
	for p in range(5):
		for i in range(0, 100, 1):
			pwm.ChangeDutyCycle(i)
			sleep(0.01)
		for i in range(100, 0, -1):
			pwm.ChangeDutyCycle(i)
			sleep(0.01)
except KeyboardInterrupt:
	pass
pwm.stop()
gpio.cleanup()
