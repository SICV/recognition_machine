
## Wiring the pimajoi


in my case:
purple: GPIO
gray: GND1
white: GND2
black: 3V

black: pin 2 - 5V (top-right pin of PI)
gray: pin 6 - Ground (3 from top on right)
purple: pin 7 - GPIO4 (4 down on left) 



## Startup

Useful!
https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/


### /etc/rc.local
Added (at end before exit 0)
```
/home/pi/start.sh &
exit 0
```

### /home/pi/start.sh
```
python /home/pi/project/recogntion_machine/cameraserver/light/light.py --off
```
and:

  chmod +x start.sh


# Printer
<https://mike42.me/blog/2015-03-getting-a-usb-receipt-printer-working-on-linux>

  sudo adduser pi lp

  ls /dev/usb
  echo "Hello" >> /dev/usb/lp1

Using python-escpos

lsusb

```
Bus 001 Device 005: ID 0416:5011 Winbond Electronics Corp. Virtual Com Port
Bus 001 Device 003: ID 0424:ec00 Standard Microsystems Corp. SMSC9512/9514 Fast Ethernet Adapter
Bus 001 Device 002: ID 0424:9514 Standard Microsystems Corp. SMC9514 Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

It's the first one! So address is 0416:5011


Get the Product ID and Vendor ID from the lsusb command # lsusb  Bus 002 Device 001: ID 1a2b:1a2b Device name


Create a udev rule to let users belonging to dialout group use the printer. You can create the file
    /etc/udev/rules.d/99-escpos.rules
and add the following:

SUBSYSTEM=="usb", ATTRS{idVendor}=="0416", ATTRS{idProduct}=="5011", MODE="0664", GROUP="dialout"

Replace idVendor and idProduct hex numbers with the ones that you got from the previous step. Note that you can either, add yourself to “dialout” group, or use another group you already belongs instead “dialout” and set it in the GROUP parameter in the above rule.

Restart udev # sudo service udev restart In some new systems it is done with
```
sudo udevadm control --reload
```


# Hotspot / networking

## Install software

```
sudo apt-get install dnsmasq wireless-tools hostapd
```

## Check that the firmware is installed and module loaded

```
sudo apt-get install firmware-brcm80211
rmmod brcmfmac
sudo modprobe brcmfmac
```

In any case, ulimately the important thing is that when you type:
```
lsmod
```

You see that the "brcmfmac" module is loaded, for example:

```
Module                  Size  Used by
...
brcmfmac              307200  0
...
```

## Fixed IP

We are configuring a standalone network to act as a server, so the Raspberry Pi needs to have a static IP address assigned to the wireless port (wlan0). We will assign the server the IP address 192.168.4.1.

sudo nano /etc/dhcpcd.conf
```
# ADD TO THE END: 
# STATIC IP FOR HOTSPOT (comment out to use wpa_supplicant)
interface wlan0 
static ip_address=10.9.8.7/24
nohook wpa_supplicant
```

Configure DNSMASQ to do DHCP

sudo nano /etc/dnsmasq.conf
```
# ADD TO END
interface=wlan0
dhcp-range=10.9.8.50,10.9.8.250,255.255.255.0,24h
```

## Configure HOSTAPD with hotspot name + password

Create the following new file:

sudo nano /etc/hostapd/hostapd.conf
```
# ADD (NEW FILE)
interface=wlan0
driver=nl80211
ssid=Supi8_YOURNAME
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=YourGreatWifiPassword
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

Enable the above configuration file (NB: it should already exist):

sudo nano /etc/default/hostapd
```
# EDIT the line with DAEMON_CONF TO:
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```

## Make hostapd starts at boot

**This step seems to be done automatically when installing hostapd and may thus not be necessary**
```
update-rc.d hostapd defaults
```

## Reboot

```
sudo reboot
```

After all these changes you should be able to reboot and (eventually) find and connect to the hotspot.

## Connect to the Pi via it's own hotspot

```
ssh pi@10.9.8.7
```

NB: The IP address of the Pi is now DIFFERENT than it was before. Namely it is the FIXED IP address you configured ...

## Disable the hotspot and use connect to the Internet again via wpa_supplicant settings

To disable the hotspot and fallback to using wpa_supplicant (to connect again to the Internet via another hotspot)...

Comment out the lines you added to:
/etc/dhcpcd.conf
/etc/dnsmasq.conf
/etc/default/hostapd

sudo nano /etc/dhcpcd.conf
```
# STATIC IP FOR HOTSPOT (comment out to use wpa_supplicant)
#interface wlan0 
#static ip_address=192.168.4.1/24
#nohook wpa_supplicant
```

And REBOOT
The pi should use the wpa_supplicant.conf info to login again to an existing wifi network


