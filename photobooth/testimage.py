from common import *



ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=0)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument("--genderage", default=False, action="store_true")
ap.add_argument("--post", default=None, help="post images to given URL")
args = ap.parse_args()


current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
#current_faces = get_faces(face_cascade, current_image_bgr)
#if len(current_faces) > 0:
#    n = datetime.datetime.now()
#    base = n.strftime("%Y%m%d_%H%M%S")
#    print (datetime.datetime.now())

cv2.imwrite("test.jpg", current_image)

