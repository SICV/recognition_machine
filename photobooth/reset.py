import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)#Button
GPIO.setup(4, GPIO.OUT)
GPIO.setup(19, GPIO.OUT)  #LED
GPIO.output(4, False)  #LED
GPIO.output(19, False)  #LED

GPIO.cleanup()
