from __future__ import print_function, division
from imutils.video import VideoStream
import cv2, os, sys, json
import argparse
import base64
import imutils
from imutils import face_utils
import numpy as np
from time import sleep
# from eventlet.greenthread import sleep
from PIL import Image, ImageOps
import shutil
from common import *
from printing import *
from scipy.spatial.distance import cdist
import qrcode
import datetime
from math import floor

# import subprocess
# import dlib

# if __name__ == "__main__":

ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=0)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument("--genderage", default=False, action="store_true")
# ap.add_argument("--post", default=None, help="post images to given URL")

ap.add_argument('--feremotions', default="../website/fer2013_emotions.npy", help="path to fer2013 matrix of emotion predictions")
ap.add_argument('--ferlabels', default="../website/fer2013_labels.npy", help="path to fer2013 matrix of labels")
ap.add_argument('--rmemotions', default="../website/rm_emotions.npy", help="path to rm recognition matrix of emotion predictions")
ap.add_argument('--rmlabels', default="../website/rm_labels.npy", help="path to rm matrix of labels")
# ap.add_argument('--data', default="../website/media/data", help="path to db dumps")
ap.add_argument('--media', default="../website/media", help="path to website media")

ap.add_argument('--gpiobutton', type=int, default=None, help="use given gpio pin as button")
ap.add_argument("--audio", default=False, action="store_true", help="play the audio prompts")

args = ap.parse_args()

MODELS = os.path.expanduser(args.models)

def p (msg=""):
    if msg.strip():
        os.system("espeak \"{0}\" 2> /dev/null".format(msg))
    print (msg)

def tone ():
    os.system("aplay beep.wav 2> /dev/null")


def wait_for_button(pin):
    while True:
        button_state = GPIO.input(args.gpiobutton)
        if button_state == False:
            return True
        sleep(0.1)

if args.gpiobutton:
    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    # LIGHT
    GPIO.setup(4, GPIO.OUT)
    GPIO.setup(19, GPIO.OUT)  #BUTTONLED
    # BUTTON
    GPIO.setup(args.gpiobutton, GPIO.IN, pull_up_down=GPIO.PUD_UP)#Button to GPIO23




# print ("Loading face detector", file=sys.stderr)
# face_detector = dlib.get_frontal_face_detector()
face_cascade = cv2.CascadeClassifier(os.path.join(MODELS, 'haarcascade_frontalface_default.xml'))
# print ("Loading shape predictor")
# shape_predictor = dlib.shape_predictor(os.path.join(MODELS, "shape_predictor_68_face_landmarks.dat"))

if args.emotion:
    p("loading loading the recognition machine")
    # print ("Loading emotion classifier", file=sys.stderr)
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(MODELS, "emotion_model.hdf5"))
if args.genderage:
    p ("loading genderage classifier")
    # from keras.models import load_model
    from keras.utils.data_utils import get_file
    weight_file = os.path.join(MODELS, "weights.28-3.73.hdf5")
    from wide_resnet import WideResNet
    genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
    genderage_classifier.load_weights(weight_file)


# Load the comparison matrices
feremotions = np.load(args.feremotions)
ferlabels = np.load(args.ferlabels)
rmemotions = np.load(args.rmemotions)
rmlabels = np.load(args.rmlabels)

EMOTION_CHOICES = (
    ('angry', 'angry'),
    ('disgust', 'disgust'),
    ('fear', 'fear'),
    ('happy', 'happy'),
    ('sad', 'sad'),
    ('surprise', 'surprise'),
    ('neutral', 'neutral')
)
def unparse_emotion_vector (e):
    for i in range(7):
        print ("{1:0.01f}% {0}".format(EMOTION_CHOICES[i][0], float(e[i])*100))

def unparse_emotion_summary (v1, v2, v3):
    """
    12345678901234567890123456789012
               YOU    FER     RM
    angry    53.7%  53.5%  46.9%
    disgust   1.0%   0.0%
    fear      5.6%
    happy     0.1%
    sad       8.7% 
    surprise  2.7% 
    neutral  28.1% 
    """
    ret=[]
    ret.append("*            YOU    FER     RM")
    for i in range(7):
        label = EMOTION_CHOICES[i][0]
        line = " {0: <8}  {1: >04.01f}%  {2: >04.01f}%  {3: >04.01f}%".format(label, v1[i]*100, v2[i]*100, v3[i]*100)
        ret.append(line)
    return ret

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def make_print_version(im):
    im = im.convert("L")
    rw, rh = fit_size(im, (400, 10000))
    print ("rw, rh", rw, rh)
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.equalize(im)
    im = im.convert("1")
    return im

def make_qrcode (photo_id, path):
    url = "http://recognitionmachine.constantvzw.org/r/{0}".format(photo_id)
    im = qrcode.make(url, border=0)  
    im = im.resize((386, 386), resample=Image.NEAREST)
    im.save(path)
    return url

p("all loaded")



while True:
    if args.gpiobutton:
        GPIO.output(19, True)
        wait_for_button(args.gpiobutton)
        GPIO.output(19, False)
    else:
        raw_input("Press THE BUTTON to take a photo")

    if args.audio:
        os.system("omxplayer -o both audio/01.mp4")


    # LIGHT
    if args.gpiobutton:
        GPIO.output(4, GPIO.HIGH)

    if args.audio:
        # os.system("omxplayer -o both audio/02.mp3")
        os.system("omxplayer -o both audio/02.mp4 &")
        sleep(2)
    else:
        tone()

    current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)

    if args.gpiobutton:
        GPIO.output(4, GPIO.LOW)
    if args.audio:
        os.system("killall omxplayer.bin")

    current_faces = get_faces(face_cascade, current_image_bgr)
    n = datetime.datetime.now()
    base = n.strftime("%Y%m%d_%H%M%S")
    print (datetime.datetime.now())
    if len(current_faces) > 0:
        if len(current_faces) > 1:
            s = 's'
        else:
            s = ''
        if not args.audio:
            p ("{0} face{1}".format(len(current_faces), s))
        if len(current_faces) > 0 and args.audio:
            # PHOTO TAKEN -- WE ARE CLASSIFYING...
            os.system("omxplayer -o both audio/03.mp4")

        if args.emotion:
            get_emotions(emotion_classifier, current_image, current_faces)
        if args.genderage:
            get_genderage(genderage_classifier, current_image_bgr, current_faces)

        if len(current_faces) > 0 and args.audio:
            # PLAY SEARCH VIDEO (background/loop)
            # or combine with 04 ?
            # os.system("omxplayer audio/04.mp3")
            os.system("omxplayer -o both -b audio/04A.mp4")
            # os.system("omxplayer -o both -b audio/search.mp4")
            # 05.mp3: WE ARE NOW, BASED ON THE MODEL FER2013, LOOKING FOR YOUR MATCH  IN THE DATABASE OF OLD COLONIAL IMAGES
            # 06.mp3: YOU ARE NOW MATCHED WITH ONE OF THE IMAGES BASED ON YOUR AND THEIR EMOTION. IT IS FOR YOU TO DECIDE IF THIS MATCH FITS.
            # 07.mp3: TIME TO SPEAK OF YOUR HOMEWORK:
            # 08.mp3: YOU MAY NOW GET YOUR PRINT. 
            # you have been matched
            os.system("omxplayer -o both -b audio/05.mp4")
            os.system("omxplayer -o both -b audio/06.mp4")
            os.system("omxplayer -o both -b audio/07.mp4")

        for i, face in enumerate(current_faces):
            # cv2.imwrite("image.jpg", current_image)
            # fr = face['rect']
            ## fr = current_faces[0]['rect']
            # x1, y1, x2, y2 = fr.left(), fr.top(), fr.right(), fr.bottom()
            x, y, width, height = face['x'], face['y'], face['width'], face['height']
            x1, y1, x2, y2 = x, y, x+width, y+height
            current_face = current_image[y1:y2, x1:x2]
            # cv2.imwrite("face.jpg", current_face)
            # landmarks = get_landmarks(shape_predictor, current_image, current_faces)
            fname = base+"_f{0}".format(i)
            image_fname = os.path.join("images", base + ".jpg")
            print ("image saved to {0}".format(image_fname), file=sys.stderr)
            cv2.imwrite(image_fname, current_image)

            if 'gender' in face:
                print ("FACEGENDER", face['gender'])
                gl = face['gender']['label']
                fname += "g"+gl[0]
                gp = face['gender'][gl] * 100.0
                p("Face {0}: {1} ({2:0.02f}%)\nestimated age: {3:0.02f}".format(i+1, gl, gp, face['age']))
                fname += "a{0}".format(int(floor(face['age']/10.0)))
            if 'emotion' in face:
                if not args.audio:
                    p("predominant emotion: {0}".format(face['emotion']['label']))
                fname += "e{0}".format(face['emotion']['label'][:2])
            fname += ".jpg"
            print ("Face saved to {0}".format(fname), file=sys.stderr)
            cv2.imwrite(os.path.join("faces", fname), current_face)

            ######## NEW MATCH CODE
            fe = face['emotionarray']
            # print ("face.emotionarray", fe)
            #fr = face['rect']
            # fr = current_faces[0]['rect']
            # x1, y1, x2, y2 = fr.left(), fr.top(), fr.right(), fr.bottom()
            # current_face = current_image[y1:y2, x1:x2]
            current_face_bgr = current_image_bgr[y1:y2, x1:x2]

            cd = cdist(feremotions, fe, metric="cityblock")
            fer_match_index = np.argmin(cd, axis=0)[0]

            cd = cdist(rmemotions, fe, metric="cityblock")
            rm_match_index = np.argmin(cd, axis=0)[0]
            # print ("Matches {0} fer, {1} rm".format(fer_match_index, rm_match_index))

            face_id, photo_id = rmlabels[rm_match_index]
            # crop photo to match the portrait
            with open(os.path.join(args.media, "data/faces/{0:06d}.json".format(face_id))) as inf:
                face_data = json.load(inf)
            with open (os.path.join(args.media, "data/photos/{0:06d}.json".format(photo_id))) as inf:
                photo_data = json.load(inf)

            rotate = photo_data['imageWidth']>photo_data['imageHeight']
            current_face_pil = Image.fromarray(cv2.cvtColor(current_face_bgr,cv2.COLOR_BGR2RGB))
            current_face_pil = make_print_version(current_face_pil)
            if rotate:
                current_face_pil = current_face_pil.rotate(-90, expand=1)
            current_face_pil.save("print01.jpg")

            # copy / prepare the fer image
            ferpath = os.path.join(args.media, "datasets/fer2013/Train/{0}.jpg".format(fer_match_index+1))
            im = Image.open(ferpath)
            im = make_print_version(im)
            if rotate:
                im = im.rotate(-90, expand=1)
            im.save("print02.jpg")

            # copy the original
            opath = os.path.join(args.media, "photos", photo_data['id'].replace(".o.jpg", ".print.jpg"))
            shutil.copyfile(opath, "print03.jpg")

            ps = {}
            ps['items'] = psi = []
            psi.append({'text': 'The Recognition Machine'})
            psi.append({'text': datetime.datetime.now().strftime("%d %b %Y %H:%M:%S+UTC")})
            psi.append({'text': ''})
            psi.append({'imagef': "print01.jpg"})
            psi.append({'imagef': "print02.jpg"})
            psi.append({'imagef': "print03.jpg"})
            make_qrcode(photo_id, "qr.png")
            psi.append({'text': ''})
            psi.append({'imagef': "qr.png"})
            psi.append({'text': ''})
            lines = unparse_emotion_summary(fe[0], feremotions[fer_match_index], rmemotions[rm_match_index])
            for l in lines:
                psi.append({'text': l})        
            print_photostrip(ps, copies=2)


        p()
    else:
        # p("No face detected")
        if args.audio:
            os.system("omxplayer -o both -b audio/04B.mp4")
            os.system("omxplayer -o both -b audio/07.mp4")

        image_fname = os.path.join("images", base + ".jpg")
        print ("image saved to {0}".format(image_fname), file=sys.stderr)
        cv2.imwrite(image_fname, current_image)

        # NO MATCH
        with open(os.path.join(args.media, "data/photos/nofaces.json")) as inf:
            nofaces = json.load(inf)
        from random import choice 
        photo_id = choice(nofaces)
        with open (os.path.join(args.media, "data/photos/{0:06d}.json".format(photo_id))) as inf:
            photo_data = json.load(inf)

        rotate = photo_data['imageWidth']>photo_data['imageHeight']

        # im = Image.open(image_fname)
        im = Image.fromarray(cv2.cvtColor(current_image_bgr,cv2.COLOR_BGR2RGB))
        if rotate:
            print ("rotating")
            im = im.rotate(-90, expand=1)
        print ("im", im, im.size)
        im = make_print_version(im)
        im.save("print01.jpg")

        opath = os.path.join(args.media, "photos", photo_data['id'].replace(".o.jpg", ".print.jpg"))
        shutil.copyfile(opath, "print02.jpg")
       
        ps = {}
        ps['items'] = psi = []
        psi.append({'text': 'The Recognition Machine'})
        psi.append({'text': datetime.datetime.now().strftime("%d %b %Y %H:%M:%S+UTC")})
        psi.append({'text': 'NO FACES DETECTED'})
        psi.append({'text': ''})
        psi.append({'imagef': "print01.jpg"})
        psi.append({'imagef': "print02.jpg"})
        make_qrcode(photo_id, "qr.png")
        psi.append({'text': ''})
        psi.append({'imagef': "qr.png"})
        print_photostrip(ps, copies=2)
            
    # print("sleeping")
    if args.audio:
        os.system("omxplayer -o both -b audio/08.mp4")

    sleep(5.0)

if args.gpiobutton:
    GPIO.cleanup()
