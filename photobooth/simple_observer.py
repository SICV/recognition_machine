from __future__ import print_function
from imutils.video import VideoStream
import cv2, os, sys, json
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
import dlib
import numpy as np
from time import sleep

from common import *
from printing import *
# import subprocess

# if __name__ == "__main__":

ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=0)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument("--genderage", default=False, action="store_true")
ap.add_argument("--post", default=None, help="post images to given URL")
args = ap.parse_args()

MODELS = os.path.expanduser(args.models)

def p (msg=""):
    if msg.strip():
        os.system("espeak \"{0}\" 2> /dev/null".format(msg))
    print (msg)

def tone ():
    os.system("aplay beep.wav 2> /dev/null")

# print ("Loading face detector", file=sys.stderr)
# face_detector = dlib.get_frontal_face_detector()
face_cascade = cv2.CascadeClassifier(os.path.join(MODELS, 'haarcascade_frontalface_default.xml'))
# print ("Loading shape predictor")
# shape_predictor = dlib.shape_predictor(os.path.join(MODELS, "shape_predictor_68_face_landmarks.dat"))

if args.emotion:
    p("loading emotion classifier")
    # print ("Loading emotion classifier", file=sys.stderr)
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(MODELS, "emotion_model.hdf5"))
if args.genderage:
    p ("loading genderage classifier")
    # from keras.models import load_model
    from keras.utils.data_utils import get_file
    weight_file = os.path.join(MODELS, "weights.28-3.73.hdf5")
    from wide_resnet import WideResNet
    genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
    genderage_classifier.load_weights(weight_file)
p("all loaded")

import datetime
from math import floor


while True:
    tone()
    current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
    current_faces = get_faces(face_cascade, current_image_bgr)
    if len(current_faces) > 0:
        n = datetime.datetime.now()
        base = n.strftime("%Y%m%d_%H%M%S")
        print (datetime.datetime.now())

        if len(current_faces) > 1:
            s = 's'
        else:
            s = ''
        p ("{0} face{1}".format(len(current_faces), s))

        if args.emotion:
            get_emotions(emotion_classifier, current_image, current_faces)
        if args.genderage:
            get_genderage(genderage_classifier, current_image_bgr, current_faces)

    for i, face in enumerate(current_faces):
        # cv2.imwrite("image.jpg", current_image)
        fr = face['rect']
        # fr = current_faces[0]['rect']
        x1, y1, x2, y2 = fr.left(), fr.top(), fr.right(), fr.bottom()
        current_face = current_image[y1:y2, x1:x2]
        # cv2.imwrite("face.jpg", current_face)
        # landmarks = get_landmarks(shape_predictor, current_image, current_faces)
        fname = base+"_f{0}".format(i)
        image_fname = os.path.join("images", base + ".jpg")
        print ("image saved to {0}".format(image_fname), file=sys.stderr)
        cv2.imwrite(image_fname, current_image)
        if 'gender' in face:
            print ("FACEGENDER", face['gender'])
            gl = face['gender']['label']
            fname += "g"+gl[0]
            gp = face['gender'][gl] * 100.0
            p("Face {0}: {1} ({2:0.02f}%)\nestimated age: {3:0.02f}".format(i+1, gl, gp, face['age']))
            fname += "a{0}".format(int(floor(face['age']/10.0)))
        if 'emotion' in face:
            p("predominant emotion: {0}".format(face['emotion']['label']))
            fname += "e{0}".format(face['emotion']['label'][:2])
        fname += ".jpg"
        print ("Face saved to {0}".format(fname), file=sys.stderr)
        cv2.imwrite(os.path.join("faces", fname), current_face)

        if args.post:
            # POST IT
            print ("posting face to {0}".format(args.post), file=sys.stderr)
            import post
            # _, imagedata = cv2.imencode(".jpg", current_image)
            # rescode = post.post(imagedata, face, args.post)
            rescode = post.post(image_fname, face, args.post)
            print (u"post result {0}\n{1}".format(rescode, rescode.text).encode("utf-8"), file=sys.stderr)
            rdata = json.loads(rescode.text)
            print (u"post result", rdata, file=sys.stderr)
            print_photostrip(rdata)
            # os.system("/home/pi/projects/")
            # subprocess.call(["python", "eptest1.py"], cwd="/home/pi/projects/recognition_machine/photobooth/printer")

        # print("sleeping")
    if len(current_faces) > 0:
        p()
    sleep(15.0)

