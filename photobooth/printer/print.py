#!/usr/bin/env python3
# test.py
import sys

# Bitmap printing example python
# https://github.com/mike42/escpos-snippets/blob/master/image-ports/columnFormat.py


ESC = b"\x1B"
BANG = b"!"


def w (b):
    sys.stdout.buffer.write(b)

RESET = b"\x1B\x40"

# FROM THE "interweb"
# byte[] ALLINEA_CT = {0x1B, 0x61, 0x01};
ALLINEA_CT = b"\x1B\x61\x01"

# PRINT MODES
PM = b'\x1B!'
PM_CLEAR = 0
PM_FONTB = 1
PM_BOLD = 8
PM_DH = 16
PM_DW = 32
PM_UNDERLINE = 128

PM_RESET = PM+b"\x00"

# w(PM_RESET)
# w(b"Testing...\n")
# w(PM + chr(PM_DW).encode("ascii"))
# w(b"dw\n")
# w(PM + chr(PM_DW | PM_DH).encode("ascii"))
# w(b"dw and dh\n")
# w(PM + chr(PM_BOLD | PM_DW | PM_DH).encode("ascii"))
# w(b"bold and dw and dh\n")
# w(PM + chr(PM_BOLD | PM_DW).encode("ascii"))
# w(b"bold and dw\n")
# w(PM + chr(PM_BOLD | PM_DH).encode("ascii"))
# w(b"bold and dh\n")
# w(PM_RESET)

# w(PM_RESET)
# w(b"Testing...\n")
# w(PM + chr(PM_FONTB).encode("ascii"))
# w(b"This is FONT B, different?\n")

w(RESET)
# w(b"One, two, three and four.\n")
# w(b"Five, six, seven, eight.\n")

def feed (n=255):
    w(b"\x1B\x4A"+bytes(bytearray((n,))))

# feed()

# https://reference.epson-biz.com/modules/ref_escpos/index.php?content_id=88

def bitmap ():
    # b"\x1B\x2A"
    p = 0b10000000
    data = bytearray((27, 42, 0, 10, 1))
    for row in range(10):
        for col in range(256):
            data.append(p)
    w(bytes(data))


# w(b"Hello bitmap\n")
# bitmap()

w(b"Bongiorno Massimo\n")
feed(100)



