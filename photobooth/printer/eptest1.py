from escpos.printer import Usb

# 0416:5011

""" Seiko Epson Corp. Receipt Printer M129 Definitions (EPSON TM-T88IV) """
p = Usb(0x0416,0x5011,0)
p.text("Hello World\n")
# p.image("00001Tris.400x.jpg")
p.image("00001Tris.400x.dither.png")
p.text("\n")
# p.image("tmp.png")
# p.text("\n")
#p.qr(content="http://recognitionmachine.constantvzw.org/p/1234567890/")
#p.text("\n")
#p.image("qr.test.png")
# p.barcode('1324354657687','EAN13',64,2,'','')
p.cut()
