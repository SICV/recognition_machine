from __future__ import print_function
from imutils.video import VideoStream
import cv2, os, sys, json
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
import numpy as np
from time import sleep
import datetime

from common import *
# from printing import *
# import subprocess
from caption import caption

# if __name__ == "__main__":

ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=0)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument("--genderage", default=False, action="store_true")
ap.add_argument("--post", default=None, help="post images to given URL")
args = ap.parse_args()

MODELS = os.path.expanduser(args.models)

def p (msg=""):
    if msg.strip():
        os.system("espeak \"{0}\" 2> /dev/null".format(msg))
    print (msg)

def tone ():
    os.system("aplay beep.wav 2> /dev/null")

# print ("Loading face detector", file=sys.stderr)
# face_detector = dlib.get_frontal_face_detector()
face_cascade = cv2.CascadeClassifier(os.path.join(MODELS, 'haarcascade_frontalface_default.xml'))
# print ("Loading shape predictor")
# shape_predictor = dlib.shape_predictor(os.path.join(MODELS, "shape_predictor_68_face_landmarks.dat"))

if args.emotion:
    p("loading emotion classifier")
    # print ("Loading emotion classifier", file=sys.stderr)
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(MODELS, "emotion_model.hdf5"))
if args.genderage:
    p ("loading genderage classifier")
    # from keras.models import load_model
    from keras.utils.data_utils import get_file
    weight_file = os.path.join(MODELS, "weights.28-3.73.hdf5")
    from wide_resnet import WideResNet
    genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
    genderage_classifier.load_weights(weight_file)
p("all loaded")

import datetime
from math import floor

try:
    os.makedirs("images")
except OSError:
    pass

while True:
    tone()
    current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
    current_faces = get_faces(face_cascade, current_image_bgr, use_dlib=False)
    if len(current_faces) > 0:
        n = datetime.datetime.now()
        base = n.strftime("%Y%m%d_%H%M%S")
        print (datetime.datetime.now())

        if len(current_faces) > 1:
            s = 's'
        else:
            s = ''
        p ("{0} face{1}".format(len(current_faces), s))

        if args.emotion:
            get_emotions(emotion_classifier, current_image, current_faces)
        if args.genderage:
            get_genderage(genderage_classifier, current_image_bgr, current_faces)

    for i, face in enumerate(current_faces):
        # cv2.imwrite("image.jpg", current_image)

        # draw bounding rectangle
        # fr = face['rect']
        x1, y1, x2, y2 = face['x'], face['y'], face['x'] + face['width'], face['y'] + face['height']
        cv2.rectangle(current_image_bgr, (x1, y1), (x2, y2), (255, 255, 0), 2)

        # text = "face{0}".format(i)
        text = caption(face)
        if "gender" in face:
            text += ""
        font_scale = 0.50
        thickness = 2
        color = (255, 0, 255)
        line_height = 18
        for i, line in enumerate(text.splitlines()):
            cv2.putText(current_image_bgr, line,
                (x1, y2 + (line_height * i+1)),
                cv2.FONT_HERSHEY_SIMPLEX,
                font_scale, color, thickness) # cv2.LINE_AA


        # print("sleeping")
    if len(current_faces) > 0:
        p()
        ts = os.path.join("images", datetime.datetime.now().strftime("%Y%m%d_%H%M%S.jpg"))
        cv2.imwrite(ts, current_image_bgr)
    cv2.imshow('window_frame', current_image_bgr)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # sleep(1.0)

