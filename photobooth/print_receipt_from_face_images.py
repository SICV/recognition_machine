from __future__ import print_function
from imutils.video import VideoStream
import cv2, os, sys, json
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
# import dlib
import numpy as np
import json

from common import *
from printing import *
# import subprocess
from PIL import Image, ImageOps

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)


# if __name__ == "__main__":

ap = argparse.ArgumentParser("")
ap.add_argument("--emotion", default=False, action="store_true")
ap.add_argument("--genderage", default=False, action="store_true")
ap.add_argument("--models", default="models")
ap.add_argument("image", nargs="+")
args = ap.parse_args()

def p (msg=""):
    if msg.strip():
        os.system("espeak \"{0}\" 2> /dev/null".format(msg))
    print (msg)

if args.emotion:
    p("loading emotion classifier")
    # print ("Loading emotion classifier", file=sys.stderr)
    from keras.models import load_model
    emotion_classifier = load_model(os.path.join(args.models, "emotion_model.hdf5"))
if args.genderage:
    p ("loading genderage classifier")
    # from keras.models import load_model
    from keras.utils.data_utils import get_file
    weight_file = os.path.join(args.models, "weights.28-3.73.hdf5")
    from wide_resnet import WideResNet
    genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
    genderage_classifier.load_weights(weight_file)
p("all loaded")


AGE_CHOICES = (
    ('0,5', 'infant'),
    ('5,12', 'infant'),
    ('12,20', 'adolescent'),
    ('20,35', 'young'),
    ('35,50', 'middle-aged'),
    ('50,65', 'older'),
    ('65,110', 'elderly')
)
def map_age (n):
    for val, label in AGE_CHOICES:
        low, high = [float(x) for x in val.split(",")]
        # print ("age_map {0} {1} {2} {3}".format(x, low, high, val, label))
        if n >= low and n < high:
            return label

def get_emotions_sorted (d):
    ret = []
    ret.append((d['emotion']['angry'], "angry"))
    ret.append((d['emotion']['disgust'], "disgust"))
    ret.append((d['emotion']['fear'], "fear"))
    ret.append((d['emotion']['happy'], "happy"))
    ret.append((d['emotion']['sad'], "sad"))
    ret.append((d['emotion']['neutral'], "neutral"))
    ret.append((d['emotion']['surprise'], "surprise"))
    ret.sort(reverse=True)
    return ret

def caption(d, max_length=32):
    lines = []
    if d['gender']:
        if d['gender']['label'] == "male":
            n = "man"
        else:
            n = "woman"
        title = "\"{0} {1} {2}\"".format(d['emotion']['label'], map_age(d['age']), n)
        lines.append(title.upper())
    if d['gender']['male'] != None:
        lines.append("male({0:0.01f}%)/female({1:0.01f}%)".format(d['gender']['male']*100.0, d['gender']['female']*100.0))
    if d['age']:
        lines.append("age {0:0.1f}".format(d['age']))
    if d['emotion']:
        es = get_emotions_sorted(d)
        lines.append("/".join(["{0}({1:0.01f}%)".format(label, value*100.0) for value, label in es[:2]]))

    if max_length:
        lines = [x[:max_length] for x in lines]

    return "\n".join(lines)

def make_print_image (pin, pout):
    im = Image.open(pin)
    im = im.convert("L")
    rw, rh = fit_size(im, (400, 400))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.equalize(im)
    im = im.convert("1")
    im.save(pout)

p = {}
p['items'] = items = []

for n in args.image:
    print (n)
    current_image_bgr = cv2.imread(n)
    h, w, _ = current_image_bgr.shape
    current_image = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2GRAY)
    current_faces = []
    face = {}
    face['x'] = 0
    face['y'] = 0
    face['width'] = w
    face['height'] = h
    # face['rect'] = dlib.rectangle(0, 0, w, h)
    current_faces.append(face)
    if args.emotion:
         get_emotions(emotion_classifier, current_image, current_faces)
    if args.genderage:
         get_genderage(genderage_classifier, current_image_bgr, current_faces)
    
    print(face)
    base, ext = os.path.splitext(n)
    nprint = base + ".print.png"
    make_print_image(n, nprint)

    items.append({'image': 'file://{0}'.format(nprint)})
    items.append({'text': caption(face)})
    items.append({'text': ''})

print (json.dumps(p, indent=2))
print_photostrip(p)

    # print (face)
