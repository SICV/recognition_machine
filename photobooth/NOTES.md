Using the "BCM" pin ordering
Pins 1 + 2 are the top pins when viewing pi sd card up / usb ports down -- gpio pins on the top right of profile.

Wiring
=============

Shutdown Button
-----------------
Pins 14 & 16 (7th & 8th in case-side) (gnd + gpio23)



Flash on Pi
--------------
GPIO -Purple -- Pin7: GPIO4
GND   Gray --- Pin 6 : Ground
GND   White? -- not connected
5V -- Black -- Pin 2: 5V

Button
-----------
35 Yellow GPIO19
37 Green GPIO26
39 Blue GND



Added
logo.nologo
to the end of the line in /boot/cmdline.txt


    omxplayer -o both -b foo.mp4

* -o both: force audio to both hdmi + headphone, vals: local, hdmi, both
* -b: blank the screen


fallback ip address is 192.168.1.23


Working in Koln
===================
* charge speaker for testing...
* camera_test ... add bracketed exposure? ... try different brightness values (live ?!)

* Wire and test RED BUTTON
* Use the old button as a shutdown button !

photobooth
--------------
x NEW PHOTOS
* NEW VIDEOS
* NEW SOUNDS
* POSTER/print tiles for booth? ("Happy")
* play video (loop) while taking picture?
* BRIGHTNESS TESTING!
* subtitles (poc + DE)


website
------------
* Link rephotos with Institutions
* Check links/Item IDs for Rauch objects / extract ID if available ?!
* Show full emotion spectrum


For new images
===================
cd website/
python manage.py import_images ../*.jpg
(python manage.py rephotograph_set_sizes) ... should be part of import_images now

make thumb
make print

cd website/media/photos
website/scripts/tileimages *.o.jpg

python manage.py detect_faces
    This generates rm_emotions.npy & rm_labels.npy

python manage.py dump_data
    Updates media/data/photos/*.json & faces




Install
=============
apt install git python-pip python-opencv python-pil python-scipy python-h5py espeak omxplayer python-picamers ttf-freefont
pip install imutils python-escpos tensorflow keras

sudo adduser pi lp

Copied recognitionmachine.service and chown root:root, chmod 644
sudo systemctl enable recognitionmachine


Nice screen
================
http://linuxfromscratch.org/blfs/view/svn/postlfs/logon.html

sudo su
cp /etc/issue /etc/issue~
clear > /etc/issue

nano /etc/issue

Added Display line on next line.


# VIDEO
python scripts/make_movie_rm.py
ffmpeg -i to check duration
44.76 ... 
match framerate to fit this duration...
how many frames ?? ... melt to see 392
python scripts/make_movie_rm.py --framerate 8.775464517573315
ffmpeg -i search_rm.avi -i ../photobooth/audio/05.mp3 05.mp4

python scripts/make_movie_fer.py --labels ~/machinelearning/datasets/fer2013_public/labels_public.txt --framerate 8
Interupt at some point... (have enough)

Firefox always accept camera...

https://stackoverflow.com/questions/12160174/always-accept-webrtc-webcam-request#18230492

Go in url about:config
Search media.navigator.permission.disabled
dbClick or set value to true

THURSDAY
================
x REMAKE 03+05 with titles on top!
x COPY 03+05 to pi


WORKING wpa_supplicant.conf on black pi
==================
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=BE

network={
    ssid="rjmwlan"
    psk="wlan4internet2016!"
}



USB permissions

#currently:
#pi adm dialout cdrom sudo audio video plugdev games users input netdev gpio i2c spi
#pi adm lp dialout cdrom sudo audio video plugdev games users input netdev gpio i2c spi

media/datasets photos data


FRIDAY
==============
* PAGEKITE + tunnel!
* TWO COPIES with CUT HERE MARKS
* REMOVE DATE (add generic date + place)
* ADD TITLE / CREDIT ??
* RE-RENDER
* ADD LABEL FACE x/xx when multiple !!!

Volume Buttons (3rd + 4th from top on RHS of monitor)


Pagekite

mkdir /home/pi/pk
cd /home/pi/pk
wget https://pagekite.net/pk/pagekite.py
chmod +x pagekite.py

Using pagekite.service from ergwikitunnel
essentially which runs the command:

    /home/pi/pk/pagekite.py --clean --frontend=bot.activearchives.org:10109 --service_on=http:bot.activearchives.org:localhost:80:recognitionmachine --service_on=raw-22:bot.activearchives.org:localhost:22:recognitionmachine


full service:

[Unit]
Description=pagekite
After=syslog.target network.target

[Service]
Type=simple
User=pi
Group=pi
ExecStart=/home/pi/pk/pagekite.py --clean --frontend=bot.activearchives.org:10109 --service_on=http:bot.activearchives.org:localhost:80:recognitionmachine --service_on=raw-22:bot.activearchives.org:localhost:22:recognitionmachine

[Install]
WantedBy=multi-user.target


sudo systemctl enable pagekite


Kiosk
==============

https://willhaley.com/blog/debian-fullscreen-gui-kiosk/
https://stackoverflow.com/questions/17069979/chrome-always-allow-webcam-over-http
https://www.chromium.org/administrators/linux-quick-start

https://wiki.debian.org/WiFi/HowToUse#Command_Line

wpa_passphrase myssid my_very_secret_passphrase > /etc/wpa_supplicant/wpa_supplicant.conf
wpa_passphrase rjmwlan "wlan4internet2016!" > 
