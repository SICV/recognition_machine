from flask import Flask, render_template, jsonify, request
# from flask_socketio import SocketIO, send, emit
from imutils.video import VideoStream
import cv2, os
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
import dlib
import numpy as np



current_image = None
current_image_bgr = None
current_faces = None



app = Flask(__name__)
# socketio = SocketIO(app)
# app.config['SECRET_KEY'] = 'secret!'


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/control")
def control():
    return render_template("control.html")

@app.route("/photo")
def photo():
    global current_image, current_image_bgr
    print ("handle_photo", request)
    width = request.args.get("width", app.config.get("width", 640))
    height = request.args.get("height", app.config.get("height", 480))
    iso = request.args.get("iso", app.config.get("iso", 400))
    sleept = request.args.get("sleep", app.config.get("sleep", 2.0))
    camera = app.config.get("camera")
    if camera == -1:
        import picamera, picamera.array
        with picamera.PiCamera(resolution=(width, height)) as camera:
            # camera.resolution = ()
            camera.iso = iso
            if sleept:
                print ("sleep", sleep)
                sleep(sleept)
            ss = camera.exposure_speed
            camera.shutter_speed = ss
            camera.exposure_mode = 'off'
            g = camera.awb_gains
            camera.awb_mode = 'off'
            camera.awb_gains = g
            data = {}
            data['exposure_mode'] = str(camera.exposure_mode)
            data['shutter_speed'] = str(camera.shutter_speed)
            data['awb_mode'] = str(camera.awb_mode)
            data['awb_gains'] = str(camera.awb_gains)

            with picamera.array.PiRGBArray(camera) as frame:
                camera.capture(frame, 'bgr')
                print("got frame", frame)
                current_image_bgr = frame.array
                gray = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2GRAY)
                if app.config.get("rotate"):
                    gray = imutils.rotate_bound(gray, app.config.get("rotate"))
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, app.config.get("rotate"))
                else:
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, 0)
                current_image = gray
                # print('Captured %dx%d image' % (
                #         output.array.shape[1], output.array.shape[0]))
                _, imdata = cv2.imencode(".jpg", gray)
                data["height"], data["width"] = gray.shape[:2]
                data["image"] = make_jpeg_data_url(imdata)
                # emit("photo", data)
                return jsonify(data)
    else:
        stream = cv2.VideoCapture(camera)
        (grabbed, frame) = stream.read()
        current_image_bgr = frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if app.config.get("rotate"):
            gray = imutils.rotate_bound(gray, app.config.get("rotate"))
            current_image_bgr = imutils.rotate_bound(current_image_bgr, app.config.get("rotate"))
        current_image = gray
        _, imdata = cv2.imencode(".jpg", gray)
        data={}
        data["height"], data["width"] = gray.shape[:2]
        data["image"] = make_jpeg_data_url(imdata)
        # emit("photo", data)
        stream.release()
        return jsonify(data)


def make_jpeg_data_url(binary_data):
    prefix = 'data:image/jpeg;base64,'
    # fin = open(filename, 'rb')
    # contents = fin.read()
    return prefix + base64.b64encode(binary_data)

def detect_faces (gray, face_cascade, scaleFactor=None, minNeighbors=None, minSize=None):
    global current_faces
    print ("detect_faces")
    args = dict()
    if scaleFactor:
        args['scaleFactor'] = scaleFactor
    if minNeighbors:
        args['minNeighbors'] = minNeighbors
    if minSize:
        args['minSize'] = (minSize, minSize)
    faces = face_cascade.detectMultiScale(gray, **args)
    ret=[]
    current_faces = []
    for rect in faces:
        current_faces.append(rect)
        x, y, w, h = rect
        ret.append([int(x), int(y), int(w), int(h)])
    return ret

@app.route("/faces")
def handle_faces():
    global face_detector, current_faces
    if face_detector == None:
        print ("Loading face detector")
        face_detector = dlib.get_frontal_face_detector()
    print ("handle_faces", request)
    # faces = detect_faces(current_image, face_cascade)
    faces = face_detector(current_image, 0)
    dd = []
    current_faces = []
    for r in faces:
        print ("got dlib.rect", r)
        current_faces.append(r)
        dd.append({'x': r.left(), 'y': r.top(), 'width': r.width(), 'height': r.height()})
    data = {}
    data['faces'] = dd
    return jsonify(data)

@app.route("/landmarks")
def handle_landmarks():
    global shape_predictor
    if shape_predictor == None:
        print ("Loading shape predictor")
        shape_predictor = dlib.shape_predictor(os.path.join(MODELS, "shape_predictor_68_face_landmarks.dat"))
    print ("handle_landmarks", request)
    shapes = []
    for rect in current_faces:
        ss = shape_predictor(current_image, rect)
        ss = face_utils.shape_to_np(ss)
        print ("got shape", ss)
        shapes.append([{'x': int(x[0]), 'y': int(x[1])} for x in ss])
    data = {}
    data['landmarks'] = shapes
    return jsonify(data)

@app.route("/emotions")
def handle_emotions():
    print ("handle_emotions", request)
    global emotion_classifier
    if emotion_classifier == None:
        print ("Loading emotion classifier")
        from keras.models import load_model
        emotion_classifier = load_model(os.path.join(MODELS, "emotion_model.hdf5"))
        print ("Loaded")

    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_offsets = (20, 40)
    # fer2013
    # emotion_labels = {0:'angry',1:'disgust',2:'fear',3:'happy', 4:'sad',5:'surprise',6:'neutral'}
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    dd = []
    for rect in current_faces:
        face_coordinates = (rect.left(), rect.top(), rect.width(), rect.height())
        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        gray_face = current_image[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except:
            print ("exception resizing gray_face")
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        # emotion_probability = np.max(emotion_prediction)
        # emotion_label_arg = np.argmax(emotion_prediction)
        # emotion_text = emotion_labels[emotion_label_arg]
        # emotion_window.append(emotion_text)
        d = {}
        for label, score in zip(emotion_labels, emotion_prediction[0]):
            d[label] = float(score)
        print ("got emotion_prediction", d)
        dd.append(d)
    return jsonify({'emotions': dd})

@app.route("/genderage")
def handle_genderage():
    print ("handle_genderage", req)
    global genderage_classifier

    IMG_SIZE = 64
    DEPTH = 16
    K = WIDTH = 8
    MARGIN = 0.4
    if genderage_classifier == None:
        print ("Loading genderage classifier")
        # from keras.models import load_model
        from keras.utils.data_utils import get_file
        weight_file = os.path.join(MODELS, "weights.28-3.73.hdf5")
        from wide_resnet import WideResNet
        genderage_classifier = WideResNet(IMG_SIZE, depth=DEPTH, k=K)()
        genderage_classifier.load_weights(weight_file)
        print ("Loaded")

    img = current_image_bgr
    img_h, img_w, _ = np.shape(img)

    # detect faces using dlib detector
    # detected = detector(input_img, 1)
    detected = current_faces
    faces = np.empty((len(detected), IMG_SIZE, IMG_SIZE, 3))

    data = {}
    if len(detected) > 0:
        for i, d in enumerate(detected):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            xw1 = max(int(x1 - MARGIN * w), 0)
            yw1 = max(int(y1 - MARGIN * h), 0)
            xw2 = min(int(x2 + MARGIN * w), img_w - 1)
            yw2 = min(int(y2 + MARGIN * h), img_h - 1)
            # cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
            # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i, :, :, :] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (IMG_SIZE, IMG_SIZE))

        # predict ages and genders of the detected faces
        results = genderage_classifier.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages_raw = results[1]
        print ("predicted_ages", predicted_ages_raw, predicted_ages_raw.shape)
        predicted_ages = results[1].dot(ages).flatten()
        # print ("predicted_ages", predicted_ages, predicted_ages[0], type(predicted_ages[0]))
        data['ages'] = [float(x) for x in predicted_ages]
        data['genders'] = [{'female': float(x[0]), 'male': float(x[1])} for x in predicted_genders]
        print ("predicted_genderage", data)
        # print ("predicted_genders", predicted_genders)

    return jsonify(data)

ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=640)
ap.add_argument("--height", type=int, default=480)
ap.add_argument("--rotate", type=int, default=270)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the cascade XML files, default: ./models")
args = ap.parse_args()
app.config['vflip'] = args.vflip
app.config['camera'] = args.camera
w = app.config['width'] = args.width
h = app.config['height'] = args.height
app.config['rotate'] = args.rotate
app.config['vflip'] = args.vflip

MODELS = os.path.expanduser(args.models)

# CLASSIFIERS
face_detector = None
shape_predictor = None
emotion_classifier = None
genderage_classifier = None

# print ("Starting server...")
# socketio.run(app, host="0.0.0.0")
app.run(host='127.0.0.1', port=8080, debug=True)
#   app.run(host='0.0.0.0', port=8080, debug=True)
