Recognition Machine
======================


website
-------
The database (public web server)

* python3

  pip install django pillow opencv-python qrcode markdown keras tensorflow


python3 -m venv venv
source venv/bin/activate
pip install -r pip-requirements.txt


photobooth
-----------------
The photobooth code running on a Raspberry Pi 3.

* python2
* Flask
* ---flask-socketio---
* python-opencv

* html5lib
* requests

* tensorflow
* numpy
* scipy
* opencv-python
* pillow
* pandas
* matplotlib
* h5py
* keras

Includes / Licenses
===============
Includes [instascan.js](https://github.com/schmich/instascan/releases) by Chris Schmich. Released under an MIT License.

Links
=============
* <http://www.paulvangent.com/2016/08/05/emotion-recognition-using-facial-landmarks/>
* <https://www.researchgate.net/publication/5440197_Facial_expressions_of_emotion_KDEF_Identification_under_different_display-duration_conditions>
* <https://github.com/oarriaga/face_classification>

### models
* Emotion: <https://github.com/petercunha/Emotion> based on fer2013
* <https://github.com/yu4u/age-gender-estimation/>
* <https://github.com/zZyan/race_gender_recognition> (no model available, but linked to [this article](https://medium.com/@zhuyan/gender-and-race-recognition-transfer-multi-task-learning-for-the-laziest-88316e6e492) which is quite interesting and problematic (Claims to counter [this article](https://www.nytimes.com/2018/02/09/technology/facial-recognition-race-artificial-intelligence.html))

### datasets
* [IMDB-wiki](https://data.vision.ee.ethz.ch/cvl/rrothe/imdb-wiki/)
* [UTKFace](https://susanqq.github.io/UTKFace/)

